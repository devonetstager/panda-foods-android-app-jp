package com.sha.netstager.pandafoods.contentProvider;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;

import com.sha.netstager.pandafoods.datatables.CartDb;
import com.sha.netstager.pandafoods.datatables.CatagoryDb;
import com.sha.netstager.pandafoods.datatables.CountryDb;
import com.sha.netstager.pandafoods.datatables.CustomerDb;
import com.sha.netstager.pandafoods.datatables.DistrictDb;
import com.sha.netstager.pandafoods.datatables.LocationDb;
import com.sha.netstager.pandafoods.datatables.OfferDb;
import com.sha.netstager.pandafoods.datatables.OrderDb;
import com.sha.netstager.pandafoods.datatables.OrderProductsDb;
import com.sha.netstager.pandafoods.datatables.ProductsDb;
import com.sha.netstager.pandafoods.datatables.StateDb;
import com.sha.netstager.pandafoods.datatables.WeightDb;

public class LocalDbProvider extends ContentProvider {
    public LocalDbProvider() {
    }

    private SQLiteConnector dbHelper;
    private static final int ALL_CATEGORIES = 1;
    private static final int ALL_PRODUCTS = 2;
    private static final int ALL_ORDERS= 3;
    private static final int ALL_IN_CART = 4;
    private static final int ALL_ORDER_PRODUCTS = 5;
    private static final int ALL_WEIGHTS = 6;
    private static final int ALL_LOCATIONS= 7;
    private static final int ALL_CUSTOMERS= 8;
    private static final int ALL_OFFERS= 9;
    private static final int ALL_DISTRICTS= 10;
    private static final int ALL_STATES= 11;
    private static final int ALL_COUNTRY= 12;


    // authority is the symbolic name of your provider
    // To avoid conflicts with other providers, you should use
    // Internet domain ownership (in reverse) as the basis of your provider authority.
    private static final String AUTHORITY = "com.sha.netstager.pandafoods.contentprovider";

    // create content URIs from the authority by appending path to database table
    public static final Uri CATAGORY_URI=
            Uri.parse("content://" + AUTHORITY + "/category");
    public static final Uri CONTENT_URI =
            Uri.parse("content://" + AUTHORITY + "/products");
    public static final Uri ORDER_URI =
            Uri.parse("content://" + AUTHORITY + "/orders");
    public static final Uri ORDER_PRODUCT_URI =
            Uri.parse("content://" + AUTHORITY + "/ordersproducts");
    public static final Uri CART_URI =
            Uri.parse("content://" + AUTHORITY + "/cart");
    public static final Uri WEIGHT_URI =
            Uri.parse("content://" + AUTHORITY + "/weight");
    public static final Uri LOC_URI =
            Uri.parse("content://" + AUTHORITY + "/location");
    public static final Uri CUSOTMER_URI =
            Uri.parse("content://" + AUTHORITY + "/customer");
    public static final Uri OFFER_URI =
            Uri.parse("content://" + AUTHORITY + "/offers");
    public static final Uri DISTRICT_URI =
            Uri.parse("content://" + AUTHORITY + "/district");
    public static final Uri STATE_URI =
            Uri.parse("content://" + AUTHORITY + "/state");
    public static final Uri COUNTRY_URI =
            Uri.parse("content://" + AUTHORITY + "/country");
    // a content URI pattern matches content URIs using wildcard characters:
    // *: Matches a string of any valid characters of any length.
    // #: Matches a string of numeric characters of any length.
    private static final UriMatcher uriMatcher;
    static {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(AUTHORITY, "category", ALL_CATEGORIES);
        uriMatcher.addURI(AUTHORITY, "products", ALL_PRODUCTS);
        uriMatcher.addURI(AUTHORITY, "orders",ALL_ORDERS);
        uriMatcher.addURI(AUTHORITY, "cart", ALL_IN_CART);
        uriMatcher.addURI(AUTHORITY, "ordersproducts/", ALL_ORDER_PRODUCTS);
        uriMatcher.addURI(AUTHORITY, "weight/", ALL_WEIGHTS);
        uriMatcher.addURI(AUTHORITY, "location/", ALL_LOCATIONS);
        uriMatcher.addURI(AUTHORITY, "customer/", ALL_CUSTOMERS);
        uriMatcher.addURI(AUTHORITY, "offers/", ALL_OFFERS);
        uriMatcher.addURI(AUTHORITY, "district/", ALL_DISTRICTS);
        uriMatcher.addURI(AUTHORITY, "state/", ALL_STATES);
        uriMatcher.addURI(AUTHORITY, "country/", ALL_COUNTRY);
    }

    // system calls onCreate() when it starts up the provider.
    @Override
    public boolean onCreate() {
        // get access to the database helper
//        dbHelper = new SQLiteConnector(getContext());
        dbHelper= SQLiteConnector.getInstance(getContext());
        return false;
    }

    //Return the MIME type corresponding to a content URI
    @Override
    public String getType(Uri uri) {

        switch (uriMatcher.match(uri)) {
            case ALL_CATEGORIES:
                return "vnd.android.cursor.dir/vnd.com.sha.netstager.pandafoods.contentprovider.category";
            case ALL_PRODUCTS:
                return "vnd.android.cursor.dir/vnd.com.sha.netstager.pandafoods.contentprovider.products";
            case ALL_IN_CART:
                return "vnd.android.cursor.dir/vnd.com.sha.netstager.pandafoods.contentprovider.cart";
            case ALL_ORDERS:
                return "vnd.android.cursor.dir/vnd.com.netstager.pandafoods.contentprovider.orders";
            case ALL_ORDER_PRODUCTS:
                return "vnd.android.cursor.dir/vnd.com.netstager.pandafoods.contentprovider.ordersproducts";
            case ALL_WEIGHTS:
                return "vnd.android.cursor.dir/vnd.com.netstager.pandafoods.contentprovider.weight";
            case ALL_LOCATIONS:
                return "vnd.android.cursor.dir/vnd.com.netstager.pandafoods.contentprovider.location";
            case ALL_CUSTOMERS:
                return "vnd.android.cursor.dir/vnd.com.netstager.pandafoods.contentprovider.location";
            case ALL_OFFERS:
                return "vnd.android.cursor.dir/vnd.com.netstager.pandafoods.contentprovider.offers";
            case ALL_DISTRICTS:
                return "vnd.android.cursor.dir/vnd.com.netstager.pandafoods.contentprovider.district";
            case ALL_STATES:
                return "vnd.android.cursor.dir/vnd.com.netstager.pandafoods.contentprovider.state";
            case ALL_COUNTRY:
                return "vnd.android.cursor.dir/vnd.com.netstager.pandafoods.contentprovider.country";

            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }
    }

    // The insert() method adds a new row to the appropriate table, using the values
    // in the ContentValues argument. If a column name is not in the ContentValues argument,
    // you may want to provide a default value for it either in your provider code or in
    // your database schema.
    @Override
    public Uri insert(Uri uri, ContentValues values) {
        long id;
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        switch (uriMatcher.match(uri)) {
            case ALL_CATEGORIES:
                id = db.insert(CatagoryDb.SQLITE_TABLE, null, values);
                getContext().getContentResolver().notifyChange(uri, null);
                return Uri.parse(CATAGORY_URI +"/"+ id);
            case ALL_PRODUCTS:
                //do nothing
                id = db.insert(ProductsDb.SQLITE_TABLE, null, values);
                getContext().getContentResolver().notifyChange(uri, null);
                return Uri.parse(CONTENT_URI + "/" + id);
            case ALL_ORDERS:
                id = db.insert(OrderDb.SQLITE_TABLE, null, values);
                getContext().getContentResolver().notifyChange(uri, null);
                return Uri.parse(ORDER_URI + "/" + id);
            case ALL_ORDER_PRODUCTS:
                id = db.insert(OrderProductsDb.SQLITE_TABLE, null, values);
                getContext().getContentResolver().notifyChange(uri, null);
                return Uri.parse(ORDER_PRODUCT_URI + "/" + id);
            case ALL_IN_CART:
                id = db.insert(CartDb.SQLITE_TABLE, null, values);
                getContext().getContentResolver().notifyChange(uri, null);
                return Uri.parse(CART_URI + "/" + id);
            case ALL_WEIGHTS:
                id = db.insert(WeightDb.SQLITE_TABLE, null, values);
                getContext().getContentResolver().notifyChange(uri, null);
                return Uri.parse(WEIGHT_URI + "/" + id);
            case ALL_LOCATIONS:
                id = db.insert(LocationDb.SQLITE_TABLE, null, values);
                getContext().getContentResolver().notifyChange(uri, null);
                return Uri.parse(LOC_URI + "/" + id);
            case ALL_CUSTOMERS:
                id = db.insert(CustomerDb.SQLITE_TABLE, null, values);
                getContext().getContentResolver().notifyChange(uri, null);
                return Uri.parse(CUSOTMER_URI + "/" + id);
            case ALL_OFFERS:
                id = db.insert(OfferDb.SQLITE_TABLE, null, values);
                getContext().getContentResolver().notifyChange(uri, null);
                return Uri.parse(OFFER_URI + "/" + id);
            case ALL_DISTRICTS:
                id = db.insert(DistrictDb.SQLITE_TABLE, null, values);
                getContext().getContentResolver().notifyChange(uri, null);
                return Uri.parse(DISTRICT_URI + "/" + id);
            case ALL_STATES:
                id = db.insert(StateDb.SQLITE_TABLE, null, values);
                getContext().getContentResolver().notifyChange(uri, null);
                return Uri.parse(STATE_URI + "/" + id);
            case ALL_COUNTRY:
                id = db.insert(CountryDb.SQLITE_TABLE, null, values);
                getContext().getContentResolver().notifyChange(uri, null);
                return Uri.parse(COUNTRY_URI + "/" + id);

            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }
    }

    // The query() method must return a Cursor object, or if it fails,
    // throw an Exception. If you are using an SQLite database as your data storage,
    // you can simply return the Cursor returned by one of the query() methods of the
    // SQLiteDatabase class. If the query does not match any rows, you should return a
    // Cursor instance whose getCount() method returns 0. You should return null only
    // if an internal error occurred during the query process.
    @Override
    public Cursor query(Uri uri, String[] projection, String selection,
                        String[] selectionArgs, String sortOrder) {

        SQLiteDatabase db = dbHelper.getWritableDatabase();
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();


        switch (uriMatcher.match(uri)) {
            case ALL_CATEGORIES:
                //do nothing
                queryBuilder.setTables(CatagoryDb.SQLITE_TABLE);
                break;
            case ALL_PRODUCTS:
                //do nothing
                queryBuilder.setTables(ProductsDb.SQLITE_TABLE);
                break;
            case ALL_IN_CART:
                //do nothing
                queryBuilder.setTables(CartDb.SQLITE_TABLE);
                break;
            case ALL_ORDERS:
                queryBuilder.setTables(OrderDb.SQLITE_TABLE);
                break;
            case ALL_ORDER_PRODUCTS:
                queryBuilder.setTables(OrderProductsDb.SQLITE_TABLE);
                break;
            case ALL_WEIGHTS:
                queryBuilder.setTables(WeightDb.SQLITE_TABLE);
                break;
            case ALL_LOCATIONS:
                queryBuilder.setTables(LocationDb.SQLITE_TABLE);
                break;
            case ALL_CUSTOMERS:
                queryBuilder.setTables(CustomerDb.SQLITE_TABLE);
                break;
            case ALL_OFFERS:
                queryBuilder.setTables(OfferDb.SQLITE_TABLE);
                break;
            case ALL_DISTRICTS:
                queryBuilder.setTables(DistrictDb.SQLITE_TABLE);
                break;
            case ALL_STATES:
                queryBuilder.setTables(StateDb.SQLITE_TABLE);
                break;
            case ALL_COUNTRY:
                queryBuilder.setTables(CountryDb.SQLITE_TABLE);
                break;

            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }

        return queryBuilder.query(db, projection, selection,
                selectionArgs, null, null, sortOrder);

    }

    // The delete() method deletes rows based on the seletion or if an id is
    // provided then it deleted a single row. The methods returns the numbers
    // of records delete from the database. If you choose not to delete the data
    // physically then just update a flag here.
    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        int deleteCount;
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        switch (uriMatcher.match(uri)) {
            case ALL_CATEGORIES:
                //do nothing
                deleteCount = db.delete(CatagoryDb.SQLITE_TABLE, selection, selectionArgs);
                break;
            case ALL_PRODUCTS:
                //do nothing
                deleteCount = db.delete(ProductsDb.SQLITE_TABLE, selection, selectionArgs);
                break;
            case ALL_IN_CART:
                //do nothing
                deleteCount = db.delete(CartDb.SQLITE_TABLE, selection, selectionArgs);
                break;
            case ALL_ORDERS:
                deleteCount  = db.delete(OrderDb.SQLITE_TABLE, selection, selectionArgs);
                break;
            case ALL_ORDER_PRODUCTS:
                deleteCount  = db.delete(OrderProductsDb.SQLITE_TABLE, selection, selectionArgs);
                break;
            case ALL_WEIGHTS:
                deleteCount  = db.delete(WeightDb.SQLITE_TABLE, selection, selectionArgs);
                break;
            case ALL_LOCATIONS:
                deleteCount  = db.delete(LocationDb.SQLITE_TABLE, selection, selectionArgs);
                break;
            case ALL_CUSTOMERS:
                deleteCount  = db.delete(CustomerDb.SQLITE_TABLE, selection, selectionArgs);
                break;
            case ALL_OFFERS:
                deleteCount  = db.delete(OfferDb.SQLITE_TABLE, selection, selectionArgs);
                break;
            case ALL_DISTRICTS:
                deleteCount  = db.delete(DistrictDb.SQLITE_TABLE, selection, selectionArgs);
                break;
            case ALL_STATES:
                deleteCount  = db.delete(StateDb.SQLITE_TABLE, selection, selectionArgs);
                break;
            case ALL_COUNTRY:
                deleteCount  = db.delete(CountryDb.SQLITE_TABLE, selection, selectionArgs);
                break;
            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }

        getContext().getContentResolver().notifyChange(uri, null);
        return deleteCount;
    }

    // The update method() is same as delete() which updates multiple rows
    // based on the selection or a single row if the row id is provided. The
    // update method returns the number of updated rows.
    @Override
    public int update(Uri uri, ContentValues values, String selection,
                      String[] selectionArgs) {
        int updateCount = 0;
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        switch (uriMatcher.match(uri)) {
            case ALL_CATEGORIES:
                //do nothing
                updateCount= db.update(CatagoryDb.SQLITE_TABLE, values, selection, selectionArgs);
                break;
            case ALL_PRODUCTS:
                updateCount= db.update(ProductsDb.SQLITE_TABLE, values, selection, selectionArgs);
                break;
            case ALL_IN_CART:
                //do nothing
                updateCount= db.update(CartDb.SQLITE_TABLE, values, selection, selectionArgs);
                break;
            case ALL_ORDERS:
                updateCount= db.update(OrderDb.SQLITE_TABLE, values, selection, selectionArgs);
                break;
            case ALL_ORDER_PRODUCTS:
                updateCount= db.update(OrderProductsDb.SQLITE_TABLE, values, selection, selectionArgs);
                break;
            case ALL_WEIGHTS:
                updateCount= db.update(WeightDb.SQLITE_TABLE, values, selection, selectionArgs);
                break;
            case ALL_LOCATIONS:
                updateCount= db.update(LocationDb.SQLITE_TABLE, values, selection, selectionArgs);
                break;
            case ALL_CUSTOMERS:
                updateCount= db.update(CustomerDb.SQLITE_TABLE, values, selection, selectionArgs);
                break;
            case ALL_OFFERS:
                updateCount= db.update(OfferDb.SQLITE_TABLE, values, selection, selectionArgs);
                break;
            case ALL_DISTRICTS:
                updateCount= db.update(DistrictDb.SQLITE_TABLE, values, selection, selectionArgs);
                break;
            case ALL_STATES:
                updateCount= db.update(StateDb.SQLITE_TABLE, values, selection, selectionArgs);
                break;
            case ALL_COUNTRY:
                updateCount= db.update(CountryDb.SQLITE_TABLE, values, selection, selectionArgs);
                break;
            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }

        getContext().getContentResolver().notifyChange(uri, null);
        return updateCount;
    }

}
