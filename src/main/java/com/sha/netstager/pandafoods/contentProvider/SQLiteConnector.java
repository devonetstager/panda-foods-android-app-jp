package com.sha.netstager.pandafoods.contentProvider;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.sha.netstager.pandafoods.datatables.CartDb;
import com.sha.netstager.pandafoods.datatables.CatagoryDb;
import com.sha.netstager.pandafoods.datatables.CountryDb;
import com.sha.netstager.pandafoods.datatables.CustomerDb;
import com.sha.netstager.pandafoods.datatables.DistrictDb;
import com.sha.netstager.pandafoods.datatables.LocationDb;
import com.sha.netstager.pandafoods.datatables.OfferDb;
import com.sha.netstager.pandafoods.datatables.OrderDb;
import com.sha.netstager.pandafoods.datatables.OrderProductsDb;
import com.sha.netstager.pandafoods.datatables.ProductsDb;
import com.sha.netstager.pandafoods.datatables.StateDb;
import com.sha.netstager.pandafoods.datatables.WeightDb;

/**
 * Created by netstager on 17/12/14.
 */
public class SQLiteConnector extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "PandaFoods";
    private static final int DATABASE_VERSION = 1;
    private static SQLiteConnector sInstance;

    public SQLiteConnector(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public static SQLiteConnector getInstance(Context context) {
        // Use the application context, which will ensure that you
        // don't accidentally leak an Activity's context.
        // See this article for more information: http://bit.ly/6LRzfx
        if (sInstance == null) {
            sInstance = new SQLiteConnector(context.getApplicationContext());
        }
        return sInstance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        ProductsDb.onCreate(db);
        CatagoryDb.onCreate(db);
        OrderDb.onCreate(db);
        OrderProductsDb.onCreate(db);
        CartDb.onCreate(db);
        WeightDb.onCreate(db);
        LocationDb.onCreate(db);
        CustomerDb.onCreate(db);
        OfferDb.onCreate(db);
        DistrictDb.onCreate(db);
        StateDb.onCreate(db);
        CountryDb.onCreate(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        ProductsDb.onUpgrade(db, oldVersion, newVersion);
        CatagoryDb.onUpgrade(db, oldVersion, newVersion);
        OrderDb.onUpgrade(db, oldVersion, newVersion);
        OrderProductsDb.onUpgrade(db, oldVersion, newVersion);
        CartDb.onUpgrade(db, oldVersion, newVersion);
        WeightDb.onUpgrade(db, oldVersion, newVersion);
        LocationDb.onUpgrade(db, oldVersion, newVersion);
        CustomerDb.onUpgrade(db, oldVersion, newVersion);
        OfferDb.onUpgrade(db, oldVersion, newVersion);
        DistrictDb.onUpgrade(db, oldVersion, newVersion);
        StateDb.onUpgrade(db, oldVersion, newVersion);
        CountryDb.onUpgrade(db, oldVersion, newVersion);
    }

}
