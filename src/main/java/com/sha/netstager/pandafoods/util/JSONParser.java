package com.sha.netstager.pandafoods.util;

import android.text.TextUtils;
import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

public class JSONParser {
    static InputStream is = null;
    static JSONObject jObj = null;
    static String json = "";


    public JSONObject getJSONFromUrl(final String url) {

        // Making HTTP request
        try {
            // Construct the client and the HTTP request.
            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(url);

            // Execute the POST request and store the response locally.
            HttpResponse httpResponse = httpClient.execute(httpPost);
            // Extract data from the response.
            HttpEntity httpEntity = httpResponse.getEntity();
            // Open an inputStream with the data content.
            is = httpEntity.getContent();

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            // Create a BufferedReader to parse through the inputStream.
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    is, "iso-8859-1"), 8);
            // Declare a string builder to help with the parsing.
            StringBuilder sb = new StringBuilder();
            // Declare a string to store the JSON object data in string form.
            String line = null;

            // Build the string until null.
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }

            // Close the input stream.
            is.close();
            // Convert the string builder data to an actual string.
            json = sb.toString();
        } catch (Exception e) {
            Log.e("Buffer Error", "Error converting result " + e.toString());
        }

        // Try to parse the string to a JSON object
        try {
            jObj = new JSONObject(json);
        } catch (JSONException e) {
            Log.e("JSON Parser", "Error parsing data " + e.toString());
        }

        // Return the JSON Object.
        return jObj;

    }

    // function get json from url
    // by making HTTP POST or GET mehtod
    public static String makeHttpRequest(String url, String method, String request) {
        BufferedReader in = null;
        // Making HTTP request
        try {
            // JSONObject js=getJSONFromUrl(url)
            // check for request method
            if (method.equals("POST")) {
                // request method is POST
                // defaultHttpClient

                DefaultHttpClient httpClient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost(url.trim());
                httpPost.addHeader(new BasicHeader("Content-Type",
                        "application/json"));
                httpPost.addHeader(new BasicHeader("AUT",AppConstants.APIKEY));
                // httpPost.setParams()
                // httpPost.setEntity(new
                // UrlEncodedFormEntity(request,"UTF-8"));
                if (!TextUtils.isEmpty(request)){
                    httpPost.setEntity(new StringEntity(request));
                }
                // httpPost.setEntity(new StringEntity(
                HttpResponse httpResponse = httpClient.execute(httpPost);
                //long lenghtOfFile = httpResponse.getEntity().getContentLength();
                // HttpEntity httpEntity = httpResponse.getEntity();
                in = new BufferedReader(new InputStreamReader(httpResponse
                        .getEntity().getContent()));
                Log.i("posting", "postt");

            } else if (method.equals("GET")) {
                // request method is GET
                DefaultHttpClient httpClient = new DefaultHttpClient();
                String paramString = ""; /*= URLEncodedUtils.format(request, "utf-8");*/
                url += "?" + paramString;
                HttpGet httpGet = new HttpGet(url);

                HttpResponse httpResponse = httpClient.execute(httpGet);
                in = new BufferedReader(new InputStreamReader(httpResponse
                        .getEntity().getContent()));
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            // BufferedReader reader = new BufferedReader(new InputStreamReader(
            // is, "iso-8859-1"), 8);
            // StringBuilder sb = new StringBuilder();
            // String line = null;
            // while ((line = reader.readLine()) != null) {
            // sb.append(line + "\n");
            // }
            // is.close();
            // json = sb.toString();

            StringBuffer sb = new StringBuffer("");
            String line = "";
            String NL = System.getProperty("line.separator");
            while ((line = in.readLine()) != null) {
                sb.append(line + NL);
            }
            in.close();
            String result = sb.toString();
            //  Log.i("json", result);
            return result;
        } catch (Exception e) {
            Log.e("Buffer Error", "Error converting result " + e.toString());
        }

        // try parse the string to a JSON object
        // try {
        // jObj = new JSONObject(json);
        // } catch (JSONException e) {
        // Log.e("JSON Parser", "Error parsing data " + e.toString());
        // }

        // return JSON String
        Log.i("json", "nothing to return");
        return "nothing";

    }
}