package com.sha.netstager.pandafoods.util;

/**
 * Created by netstager on 08/01/15.
 */
public class AppConstants {
    public static final int TYPE_USER_LOGIN =2;
    public static final int TYPE_CUSTOMER_LOGIN =1;

    //urls of api
    public static final String URL_SERVER="http://orders.pandafoods.co.in/api/";
//  public static final String URL_SERVER="http://projectsvn.com/pandafoods/api/";
    public static final String URL_LOGIN=URL_SERVER+"login/checkuser";
    public static final String URL_SINK=URL_SERVER+"sink/sinkapp/";
    public static final String URL_REG_DATA_FETCH=URL_SERVER+"registration/registrationapp/";
    public static final String URL_SIGN_UP=URL_SERVER+"signup/signupapp/";
    public static final String URL_EDIT_PROFILE=URL_SERVER+"editprofile/editprofileapp";
    public static final String URL_ORDER=URL_SERVER+"order/orderdetails/";
    public static final String URL_CUSTOMER_SINK=URL_SERVER+"customer/customerapp/";
    public static final String URL_ADD_CUSTOMER=URL_SERVER+"addcustomer/addcustomerapp/";
    public static final String URL_PASSWORD_RESET=URL_SERVER+"forgotpassword/forgotpasswordapp/";
    public static final String URL_PREDATA_FETCH=URL_SERVER+"predata/predataapp";

    //shared preference
    public static final String PANDA_PREFERENCES="panda_app_preference";
    //Preference
    public static final String PREFERENCES_USER_MODE="user_mode";

    //User preferences
    public static final String PREFERENCES_USER_ID="cust_id";
    public static final String PREFERENCES_USER_TYPE="cust_type_id";
    public static final String PREFERENCES_USER_LOCATION="loc_id";
    public static final String PREFERENCES_USER_PHONE="cust_phone";
    public static final String PREFERENCES_USER_EMAIL="cust_email";
    public static final String PREFERENCES_USER_NAME="name";
    public static final String PREFERENCES_USER_ADDRESS="cust_address";
    public static final String PREFERENCES_USER_GROUP_ID="cust_group_id";

    //Executive Preferences
    public static final String PREFERENCES_EXE_ID="exe_id";
    public static final String PREFERENCES_EXE_NAME="name";
    public static final String PREFERENCES_EXE_LOCATION="exe_location";
    public static final String PREFERENCES_EXE_EMAIL="exe_email";
    public static final String PREFERENCES_EXE_ADDRESS="exe_address";

    public static final String PREFERENCES_CUSTOMER_PRICE_TYPE = "preference_customer_price_type";

    public static final String APIKEY = "f535cc112c4fd53386e436afcf6436d9";

    public static final String logCheck="nonLoggedId";
    public static final String cartcount="cartCount";
    public static final String xShop="xshop";


}
