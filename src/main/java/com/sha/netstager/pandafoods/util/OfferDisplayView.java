package com.sha.netstager.pandafoods.util;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sha.netstager.pandafoods.R;

/**
 * Created by netstager on 16/02/15.
 */
public class OfferDisplayView extends LinearLayout {
    private TextView productnameview;
    private TextView quantityview;
    private String productText="Prod";
    private String quantityText="0";
    public OfferDisplayView(Context context) {
        super(context);
        init();
    }
    public OfferDisplayView(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray a = context.obtainStyledAttributes(attrs,
                R.styleable.OfferDisplayView, 0, 0);
        productText = a.getString(R.styleable.OfferDisplayView_offername);
        quantityText = a.getString(R.styleable.OfferDisplayView_quantity);
        a.recycle();
        init();
    }

    public void setProductText(String productText) {
        productnameview.setText(productText);
        this.productText = productText;
    }

    public void setQuantityText(String quantityText) {
        quantityview.setText(quantityText);
        this.quantityText = quantityText;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        productnameview.setText(productText);
        quantityview.setText(quantityText);
    }

    public OfferDisplayView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        TypedArray a = context.obtainStyledAttributes(attrs,
                R.styleable.OfferDisplayView, 0, 0);
        productText = a.getString(R.styleable.OfferDisplayView_offername);
        quantityText = a.getString(R.styleable.OfferDisplayView_quantity);
        a.recycle();
        init();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public OfferDisplayView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        TypedArray a = context.obtainStyledAttributes(attrs,
                R.styleable.OfferDisplayView, 0, 0);
        productText = a.getString(R.styleable.OfferDisplayView_offername);
        quantityText = a.getString(R.styleable.OfferDisplayView_quantity);
        a.recycle();
        init();
    }
    private void init() {
        inflate(getContext(), R.layout.view_offer_free_item, this);
        this.productnameview = (TextView)findViewById(R.id.free_productname);
        this.quantityview = (TextView)findViewById(R.id.free_productqty);
    }
}
