package com.sha.netstager.pandafoods.fragments;


import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sha.netstager.pandafoods.R;
import com.sha.netstager.pandafoods.contentProvider.LocalDbProvider;
import com.sha.netstager.pandafoods.datatables.CustomerDb;
import com.sha.netstager.pandafoods.util.AppConstants;

public class OrderAdressSetFragment extends Fragment {
    private SharedPreferences sharedPreferences;




    public OrderAdressSetFragment() {
    }
    public static OrderAdressSetFragment newInstance(String custId) {
        OrderAdressSetFragment fragment = new OrderAdressSetFragment();
        Bundle bundle=new Bundle();
        Log.i("In order fragment","dd");
        bundle.putString("custId",custId);
        fragment.setArguments(bundle);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        sharedPreferences = getActivity().getSharedPreferences(AppConstants.PANDA_PREFERENCES, Context.MODE_PRIVATE);
        String cID = sharedPreferences.getString(AppConstants.PREFERENCES_USER_ID, "0");
        String cname = sharedPreferences.getString(AppConstants.PREFERENCES_USER_NAME, "0");
        String cadd = sharedPreferences.getString(AppConstants.PREFERENCES_USER_ADDRESS, "0");

        Log.i("Cust Id",cID);
        Log.i("Cust name",cname);
        Log.i("Cust address",cadd);
        // Inflate the layout for this fragment
        View rootView=inflater.inflate(R.layout.fragment_order_adress_set, container, false);
        TextView custname = (TextView) rootView.findViewById(R.id.orderproducts_custname);
        TextView custid = (TextView) rootView.findViewById(R.id.orderproducts_custid);
        TextView address = (TextView) rootView.findViewById(R.id.orderproducts_address);
        Bundle bundle=getArguments();

        if (cID.equals("0")) {
            custid.setText(sharedPreferences.getString(AppConstants.PREFERENCES_USER_ID, "0"));
            custname.setText(sharedPreferences.getString(AppConstants.PREFERENCES_USER_NAME, "0"));
            address.setText(sharedPreferences.getString(AppConstants.PREFERENCES_USER_ADDRESS, "0"));
        }
        else {

            //String custId=bundle.getString("custId");

            Cursor cursor=getActivity().getContentResolver().query(LocalDbProvider.CUSOTMER_URI,new String[]{CustomerDb.CUST_NAME,CustomerDb.CUST_ADDRESS},CustomerDb.CUST_ID+"=?",new String[]{cID},null);
            if (cursor.moveToFirst()){
                custid.setText(cID);

                Log.i("Cust Id",cID);
                custname.setText(cname);
                address.setText(cadd);
            }
            else{
                Log.i("Cust Id Else",cID);
                custid.setText(cID);
                custname.setText(cname);
                address.setText(cadd);
            }

        }
        return rootView;
    }
}
