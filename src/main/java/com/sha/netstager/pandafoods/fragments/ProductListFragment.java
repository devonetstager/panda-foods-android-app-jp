package com.sha.netstager.pandafoods.fragments;

import android.app.Fragment;
import android.app.LoaderManager;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Loader;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CursorAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.sha.netstager.pandafoods.R;
import com.sha.netstager.pandafoods.activity.AddCustomerActivity;
import com.sha.netstager.pandafoods.activity.CartShowActivity;
import com.sha.netstager.pandafoods.activity.OrderedProductShowActivity;
import com.sha.netstager.pandafoods.activity.PandaActivity;
import com.sha.netstager.pandafoods.contentProvider.LocalDbProvider;
import com.sha.netstager.pandafoods.datatables.CartDb;
import com.sha.netstager.pandafoods.datatables.OrderProductsDb;
import com.sha.netstager.pandafoods.datatables.ProductsDb;
import com.sha.netstager.pandafoods.datatables.WeightDb;
import com.sha.netstager.pandafoods.util.AppConstants;

public class ProductListFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {

    private CartCursorAdapter dataAdapter;
    private ListView cartListView;
    private float totalcash=0;
    private int type=0;
    private String orderid;
    private MatrixCursor matrixcursor;
    private SharedPreferences sharedPreferences;

    public static ProductListFragment newInstance(int type,String orderid) {
        ProductListFragment fragment = new ProductListFragment();
        Bundle bundle=new Bundle();
        bundle.putInt("type",type);
        bundle.putString("orderid",orderid);
        fragment.setArguments(bundle);
        return fragment;
    }

    public ProductListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle= getArguments();
        sharedPreferences=getActivity().getSharedPreferences(AppConstants.PANDA_PREFERENCES,Context.MODE_PRIVATE);

        type=bundle.getInt("type");
        orderid= bundle.getString("orderid");



    }
    private void loadListView(){
        // the XML defined views which the data will be bound to
        dataAdapter= new CartCursorAdapter(getActivity().getApplicationContext(),null,0);
        cartListView.setAdapter(dataAdapter);
        //Ensures a loader is initialized and active.
        if (type==3){
            getLoaderManager().initLoader(1, null, this);
        }
        else {
            getLoaderManager().initLoader(0, null, this);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View fragmentView=inflater.inflate(R.layout.fragment_product_list, container, false);
        cartListView= (ListView) fragmentView.findViewById(R.id.cart_listview);
        loadListView();
        return fragmentView;
    }


        @Override
        public Loader<Cursor> onCreateLoader(int id, Bundle args) {
            if (id==1){
                String[] projection={
                        OrderProductsDb._ID,
                        OrderProductsDb.ORDER_ID,
                        OrderProductsDb.PROD_ID,
                        OrderProductsDb.PROD_QUANTITY
                };
                return new CursorLoader(getActivity().getApplicationContext(),LocalDbProvider.ORDER_PRODUCT_URI,projection,OrderProductsDb.ORDER_ID+"=?",new String[]{orderid},null);
            }
            String[] projection = {
                    CartDb._ID,
                    CartDb.PROD_ID,
                    CartDb.PROD_QUANTITY,
            };

            String loggedCheck=sharedPreferences.getString(AppConstants.logCheck,"0");

            Log.i("Log in check value", loggedCheck);

            if (loggedCheck.equals("0"))
            {
                return new CursorLoader(getActivity().getApplicationContext(),
                        LocalDbProvider.CART_URI, projection, CartDb.USER_ID+"=?", new String[]{loggedCheck}, null);
            }
            else
            {
                String userid=sharedPreferences.getString(AppConstants.PREFERENCES_USER_ID,"0");

                Log.i("User Id in Fragment", userid);

                return new CursorLoader(getActivity().getApplicationContext(),
                        LocalDbProvider.CART_URI, projection, CartDb.USER_ID+"=?", new String[]{userid}, null);
            }


        }

        @Override
        public void onLoadFinished(Loader<Cursor> loader, Cursor data) {

            int count= data.getCount();
            SharedPreferences.Editor editor=sharedPreferences.edit();
            editor.putString("cartCount", String.valueOf(count));
            editor.apply();


            String userid=sharedPreferences.getString(AppConstants.PREFERENCES_USER_ID,"0");
            MatrixCursor matrixCursor=new MatrixCursor(new String[] {ProductsDb._ID,ProductsDb.PROD_ID,ProductsDb.PROD_NAME,ProductsDb.PROD_WEIGHT_ID,ProductsDb.PROD_WEIGHT,ProductsDb.PROD_UNIT,CartDb.PROD_QUANTITY,ProductsDb.PROD_CATEGORY,ProductsDb.PROD_GROUP,sharedPreferences.getString(AppConstants.PREFERENCES_CUSTOMER_PRICE_TYPE,ProductsDb.PROD_CFSTOCKIST_PRICE)});
            if(data.moveToFirst()){
                do{
                    String prod_id = data.getString(data.getColumnIndexOrThrow(CartDb.PROD_ID));
                    String quantity = data.getString(data.getColumnIndexOrThrow(CartDb.PROD_QUANTITY));
                    String[] projection = {
                            ProductsDb._ID,
                            ProductsDb.PROD_ID,
                            ProductsDb.PROD_NAME,
                            ProductsDb.PROD_GROUP,
                            ProductsDb.PROD_WEIGHT_ID,
                            ProductsDb.PROD_WEIGHT,
                            ProductsDb.PROD_UNIT,
                            sharedPreferences.getString(AppConstants.PREFERENCES_CUSTOMER_PRICE_TYPE,ProductsDb.PROD_CFSTOCKIST_PRICE),
                            ProductsDb.PROD_CATEGORY
                    };
                    Cursor dataCursor=getActivity().getContentResolver().query(LocalDbProvider.CONTENT_URI, projection, ProductsDb.PROD_ID + "=? ", new String[]{prod_id}, null);
                    dataCursor.moveToFirst();
                    String _id=dataCursor.getString(dataCursor.getColumnIndexOrThrow(ProductsDb._ID));
                    String prod_name=dataCursor.getString(dataCursor.getColumnIndexOrThrow(ProductsDb.PROD_NAME));
                    String prod_prize=dataCursor.getString(dataCursor.getColumnIndexOrThrow(sharedPreferences.getString(AppConstants.PREFERENCES_CUSTOMER_PRICE_TYPE,ProductsDb.PROD_CFSTOCKIST_PRICE)));
                    String prod_catagory= dataCursor.getString(dataCursor.getColumnIndexOrThrow(ProductsDb.PROD_CATEGORY));
                    String prod_group= dataCursor.getString(dataCursor.getColumnIndexOrThrow(ProductsDb.PROD_GROUP));
                    String prod_weight= dataCursor.getString(dataCursor.getColumnIndexOrThrow(ProductsDb.PROD_WEIGHT_ID));
                    String prod_weight_details= dataCursor.getString(dataCursor.getColumnIndexOrThrow(ProductsDb.PROD_WEIGHT));
                    String prod_weight_unit= dataCursor.getString(dataCursor.getColumnIndexOrThrow(ProductsDb.PROD_UNIT));
                    matrixCursor.addRow(new String[]{_id,prod_id,prod_name,prod_weight,prod_weight_details,prod_weight_unit,quantity,prod_catagory,prod_group,prod_prize});
                    dataCursor.close();
                }while (data.moveToNext());
            }
            data.close();
            processTotal(matrixCursor);
            this.matrixcursor=matrixCursor;
            dataAdapter.swapCursor(matrixCursor);
        }


        private void processTotal(MatrixCursor newCursor) {
            totalcash=0;

            while (newCursor.moveToNext()){
                float amountinrow=Float.parseFloat(newCursor.getString(newCursor.getColumnIndexOrThrow(sharedPreferences.getString(AppConstants.PREFERENCES_CUSTOMER_PRICE_TYPE,ProductsDb.PROD_CFSTOCKIST_PRICE))));
                int quantityinrow= Integer.parseInt(newCursor.getString(newCursor.getColumnIndexOrThrow(CartDb.PROD_QUANTITY)));
                float totalinrow=amountinrow*quantityinrow;
                totalcash=totalcash+totalinrow;
            }
            newCursor.close();
            switch (type){
                case 1:
                    ((CartShowActivity)getActivity()).totalFragmentDisplay(totalcash);
                    break;
                case 3:
                    ((OrderedProductShowActivity)getActivity()).totalFragmentDisplay(totalcash);
                    break;
            }

        }


    @Override
        public void onLoaderReset(Loader<Cursor> loader) {
            dataAdapter.swapCursor(null);
        }

    public MatrixCursor getCursor() {
        return matrixcursor;
    }

    public void refreshData() {
        getActivity().getLoaderManager().restartLoader(0,null,this);
        dataAdapter.notifyDataSetChanged();
    }

    private class CartCursorAdapter extends CursorAdapter {

        private LayoutInflater mInflater;
        private Context context;


        public CartCursorAdapter(Context context, Cursor c, int flags) {
            super(context, c, flags);
            this.context = context;

            mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public void bindView(View view, Context context, final Cursor cursor) {

            // TextView prod_id = (TextView) view.findViewById(R.id.product_id);
            TextView prod_name = (TextView) view.findViewById(R.id.offer_description);
            TextView prod_price = (TextView) view.findViewById(R.id.offer_title);
            TextView qty = (TextView) view.findViewById(R.id.promo_code);
            // prod_id.setText(cursor.getString(cursor.getColumnIndexOrThrow(ProductsDb.PROD_ID)));

            //retrieve weight
            String weightID = cursor.getString(cursor.getColumnIndexOrThrow(ProductsDb.PROD_WEIGHT_ID));
            String[] projection = new String[]{
                    WeightDb._ID,
                    WeightDb.WEIGHT_ID,
                    WeightDb.PROD_WEIGHT,
                    WeightDb.PROD_UNIT
            };
            Cursor weighCursor = context.getContentResolver().query(LocalDbProvider.WEIGHT_URI, projection, WeightDb.WEIGHT_ID + "=?", new String[]{weightID}, null);
            String productWeight = null, weightUnit = null;
            if (weighCursor.moveToFirst()) {
                productWeight = weighCursor.getString(weighCursor.getColumnIndexOrThrow(WeightDb.PROD_WEIGHT));
                weightUnit = weighCursor.getString(weighCursor.getColumnIndexOrThrow(WeightDb.PROD_UNIT));
            }
            //set product name
            prod_name.setText(cursor.getString(cursor.getColumnIndexOrThrow(ProductsDb.PROD_NAME)) + " " + productWeight + weightUnit);
            prod_price.setText("\u20B9" + cursor.getString(cursor.getColumnIndexOrThrow(sharedPreferences.getString(AppConstants.PREFERENCES_CUSTOMER_PRICE_TYPE, ProductsDb.PROD_CFSTOCKIST_PRICE))));
            qty.setText(cursor.getString(cursor.getColumnIndexOrThrow(CartDb.PROD_QUANTITY)));

        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View view = super.getView(position, convertView, parent);

            Button edit_btn = (Button) view.findViewById(R.id.product_btn_edit);
            Log.i("type value", String.valueOf(type));
            if (type != 1) {


                edit_btn.setVisibility(View.INVISIBLE);
                edit_btn.setActivated(false);
            } else {
                edit_btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Cursor c = (Cursor) getItem(position);
                        String prod_id = c.getString(c.getColumnIndexOrThrow(ProductsDb.PROD_ID));
                        AddCustomerActivity.AddToCartFragment cartFragment = AddCustomerActivity.AddToCartFragment.getInstance(prod_id);
                        FragmentManager fragmentManager = ((PandaActivity) getActivity()).getSupportFragmentManager();
                        cartFragment.show(fragmentManager, "Edit Cart");
                    }
                });
            }
            return view;
        }

        @Override
        public View newView(Context context, Cursor cursor, ViewGroup parent) {
            return mInflater.inflate(R.layout.view_cart_list_item, parent, false);
        }
    }
}
