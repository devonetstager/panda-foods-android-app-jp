package com.sha.netstager.pandafoods.fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ProgressBar;

import com.sha.netstager.pandafoods.R;
import com.sha.netstager.pandafoods.activity.Grid_Home_Activity;
import com.sha.netstager.pandafoods.activity.SubCatagoryActivity;
import com.sha.netstager.pandafoods.adapters.CatagoryCursorAdapter;
import com.sha.netstager.pandafoods.contentProvider.LocalDbProvider;
import com.sha.netstager.pandafoods.datatables.CatagoryDb;
import com.sha.netstager.pandafoods.services.SyncService;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link GridFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class GridFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    ProgressDialog mProgressDialog;

    private CatagoryCursorAdapter dataAdapter;
    private GridView catListView;
    Activity activity = getActivity();
    private ProgressBar product_availability_view;
    private SharedPreferences sharedPreferences;

    //private OnFragmentInteractionListener mListener;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment GridFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static GridFragment newInstance(String param1, String param2) {
        GridFragment fragment = new GridFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public GridFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        //return inflater.inflate(R.layout.fragment_grid, container, false);
        //myFragmentView inflater.inflate(R.layout.activity_product_catagory, container, false);
        //View rootView = inflater.inflate(R.layout.activity_product_catagory, container,
        View rootView = inflater.inflate(R.layout.fragment_grid, container,
                false);


        catListView = (GridView) rootView.findViewById(R.id.catagory_list_view);
        product_availability_view= (ProgressBar) rootView.findViewById(R.id.product_availability_display);
        //by bsr
        product_availability_view.setVisibility(View.GONE);
        //by bsr
        loadListView();

        return rootView;
    }

    private void loadListView() {


        dataAdapter= new CatagoryCursorAdapter(getActivity().getApplicationContext(),null,0);
        catListView.setAdapter(dataAdapter);

        catListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //when clicking on an item, retrieve cursor data at that position,then obtain category id of that particular category, and pass into SubCatagoryActivity and launch it
                Cursor cursor = (Cursor) catListView.getItemAtPosition(position);
                Intent intent=new Intent(getActivity().getApplicationContext(),SubCatagoryActivity.class);
                String cat_id=cursor.getString(cursor.getColumnIndexOrThrow(CatagoryDb.CAT_ID));
                //intent.putExtra(CatagoryDb.CAT_ID,cat_id);
                intent.putExtra(CatagoryDb.CAT_ID,cat_id);
                startActivity(intent);
            }
        });
        getLoaderManager().initLoader(0, null, this);
    }


    public static GridFragment newInstance(String param1) {
        GridFragment fragment = new GridFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onLoadFinished(android.support.v4.content.Loader<Cursor> loader, Cursor data) {
        dataAdapter.swapCursor(data);
        if (!data.moveToFirst()) {

            product_availability_view.setVisibility(View.VISIBLE);
            //by bsr
            Intent productIntent = new Intent(getActivity(), Grid_Home_Activity.class);
            getActivity().finish();
            getActivity().finish();
            startActivity(productIntent);
            //by bsr
//            product_list_view.setVisibility(View.GONE);
            SyncService s=new SyncService();

        }

    }

    @Override
    public void onLoaderReset(android.support.v4.content.Loader<Cursor> loader) {
        dataAdapter.swapCursor(null);
    }


    //this loader class contact with database with the help of content provider class
    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        String[] projection = {
                CatagoryDb._ID,
                CatagoryDb.CAT_ID,
                CatagoryDb.CAT_NAME,
                CatagoryDb.CAT_IMAGE,
                CatagoryDb.CAT_DESCRIPTION
        };
        return new CursorLoader(getActivity().getApplicationContext(),
                LocalDbProvider.CATAGORY_URI, projection, CatagoryDb.PARENT_CAT+"=?", new String[]{"0"}, null);
    }


}
