package com.sha.netstager.pandafoods.fragments;


import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sha.netstager.pandafoods.R;
import com.sha.netstager.pandafoods.activity.OrderProductsActivity;
import com.sha.netstager.pandafoods.contentProvider.LocalDbProvider;
import com.sha.netstager.pandafoods.datatables.CartDb;
import com.sha.netstager.pandafoods.datatables.ProductsDb;
import com.sha.netstager.pandafoods.util.AppConstants;

import java.text.DecimalFormat;

/**
 * A simple {@link Fragment} subclass.
 */
public class TotalBillFragment extends Fragment {

private TextView totalamount,grandtotal,discountView,deductamount;
private float finalcash=0;

private MatrixCursor productCursor;
private int type=0;
    private SharedPreferences sharedPreferences;

    public TotalBillFragment(){
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            type = getArguments().getInt("type");
        }
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        sharedPreferences= getActivity().getSharedPreferences(AppConstants.PANDA_PREFERENCES,Context.MODE_PRIVATE);
        View rootView=inflater.inflate(R.layout.fragment_total_bill, container, false);
        TextView cust_id = (TextView) rootView.findViewById(R.id.cart_cust_id);
        totalamount= (TextView) rootView.findViewById(R.id.cart_total_amount);
        grandtotal = (TextView) rootView.findViewById(R.id.cart_grand_total);
        discountView = (TextView) rootView.findViewById(R.id.cart_discount);
        deductamount = (TextView) rootView.findViewById(R.id.cart_discount_amount);
        cust_id.setText(sharedPreferences.getString("cust_id", "0"));
        totalamount.setText("");
        grandtotal.setText("");
        discountView.setText("0%");
        loadCursor();
        return rootView;
    }

    public void setFinalcash(float totalcash, float discount){
        grandtotal.setText("\u20B9"+totalcash+"");
        float deduted= finalcash-totalcash;
        this.finalcash=totalcash;
        discountView.setText(discount+"%");
        DecimalFormat twoDForm = new DecimalFormat("#.##");
        deductamount.setText("-"+Float.valueOf(twoDForm.format(deduted)));
    }


    public void setTotalcash(float totalcash){
        this.finalcash=totalcash;
        totalamount.setText("\u20B9" +totalcash + "");
        grandtotal.setText("\u20B9" +totalcash+"");
    }


    public static TotalBillFragment newInstance(int type) {
        TotalBillFragment fragment = new TotalBillFragment();
        Bundle args = new Bundle();
        args.putInt("type", type);
        fragment.setArguments(args);
        return fragment;
    }

    private void loadCursor(){
        String[] projection = {
                CartDb._ID,
                CartDb.PROD_ID,
                CartDb.PROD_QUANTITY,
        };

        String loggedId=sharedPreferences.getString(AppConstants.logCheck,"0");
        String userid=sharedPreferences.getString(AppConstants.PREFERENCES_USER_ID,"0");
        if(loggedId.equals("0"))
        {
            Cursor data= getActivity().getContentResolver().query(
                    LocalDbProvider.CART_URI, projection, CartDb.USER_ID + "=?", new String[]{loggedId}, null);
            MatrixCursor matrixCursor=new MatrixCursor(new String[] {ProductsDb._ID,ProductsDb.PROD_ID,ProductsDb.PROD_NAME,ProductsDb.PROD_WEIGHT_ID,ProductsDb.PROD_WEIGHT,ProductsDb.PROD_UNIT,CartDb.PROD_QUANTITY,ProductsDb.PROD_CATEGORY,ProductsDb.PROD_SUB_CATEGORY,ProductsDb.PROD_GROUP,sharedPreferences.getString(AppConstants.PREFERENCES_CUSTOMER_PRICE_TYPE,ProductsDb.PROD_CFSTOCKIST_PRICE)});
            if(data.moveToFirst()){
                do{
                    String prod_id = data.getString(data.getColumnIndexOrThrow(CartDb.PROD_ID));
                    String quantity = data.getString(data.getColumnIndexOrThrow(CartDb.PROD_QUANTITY));
                    String[] projectionforProd = {
                            ProductsDb._ID,
                            ProductsDb.PROD_ID,
                            ProductsDb.PROD_NAME,
                            ProductsDb.PROD_GROUP,
                            ProductsDb.PROD_WEIGHT_ID,
                            ProductsDb.PROD_WEIGHT,
                            ProductsDb.PROD_UNIT,
                            sharedPreferences.getString(AppConstants.PREFERENCES_CUSTOMER_PRICE_TYPE,ProductsDb.PROD_CFSTOCKIST_PRICE),
                            ProductsDb.PROD_CATEGORY,
                            ProductsDb.PROD_SUB_CATEGORY
                    };
                    Cursor dataCursor=getActivity().getContentResolver().query(LocalDbProvider.CONTENT_URI, projectionforProd, ProductsDb.PROD_ID + "=? ", new String[]{prod_id}, null);
                    dataCursor.moveToFirst();
                    String _id=dataCursor.getString(dataCursor.getColumnIndexOrThrow(ProductsDb._ID));
                    String prod_name=dataCursor.getString(dataCursor.getColumnIndexOrThrow(ProductsDb.PROD_NAME));
                    String prod_prize=dataCursor.getString(dataCursor.getColumnIndexOrThrow(sharedPreferences.getString(AppConstants.PREFERENCES_CUSTOMER_PRICE_TYPE,ProductsDb.PROD_CFSTOCKIST_PRICE)));
                    String prod_catagory= dataCursor.getString(dataCursor.getColumnIndexOrThrow(ProductsDb.PROD_CATEGORY));
                    String prod_sub_catagory= dataCursor.getString(dataCursor.getColumnIndexOrThrow(ProductsDb.PROD_SUB_CATEGORY));
                    String prod_group= dataCursor.getString(dataCursor.getColumnIndexOrThrow(ProductsDb.PROD_GROUP));
                    String prod_weight= dataCursor.getString(dataCursor.getColumnIndexOrThrow(ProductsDb.PROD_WEIGHT_ID));
                    String prod_weight_details= dataCursor.getString(dataCursor.getColumnIndexOrThrow(ProductsDb.PROD_WEIGHT));
                    String prod_weight_unit= dataCursor.getString(dataCursor.getColumnIndexOrThrow(ProductsDb.PROD_UNIT));
                    matrixCursor.addRow(new String[]{_id,prod_id,prod_name,prod_weight,prod_weight_details,prod_weight_unit,quantity,prod_catagory,prod_sub_catagory,prod_group,prod_prize});
                    dataCursor.close();
                }while (data.moveToNext());
            }
            data.close();
            processTotal(matrixCursor);
            setProductCursor(matrixCursor);
        }
        else {

            Cursor data = getActivity().getContentResolver().query(
                    LocalDbProvider.CART_URI, projection, CartDb.USER_ID + "=?", new String[]{userid}, null);
            MatrixCursor matrixCursor = new MatrixCursor(new String[]{ProductsDb._ID, ProductsDb.PROD_ID, ProductsDb.PROD_NAME, ProductsDb.PROD_WEIGHT_ID, ProductsDb.PROD_WEIGHT, ProductsDb.PROD_UNIT, CartDb.PROD_QUANTITY, ProductsDb.PROD_CATEGORY, ProductsDb.PROD_SUB_CATEGORY, ProductsDb.PROD_GROUP, sharedPreferences.getString(AppConstants.PREFERENCES_CUSTOMER_PRICE_TYPE, ProductsDb.PROD_CFSTOCKIST_PRICE)});
            if (data.moveToFirst()) {
                do {
                    String prod_id = data.getString(data.getColumnIndexOrThrow(CartDb.PROD_ID));
                    String quantity = data.getString(data.getColumnIndexOrThrow(CartDb.PROD_QUANTITY));
                    String[] projectionforProd = {
                            ProductsDb._ID,
                            ProductsDb.PROD_ID,
                            ProductsDb.PROD_NAME,
                            ProductsDb.PROD_GROUP,
                            ProductsDb.PROD_WEIGHT_ID,
                            ProductsDb.PROD_WEIGHT,
                            ProductsDb.PROD_UNIT,
                            sharedPreferences.getString(AppConstants.PREFERENCES_CUSTOMER_PRICE_TYPE, ProductsDb.PROD_CFSTOCKIST_PRICE),
                            ProductsDb.PROD_CATEGORY,
                            ProductsDb.PROD_SUB_CATEGORY
                    };
                    Cursor dataCursor = getActivity().getContentResolver().query(LocalDbProvider.CONTENT_URI, projectionforProd, ProductsDb.PROD_ID + "=? ", new String[]{prod_id}, null);
                    dataCursor.moveToFirst();
                    String _id = dataCursor.getString(dataCursor.getColumnIndexOrThrow(ProductsDb._ID));
                    String prod_name = dataCursor.getString(dataCursor.getColumnIndexOrThrow(ProductsDb.PROD_NAME));
                    String prod_prize = dataCursor.getString(dataCursor.getColumnIndexOrThrow(sharedPreferences.getString(AppConstants.PREFERENCES_CUSTOMER_PRICE_TYPE, ProductsDb.PROD_CFSTOCKIST_PRICE)));
                    String prod_catagory = dataCursor.getString(dataCursor.getColumnIndexOrThrow(ProductsDb.PROD_CATEGORY));
                    String prod_sub_catagory = dataCursor.getString(dataCursor.getColumnIndexOrThrow(ProductsDb.PROD_SUB_CATEGORY));
                    String prod_group = dataCursor.getString(dataCursor.getColumnIndexOrThrow(ProductsDb.PROD_GROUP));
                    String prod_weight = dataCursor.getString(dataCursor.getColumnIndexOrThrow(ProductsDb.PROD_WEIGHT_ID));
                    String prod_weight_details = dataCursor.getString(dataCursor.getColumnIndexOrThrow(ProductsDb.PROD_WEIGHT));
                    String prod_weight_unit = dataCursor.getString(dataCursor.getColumnIndexOrThrow(ProductsDb.PROD_UNIT));
                    matrixCursor.addRow(new String[]{_id, prod_id, prod_name, prod_weight, prod_weight_details, prod_weight_unit, quantity, prod_catagory, prod_sub_catagory, prod_group, prod_prize});
                    dataCursor.close();
                } while (data.moveToNext());
            }
            data.close();
            processTotal(matrixCursor);
            setProductCursor(matrixCursor);
        }

    }

    public void setProductCursor(MatrixCursor productCursor) {
        this.productCursor = productCursor;
    }

    public MatrixCursor getProductCursor() {
        return productCursor;
    }

    private void processTotal(Cursor newCursor) {
        float totalcash=0;
        while (newCursor.moveToNext()){
            float amountinrow=Float.parseFloat(newCursor.getString(newCursor.getColumnIndexOrThrow(sharedPreferences.getString(AppConstants.PREFERENCES_CUSTOMER_PRICE_TYPE, ProductsDb.PROD_CFSTOCKIST_PRICE))));
            int quantityinrow= Integer.parseInt(newCursor.getString(newCursor.getColumnIndexOrThrow(CartDb.PROD_QUANTITY)));
            float totalinrow=amountinrow*quantityinrow;
            totalcash=totalcash+totalinrow;
        }
        newCursor.close();
        Log.i("cash",totalcash+"");
        setTotalcash(totalcash);
        if (type==2){
            ((OrderProductsActivity)getActivity()).setTotalcash(totalcash);
        }
    }

    public String getfinalCash() {
        return finalcash+"";
    }
}
