package com.sha.netstager.pandafoods.fragments;


import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.sha.netstager.pandafoods.R;
import com.sha.netstager.pandafoods.adapters.OfferCursorAdapter;
import com.sha.netstager.pandafoods.contentProvider.LocalDbProvider;
import com.sha.netstager.pandafoods.datatables.OfferDb;
import com.sha.netstager.pandafoods.util.AppConstants;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link OfferListFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class OfferListFragment extends Fragment implements  LoaderManager.LoaderCallbacks<Cursor> {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private SharedPreferences sharedPreferences;
    private OfferCursorAdapter dataAdapter;

    // TODO: Rename and change types of parameters
    private int mParam1;
    private String mParam2;


    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment OfferListFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static OfferListFragment newInstance(int param1, String param2) {
        OfferListFragment fragment = new OfferListFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public OfferListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getInt(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view=inflater.inflate(R.layout.fragment_offer_list, container, false);
        sharedPreferences=getActivity().getSharedPreferences(AppConstants.PANDA_PREFERENCES, Context.MODE_PRIVATE);
        loadListView(view);
        return view;
    }


    private void loadListView(View view){
        // the XML defined views which the data will be bound to
        ListView cartListView = (ListView)view.findViewById(R.id.cart_listview);
        dataAdapter= new OfferCursorAdapter(getActivity().getApplicationContext(),null,0);
        cartListView.setAdapter(dataAdapter);
        //Ensures a loader is initialized and active.

        getLoaderManager().initLoader(0, null,this);
    }



    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        String[] projection={
                OfferDb._ID,
                OfferDb.OFFER_ID,
                OfferDb.OFFER_DESCRIPTION,
                OfferDb.PROMO_CODE,
                OfferDb.OFFER_TITLE,
                OfferDb.OFFER_JSON,
                OfferDb.OFFER_TYPE
        };
        String offerTypeQuery;
        if (mParam1==1){
            offerTypeQuery="value";
        }
        else offerTypeQuery="product";

        return new CursorLoader(getActivity().getApplicationContext(), LocalDbProvider.OFFER_URI,projection,OfferDb.OFFER_TYPE+"=?",new String[]{offerTypeQuery},null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        dataAdapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        dataAdapter.swapCursor(null);
    }


}
