package com.sha.netstager.pandafoods.activity;

    import android.content.Context;
    import android.content.Intent;
    import android.content.SharedPreferences;
    import android.os.Bundle;
    import android.support.v4.view.MenuItemCompat;
    import android.support.v7.app.ActionBar;
    import android.support.v7.app.ActionBarActivity;
    import android.util.Log;
    import android.view.Menu;
    import android.view.MenuItem;
    import android.view.View;
    import android.widget.Button;
    import android.widget.RelativeLayout;
    import android.widget.TextView;

    import com.sha.netstager.pandafoods.R;
    import com.sha.netstager.pandafoods.util.AppConstants;

    /**
     * Created by netstager on 12/01/15.
     */

//parent class for all activities which have a common action bar
    public class CartActionBar extends ActionBarActivity {

        TextView notifCount;
        RelativeLayout count;
        int mNotifCount = 0;
        private SharedPreferences sharedPreferences;

        protected ActionBar actionBar;
        @Override
        protected void onCreate(Bundle savedInstanceState) {

            super.onCreate(savedInstanceState);
            actionBar = getSupportActionBar();
            actionBar.setHomeButtonEnabled(false);
            actionBar.setDisplayShowCustomEnabled(true);
            actionBar.setBackgroundDrawable(getResources().getDrawable(R.drawable.topbar));
            actionBar.setHomeAsUpIndicator(getResources().getDrawable(R.drawable.backbutton));

            sharedPreferences=this.getSharedPreferences(AppConstants.PANDA_PREFERENCES, Context.MODE_PRIVATE);

        }

        @Override
        public boolean onOptionsItemSelected(MenuItem item) {
            //if (item.getItemId() == R.id.badge) {
//        if (item.getItemId() == R.id.action_cart) {
//            Intent cartIntent=new Intent(getApplicationContext(),CartShowActivity.class);
//            startActivity(cartIntent);
//            return true;
//        }
            return super.onOptionsItemSelected(item);


        }

        @Override
        public boolean onCreateOptionsMenu(Menu menu) {
            // Inflate the menu; this adds items to the action bar if it is present.
            getMenuInflater().inflate(R.menu.menu_panda, menu);
            RelativeLayout rl_viewBag = (RelativeLayout) menu.findItem(R.id.badge).getActionView();
            String cartCount=sharedPreferences.getString(AppConstants.cartcount,"0");
            Log.i("Cart Value", cartCount);
            notifCount = (TextView)rl_viewBag.findViewById(R.id.actionbar_notifcation_textview);
            notifCount.setText(cartCount);

            rl_viewBag.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent cartIntent = new Intent(getApplicationContext(), CartShowActivity.class);
                    startActivity(cartIntent);
                }
            });
            return true;
        }

    }

