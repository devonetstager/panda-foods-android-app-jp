package com.sha.netstager.pandafoods.activity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.InputType;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.sha.netstager.pandafoods.R;
import com.sha.netstager.pandafoods.datatables.ProductsDb;
import com.sha.netstager.pandafoods.services.SyncService;
import com.sha.netstager.pandafoods.util.AppConstants;
import com.sha.netstager.pandafoods.util.JSONParser;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * A login screen that offers login via email/password and via Google+ sign in.
 * <p/>
 * ************ IMPORTANT SETUP NOTES: ************
 * In order for Google+ sign in to work with your app, you must first go to:
 * https://developers.google.com/+/mobile/android/getting-started#step_1_enable_the_google_api
 * and follow the steps in "Step 1" to create an OAuth 2.0 client for your package.
 */
public class LoginAuthActivity extends Activity {


    /**
     * Keep track of the login task to ensure we can cancel it if requested.
     */
    private UserLoginTask mAuthTask = null;
    private int login_type;
    // UI references.
    private AutoCompleteTextView mEmailView;
    private EditText mPasswordView;
    private View mProgressView;
    private SharedPreferences sharedPreferences;



    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        sharedPreferences = getSharedPreferences(AppConstants.PANDA_PREFERENCES, Context.MODE_PRIVATE);

        // Set up the login form.
        TextView forgotpwdView= (TextView) findViewById(R.id.login_forgot_password);
        mEmailView = (AutoCompleteTextView) findViewById(R.id.login_email);
        ArrayList<String> autocompleteArray=new ArrayList<>();
        if (!TextUtils.isEmpty(sharedPreferences.getString("EmailTag",""))) {
            autocompleteArray.add(sharedPreferences.getString("EmailTag", ""));
            mEmailView.setText(sharedPreferences.getString("EmailTag", ""));
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, autocompleteArray);
            mEmailView.setAdapter(adapter);
        }
        mPasswordView = (EditText) findViewById(R.id.login_password);
        Button mEmailSignInButton = (Button) findViewById(R.id.login_sign_in_button);
        Button mSignUpButton = (Button) findViewById(R.id.login_sign_up_button);
        mProgressView = findViewById(R.id.login_progress);

        //attempt login after entered username and password
        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    ConnectivityManager cm =
                            (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

                    //starting login attempt while network is connected
                    NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
                    boolean isConnected = activeNetwork != null &&
                            activeNetwork.isConnectedOrConnecting();
                    if (isConnected) {
                        attemptLogin();
                    }else {
                        Toast.makeText(LoginAuthActivity.this,"Connection Error!",Toast.LENGTH_SHORT).show();
                    }

                    return true;
                }
                return false;
            }
        });
        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                ConnectivityManager cm =
                        (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                //starting login attempt while network is connected
                NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
                boolean isConnected = activeNetwork != null &&
                        activeNetwork.isConnectedOrConnecting();
                if (isConnected) {
                    attemptLogin();
                }else {
                    Toast.makeText(LoginAuthActivity.this,"Connection Error!",Toast.LENGTH_SHORT).show();
                }
            }
        });
        //retrieve login type (user/customer) from bundle arguments
        login_type = getIntent().getExtras().getInt("login_type");
        switch (login_type) {
            case AppConstants.TYPE_CUSTOMER_LOGIN:
                //if customer logged in then sign up button will be visible and set action as intent to SignUpActivity
                mSignUpButton.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(LoginAuthActivity.this, SignUpActivity.class);
                        startActivity(intent);
                    }
                });
                break;
            case AppConstants.TYPE_USER_LOGIN:
                //if user logged in then signup button should set as invisible
                mSignUpButton.setVisibility(View.GONE);
                break;
        }

        //action when click forgot password
        forgotpwdView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog();

            }
        });

    }

    //display dialog box when clicked forgot password, it contain a field for enter your email.
    private void showDialog() {
        final AlertDialog.Builder alert = new AlertDialog.Builder(LoginAuthActivity.this);
        alert.setTitle("Forgot Passwoord"); //Set Alert dialog title here
        alert.setMessage("Enter Your email"); //Message here
        // Set an EditText view to get user input
        final EditText input = new EditText(getApplicationContext());
        input.setTextColor(Color.BLACK);
        input.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);;
        alert.setView(input);

        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //You will get as string input data in this variable.
                // here we convert the input to a string and show in a toast.
                String srt = input.getEditableText().toString();
                if (srt.contains("@")){
//                    if valied email is entered forgot password api will be called to request to email password to the input mail
                    new ForgotPasswordSender().execute(srt);
                    Toast.makeText(getApplicationContext(),srt, Toast.LENGTH_LONG).show();
                }
                else {
                    //if invalied mail show this message
                    Toast.makeText(getApplicationContext(),"invalied email", Toast.LENGTH_LONG).show();
                }
                dialog.dismiss();
            } // End of onClick(DialogInterface dialog, int whichButton)
        }); //End of alert.setPositiveButton
        alert.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                // Canceled.
                dialog.dismiss();
//                dialog.cancel();
            }
        }); //End of alert.setNegativeButton
        AlertDialog alertDialog = alert.create();
        alertDialog.show();
    }


    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    public void attemptLogin() {
        if (mAuthTask != null) {
            return;
        }

        // Reset errors.
        mEmailView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        String email = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;


        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
        } else if (!isEmailValid(email)) {
            mEmailView.setError(getString(R.string.error_invalid_email));
            focusView = mEmailView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            showProgress(true);
            mAuthTask = new UserLoginTask(email, password);
            mAuthTask.execute((Void) null);
        }
    }

    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return email.contains("@");
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() >= 4;
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    public void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

//            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
//                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
//                @Override
//                public void onAnimationEnd(Animator animation) {
//                }
//            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
        }
    }

//    after successfully logged in, we need to store all information of logged user in preferences
    private void addPreferences(JSONObject responeObject) {

        try {
            SharedPreferences.Editor editor = sharedPreferences.edit();
            if (responeObject.getString("user_mode").equals("2")){
                editor.putInt(AppConstants.PREFERENCES_USER_MODE, login_type);
                editor.putString(AppConstants.PREFERENCES_EXE_ID,responeObject.getString(AppConstants.PREFERENCES_EXE_ID));
                editor.putString(AppConstants.PREFERENCES_EXE_NAME,responeObject.getString(AppConstants.PREFERENCES_EXE_NAME));
                editor.putString(AppConstants.PREFERENCES_EXE_LOCATION,responeObject.getString(AppConstants.PREFERENCES_EXE_LOCATION));
                editor.putString(AppConstants.PREFERENCES_EXE_EMAIL,responeObject.getString(AppConstants.PREFERENCES_EXE_EMAIL));
                editor.putString(AppConstants.PREFERENCES_EXE_ADDRESS,responeObject.getString(AppConstants.PREFERENCES_EXE_ADDRESS));
            }
            else {
                editor.putString(AppConstants.PREFERENCES_USER_ID, responeObject.getString(AppConstants.PREFERENCES_USER_ID));
                editor.putInt(AppConstants.PREFERENCES_USER_MODE, login_type);
                editor.putString(AppConstants.PREFERENCES_USER_NAME, responeObject.getString(AppConstants.PREFERENCES_USER_NAME));
                editor.putString(AppConstants.PREFERENCES_USER_TYPE, responeObject.getString(AppConstants.PREFERENCES_USER_TYPE));
                editor.putString(AppConstants.PREFERENCES_USER_EMAIL, responeObject.getString(AppConstants.PREFERENCES_USER_EMAIL));
                editor.putString(AppConstants.PREFERENCES_USER_PHONE, responeObject.getString(AppConstants.PREFERENCES_USER_PHONE));
                editor.putString(AppConstants.PREFERENCES_USER_LOCATION, responeObject.getString(AppConstants.PREFERENCES_USER_LOCATION));
                editor.putString(AppConstants.PREFERENCES_USER_ADDRESS, responeObject.getString(AppConstants.PREFERENCES_USER_ADDRESS));
                editor.putString(AppConstants.PREFERENCES_USER_GROUP_ID, responeObject.getString(AppConstants.PREFERENCES_USER_GROUP_ID));

                //store users pricing information according to user types
                switch (Integer.parseInt(responeObject.getString(AppConstants.PREFERENCES_USER_TYPE))) {
                    case 1:
                        editor.putString(AppConstants.PREFERENCES_CUSTOMER_PRICE_TYPE, ProductsDb.PROD_CFSTOCKIST_PRICE);
                        break;
                    case 2:
                        editor.putString(AppConstants.PREFERENCES_CUSTOMER_PRICE_TYPE, ProductsDb.PROD_DEALER_PRICE);
                        break;
                    case 3:
                        editor.putString(AppConstants.PREFERENCES_CUSTOMER_PRICE_TYPE, ProductsDb.PROD_DISTRIBUTOR_PRICE);
                        break;
                    case 4:
                        editor.putString(AppConstants.PREFERENCES_CUSTOMER_PRICE_TYPE, ProductsDb.PROD_CONSUMERS_PRICE);
                        break;
                }
            }
            editor.apply();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */
    public class UserLoginTask extends AsyncTask<Void, Void, Boolean> {

        private final String mEmail;
        private final String mPassword;

        UserLoginTask(String email, String password) {
            mEmail = email;
            mPassword = password;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            // TODO: attempt authentication against a network service.

            try {
                // Simulate network access.
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                return false;
            }
//            create a request with JSON format with email and password
            JSONObject object = new JSONObject();
            try {

                object.put("username", mEmail);
                object.put("password", mPassword);
                object.put("user_mode", login_type);
                //send login request to server and retrieve response
                String response = JSONParser.makeHttpRequest(AppConstants.URL_LOGIN, "POST", object.toString());
                JSONObject responeObject = new JSONObject(response);
                Integer status = responeObject.getInt("status");
                if (status == 1) {
                    finish();
//                    add preference from response
                    addPreferences(responeObject);
                    return true;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return false;
            // TODO: register the new account here.
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mAuthTask = null;
            showProgress(false);

            if (success) {
                finish();
//                put user email to preference for later login use
                SharedPreferences.Editor editor=sharedPreferences.edit();
                editor.putString("EmailTag",mEmailView.getText().toString());
                editor.putString("xshop", "true");
                editor.apply();
                Intent serviceIntent= new Intent(getApplicationContext(), SyncService.class);
                startService(serviceIntent);
                Intent homeIntent = new Intent(LoginAuthActivity.this, OrderProductsActivity.class);
                homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(homeIntent);
            } else {
//                if login error, clear all exisiting field and show password incorrect message
                mEmailView.setText("");
                mPasswordView.setText("");
                mPasswordView.setError(getString(R.string.error_incorrect_password));
                mPasswordView.requestFocus();
            }
        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
            showProgress(false);
        }
    }

    //send api request for forgot password send
    private class ForgotPasswordSender extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            JSONObject requestObject=new JSONObject();
            try {
                requestObject.put("email",params[0]);
                requestObject.put("user_mode",login_type);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return JSONParser.makeHttpRequest(AppConstants.URL_PASSWORD_RESET,"POST",requestObject.toString());
        }

        @Override
        protected void onPostExecute(String str) {
            try {
                JSONObject responseObj=new JSONObject(str);
                if (responseObj.getString("details").equals("success")){
                    Toast.makeText(getApplicationContext(),"Successfully Send!",Toast.LENGTH_SHORT).show();
                }
                else {
                    Toast.makeText(getApplicationContext(),"failed to Send!",Toast.LENGTH_SHORT).show();
                }
            }catch (JSONException e){
                e.printStackTrace();
            }
        }
    }
}



