package com.sha.netstager.pandafoods.activity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;

import com.sha.netstager.pandafoods.R;
import com.sha.netstager.pandafoods.adapters.OfferCursorAdapter;
import com.sha.netstager.pandafoods.fragments.OfferListFragment;

public class OfferViewActivity extends PandaActivity {

    private SharedPreferences sharedPreferences;
    private OfferCursorAdapter dataAdapter;
    private FragmentPagerAdapter adapterViewPager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        actionBar.setTitle("Offers");
        setContentView(R.layout.activity_offer_view);
        ViewPager vpPager = (ViewPager) findViewById(R.id.vpPager);
        adapterViewPager = new MyPagerAdapter(getSupportFragmentManager());
        vpPager.setAdapter(adapterViewPager);
//        sharedPreferences=getSharedPreferences(AppConstants.PANDA_PREFERENCES, Context.MODE_PRIVATE);
//        loadListView();
    }

//    private void loadListView(){
//        // the XML defined views which the data will be bound to
//        ListView cartListView = (ListView)findViewById(R.id.cart_listview);
//        dataAdapter= new OfferCursorAdapter(getApplicationContext(),null,0);
//        cartListView.setAdapter(dataAdapter);
//        //Ensures a loader is initialized and active.
//
////        getLoaderManager().initLoader(0, null, this);
//    }


    //pager class for swipe and view offer list according to type
    public static class MyPagerAdapter extends FragmentPagerAdapter {
        private static int NUM_ITEMS = 2;

        public MyPagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        // Returns total number of pages
        @Override
        public int getCount() {
            return NUM_ITEMS;
        }

        // Returns the fragment to display for that page
        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0: // OfferListFragment - This will show Value Offers
                    return OfferListFragment.newInstance(0, "Value Offers");
                case 1: // OfferListFragment - This will show Product Offers
                    return OfferListFragment.newInstance(1, "Product Offers");
                default:
                    return null;
            }
        }

        // Returns the page title for the top indicator
        @Override
        public CharSequence getPageTitle(int position) {
            if (position==0){
                return "Value Offers";
            }
            else {
                return "Product Offers";
            }
        }

    }

}




