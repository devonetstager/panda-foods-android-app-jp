package com.sha.netstager.pandafoods.activity;

import android.app.LoaderManager;
import android.content.ContentValues;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.sha.netstager.pandafoods.R;
import com.sha.netstager.pandafoods.adapters.ProductCursorAdapter;
import com.sha.netstager.pandafoods.contentProvider.LocalDbProvider;
import com.sha.netstager.pandafoods.datatables.CartDb;
import com.sha.netstager.pandafoods.datatables.CatagoryDb;
import com.sha.netstager.pandafoods.datatables.ProductsDb;
import com.sha.netstager.pandafoods.model.CartModel;
import com.sha.netstager.pandafoods.util.AppConstants;
import com.squareup.picasso.Picasso;

import static com.sha.netstager.pandafoods.R.id.imageView2;

public class ProductMainActivity extends PandaActivity implements LoaderManager.LoaderCallbacks<Cursor>{

    //    private ProductAdapter dataAdapter;

    // instantiate variables
    public String cat_id,imgUrlPath;
    private SharedPreferences sharedPreferences;
    private static ProductCursorAdapter dataAdapter;
    private TextView totalprice,totalquantity,product_availability_view;
    private ImageView imgIcon;
    private ListView product_list_view;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState == null) {
            Bundle bundle = getIntent().getExtras();
//            obtain catagory id from arguments
            cat_id = bundle.getString(CatagoryDb.CAT_ID);
//            retrieve catagory data from db using the catagory id and set as title
            Cursor cursor=getContentResolver().query(LocalDbProvider.CATAGORY_URI,new String[]{CatagoryDb.CAT_NAME},CatagoryDb.CAT_ID+"=?",new String[]{cat_id},null);
            cursor.moveToFirst();
            actionBar.setTitle(cursor.getString(cursor.getColumnIndexOrThrow(CatagoryDb.CAT_NAME)));
            cursor.close();
            setContentView(R.layout.activity_product_main);

//            initialize the views
            sharedPreferences = getSharedPreferences(AppConstants.PANDA_PREFERENCES,MODE_PRIVATE);
            totalprice= (TextView) findViewById(R.id.product_total_quantity);
            totalquantity= (TextView) findViewById(R.id.product_total_items);
             product_list_view = (ListView) findViewById(R.id.product_list_view);
            product_availability_view = (TextView) findViewById(R.id.product_availability_display);
            product_availability_view = (TextView) findViewById(R.id.product_availability_display);
            imgIcon = (ImageView) findViewById(R.id.imageView2);



            //initialize the cursor adapter AND set to list view
            dataAdapter= new ProductCursorAdapter(this,null,0);
            product_list_view.setAdapter(dataAdapter);

           // Image setting here in product

            String imgPath=sharedPreferences.getString("itmImgUrl","0");
            String childItms=sharedPreferences.getString("childItems","0");
            imgUrlPath = imgPath;
            Log.i("childItms <<",childItms);

            //Picasso.with(this).load(imgPath).fit().centerCrop().into(imgIcon); // product cat image setting here
            Picasso.with(this).load(imgPath).into(imgIcon); // product cat image setting here
            //Picasso.with(this).load("http://img.webmd.com/dtmcms/live/webmd/consumer_assets/site_images/articles/health_tools/foods_harmful_to_cats_slideshow/jiu_rf_photo_of_cat_licking_its_chops.jpg").fit().centerCrop().into(imgIcon); // product cat image setting here



            //init loader to load products
            getLoaderManager().initLoader(0,null,this);
            final Button add_to_cart_btn= (Button) findViewById(R.id.products_add_to_cart);

            //set listener for add to cart button
            add_to_cart_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    on button clicked, load all quantity entered in quantity fields retrieve into a CartModel array
                    CartModel[] cartModelArray=dataAdapter.getQuantityArray();
                    boolean flag=false;
                    for (CartModel cm:cartModelArray){
                        if (!TextUtils.isEmpty(cm.getQuantity())){
                            if (Integer.parseInt(cm.getQuantity())!=0){
                               flag = true;

                            }

                        }
                        if (flag) break;

                    }
//                    if quantity fields are not empty, then iterate CartModel array and add each items to Cart db
                    if (flag) {
                        for (CartModel cm : cartModelArray) {
                            if (!TextUtils.isEmpty(cm.getProductid())) {
                                ContentValues values = new ContentValues();
                                String userid = sharedPreferences.getString(AppConstants.PREFERENCES_USER_ID, "0");
                                values.put(CartDb.PROD_ID, cm.getProductid());
                                values.put(CartDb.USER_ID, userid);
                                Integer totalqty;

                                //Query Variables
                                String[] projection = new String[]{CartDb.PROD_ID, CartDb.PROD_QUANTITY};
                                String selection = CartDb.PROD_ID + "=? & " + CartDb.USER_ID + "=?";
                                String[] args = new String[]{cm.getProductid(), userid};


                                //addition or updation of quantity
                                Cursor updator = getContentResolver().query(LocalDbProvider.CART_URI, projection, selection, args, null);
                                updator.moveToFirst();

                                if (updator.isAfterLast()) {
                                    values.put(CartDb.PROD_QUANTITY, cm.getQuantity());
                                    getContentResolver().insert(LocalDbProvider.CART_URI, values);
                                } else {
                                    String quantity = updator.getString(updator.getColumnIndexOrThrow(CartDb.PROD_QUANTITY));
                                    totalqty = Integer.parseInt(quantity) + Integer.parseInt(cm.getQuantity());
                                    values.put(CartDb.PROD_QUANTITY, totalqty);
                                    getContentResolver().update(LocalDbProvider.CART_URI, values, selection, args);
                                }
                                updator.close();
                                finish();

                            }
                        }
                        Toast.makeText(getApplicationContext(), "Successfully Added to cart", Toast.LENGTH_SHORT).show();
                        Intent homeGrid = new Intent(getApplicationContext(), CartShowActivity.class);
                        startActivity(homeGrid);
                    }
                    else {
                        Toast.makeText(getApplicationContext(), "There is no items in cart", Toast.LENGTH_SHORT).show();
                    }

                }
            });
        }


    }
    public void visibleFasle() {


        imgIcon.setVisibility(View.GONE);

    }
    public void visibleTrue() {

        Picasso.with(this).load(imgUrlPath).fit().centerCrop().into(imgIcon);
        imgIcon.setVisibility(View.VISIBLE);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
            if (item.getItemId() == 16908332) {
                finish();
                return true;
            }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {

        String[] projectionprod = {
                ProductsDb._ID,
                ProductsDb.PROD_ID,
                ProductsDb.PROD_NAME,
                ProductsDb.PROD_GROUP,
                ProductsDb.PROD_WEIGHT_ID,
                ProductsDb.PROD_WEIGHT,
                ProductsDb.PROD_UNIT,
                ProductsDb.PACKAGE,
                ProductsDb.PROD_IMAGE,
                sharedPreferences.getString(AppConstants.PREFERENCES_CUSTOMER_PRICE_TYPE, ProductsDb.PROD_CFSTOCKIST_PRICE),
                ProductsDb.PROD_CATEGORY,
                ProductsDb.PROD_SUB_CATEGORY
        };
        return new CursorLoader(getApplicationContext(), LocalDbProvider.CONTENT_URI, projectionprod, ProductsDb.PROD_SUB_CATEGORY + "=?", new String[]{cat_id}, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {

         dataAdapter.swapCursor(cursor);
        if (!cursor.moveToFirst()) {
            product_availability_view.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
            dataAdapter.swapCursor(null);
    }

    //set total amount while entering quantity in quantity field of products
    public void setTotal() {
        CartModel[] cartModels=dataAdapter.getQuantityArray();
        float totalPrice=0;
        int total_products=0;
        for (CartModel cm : cartModels){
            if (!TextUtils.isEmpty(cm.getProductid())){
                int quantity= Integer.parseInt(cm.getQuantity());
                Cursor cursor=getContentResolver().query(LocalDbProvider.CONTENT_URI,new String[]{ProductsDb.PROD_ID,sharedPreferences.getString(AppConstants.PREFERENCES_CUSTOMER_PRICE_TYPE,ProductsDb.PROD_CFSTOCKIST_PRICE)},ProductsDb.PROD_ID+"=?",new String[]{cm.getProductid()},null);
                if (cursor.moveToFirst()){
                    float priceproduct= Float.parseFloat(cursor.getString(cursor.getColumnIndexOrThrow(sharedPreferences.getString(AppConstants.PREFERENCES_CUSTOMER_PRICE_TYPE, ProductsDb.PROD_CFSTOCKIST_PRICE))));
                    totalPrice+= priceproduct*quantity;
                    total_products+=quantity;
                }
                cursor.close();
            }
        }
        totalquantity.setText(total_products+"");
        totalprice.setText("\u20B9" +totalPrice+"");

        //String imgPath=sharedPreferences.getString("itmImgUrl","0");

        //Log.i("Image Path <<",imgPath);





    }
}
