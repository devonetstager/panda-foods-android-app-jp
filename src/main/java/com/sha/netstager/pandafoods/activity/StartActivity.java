package com.sha.netstager.pandafoods.activity;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;

import com.sha.netstager.pandafoods.R;
import com.sha.netstager.pandafoods.services.SyncService;
import com.sha.netstager.pandafoods.util.AppConstants;

public class StartActivity extends FragmentActivity {

    private SharedPreferences sharedPreferences;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //initialize preferences and connectivity manager
        sharedPreferences=getSharedPreferences(AppConstants.PANDA_PREFERENCES,MODE_PRIVATE);
//        ConnectivityManager cm =
//                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
//
//        //starting products synchronisation service while network is connected and the service is not running
//        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
//        boolean isConnected = activeNetwork != null &&
//                activeNetwork.isConnectedOrConnecting();
//        if (isConnected) {
//            if (!isMyServiceRunning(SyncService.class)) {
//                Intent syncIntent = new Intent(getApplicationContext(), SyncService.class);
//                startService(syncIntent);
//            }
//        }

        //check whether any executives ot customer is already logged in
        if(sharedPreferences.getString(AppConstants.PREFERENCES_USER_ID,"0").equals("0")&&sharedPreferences.getString(AppConstants.PREFERENCES_EXE_ID,"0").equals("0")){
//            if not logged in, then display content view

                SyncService s=new SyncService();

            Intent intentGrid = new Intent(getApplicationContext(), Grid_Home_Activity.class);
            startActivity(intentGrid);
            //setContentView(R.layout.activity_start);
        }
        else {
            //if already logged, then directly go to home activity of user/customer
            Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(!(sharedPreferences.getString(AppConstants.PREFERENCES_USER_ID,"0").equals("0")&&sharedPreferences.getString(AppConstants.PREFERENCES_EXE_ID,"0").equals("0"))){
            Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
        }
    }

    //action for buttons in the view user login/customer login
    public void StartLoginActions(View view) {

        Intent startIntent=new Intent(StartActivity.this,LoginActivity.class);

        switch (view.getId()){
            case R.id.start_customer_login:
                startIntent.putExtra("login_type", AppConstants.TYPE_CUSTOMER_LOGIN);
                break;
            case R.id.start_user_login:
                startIntent.putExtra("login_type", AppConstants.TYPE_USER_LOGIN);
                break;
        }
        startActivity(startIntent);
    }

    //when back pressed application should close and home for android launcher will be displayed
    @Override
    public void onBackPressed() {
        Intent startMain = new Intent(Intent.ACTION_MAIN);
        startMain.addCategory(Intent.CATEGORY_HOME);
        startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(startMain);
        finish();
    }

    //method for checking any service is running on background
    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }


}
