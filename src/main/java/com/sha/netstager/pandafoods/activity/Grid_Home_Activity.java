package com.sha.netstager.pandafoods.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sha.netstager.pandafoods.R;
import com.sha.netstager.pandafoods.adapters.CatagoryCursorAdapter;
import com.sha.netstager.pandafoods.fragments.GridFragment;
import com.sha.netstager.pandafoods.services.SyncService;
import com.sha.netstager.pandafoods.util.AppConstants;

public class Grid_Home_Activity extends CartActionBar
        implements NavigationDrawerFragment.NavigationDrawerCallbacks {

    private ListView catListView;
    private CatagoryCursorAdapter dataAdapter;
    private GridFragment gridfragment;
    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;
    private SharedPreferences sharedPreferences;

    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private CharSequence mTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        sharedPreferences = getSharedPreferences(AppConstants.PANDA_PREFERENCES, Context.MODE_PRIVATE);

        String userid=sharedPreferences.getString(AppConstants.PREFERENCES_USER_ID,"0");
        String UsrCheck=sharedPreferences.getString(AppConstants.logCheck,"0");

        Log.i("Session Id",UsrCheck);
        Log.i("User Id",userid);

        if(UsrCheck.equals("sessionover")){
            SharedPreferences.Editor editor=sharedPreferences.edit();
            editor.putString("nonLoggedId",userid);
            editor.apply();
        }
        else{
            SharedPreferences.Editor editor=sharedPreferences.edit();
            //editor.putString("nonLoggedId","0");
            editor.putString("xshop", "false");
            editor.apply();
        }
        String FinalCheck=sharedPreferences.getString(AppConstants.logCheck,"0");

        Log.i("User Id Final",FinalCheck);

        Intent serviceIntent= new Intent(getApplicationContext(), SyncService.class);
        startService(serviceIntent);

        gridfragment = GridFragment.newInstance("");

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grid__home);

        ActionBar actionbar = getSupportActionBar();
        actionbar.setCustomView(R.layout.logo_grid);
        actionbar.setDisplayShowTitleEnabled(false);
        actionbar.setDisplayShowCustomEnabled(true);


        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));
    }


    @Override
    public void onNavigationDrawerItemSelected(int position) {

        // update the main content by replacing fragments
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction ft= fragmentManager.beginTransaction();

        switch (position){
            case 0:
                ft.replace(R.id.container, gridfragment);
                break;
            case 1:
                Intent intentLogin = new Intent(Grid_Home_Activity.this, LoginActivity.class);
                intentLogin.putExtra("login_type", AppConstants.TYPE_CUSTOMER_LOGIN);
                startActivity(intentLogin);
                break;
            case 2:
                Intent intentSignUp = new Intent(Grid_Home_Activity.this, SignUpActivity.class);
                startActivity(intentSignUp);
                break;
            case 3:
                Intent intentCorp = new Intent(Grid_Home_Activity.this, LoginActivity.class);
                intentCorp.putExtra("login_type", AppConstants.TYPE_USER_LOGIN);
                startActivity(intentCorp);
                break;
//            case 4:
//                showDialog();
//                break;
            default:

                ft.replace(R.id.container, gridfragment);
                break;
        }
        ft.commit();
    }

//    //method for display logout which confirming log out
//    private void showDialog() {
//        final AlertDialog.Builder alert = new AlertDialog.Builder(Grid_Home_Activity.this);
//        alert.setTitle("Confirm Logout"); //Set Alert dialog title here
//        alert.setMessage("Do you really want to log out? you will lose all your data, including Product List and Orders"); //Message here
//
//
//        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int whichButton) {
////                if user confirm to logout clear all databases and stored id of user/customer
//                SharedPreferences.Editor editor= sharedPreferences.edit();
//                editor.putString(AppConstants.PREFERENCES_EXE_ID,"0");
//                editor.putString(AppConstants.PREFERENCES_USER_ID,"0");
//                editor.apply();
////                getContentResolver().delete(LocalDbProvider.CART_URI, null, null);
////                getContentResolver().delete(LocalDbProvider.CONTENT_URI, null, null);
////                getContentResolver().delete(LocalDbProvider.CATAGORY_URI,null,null);
//                getContentResolver().delete(LocalDbProvider.ORDER_URI,null,null);
//                getContentResolver().delete(LocalDbProvider.ORDER_PRODUCT_URI,null,null);
////                getContentResolver().delete(LocalDbProvider.WEIGHT_URI,null,null);
////                getContentResolver().delete(LocalDbProvider.COUNTRY_URI,null,null);
////                getContentResolver().delete(LocalDbProvider.STATE_URI,null,null);
////                getContentResolver().delete(LocalDbProvider.DISTRICT_URI,null,null);
////                getContentResolver().delete(LocalDbProvider.LOC_URI,null,null);
////                getContentResolver().delete(LocalDbProvider.CUSOTMER_URI,null,null);
////                getContentResolver().delete(LocalDbProvider.OFFER_URI,null,null);
//                Intent intent= new Intent(getApplicationContext(),StartActivity.class);
//                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                startActivity(intent);
//                finish();
//
//                dialog.dismiss();
//            } // End of onClick(DialogInterface dialog, int whichButton)
//        }); //End of alert.setPositiveButton
//        alert.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int whichButton) {
//                dialog.dismiss();
//            }
//        }); //End of alert.setNegativeButton
//        AlertDialog alertDialog = alert.create();
//        alertDialog.show();
//    }


    public void onSectionAttached(int number) {
        switch (number) {
            case 1:
                mTitle = getString(R.string.titleHome);
                break;
            case 2:
                mTitle = getString(R.string.title_section1);
                break;
            case 3:
                mTitle = getString(R.string.title_section2);
                break;
            case 4:
                mTitle = getString(R.string.title_section3);
                break;
        }
    }

    public void restoreActionBar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#D5D2D4")));
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle("");
        actionBar.setDisplayUseLogoEnabled(false);
        actionBar.setIcon(R.drawable.addcustomer);

       // actionBar.setLogo(getResources().getDrawable(R.drawable.addcustomer));
    }


//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        if (!mNavigationDrawerFragment.isDrawerOpen()) {
//            // Only show items in the action bar relevant to this screen
//            // if the drawer is not showing. Otherwise, let the drawer
//            // decide what to show in the action bar.
//            getMenuInflater().inflate(R.menu.grid__home_, menu);
//            restoreActionBar();
//            return true;
//        }
//        return super.onCreateOptionsMenu(menu);
//    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_panda, menu);

        RelativeLayout rl_viewBag = (RelativeLayout) menu.findItem(R.id.badge).getActionView();
        String cartCount=sharedPreferences.getString(AppConstants.cartcount,"0");
        Log.i("Cart Value second",cartCount);
        notifCount = (TextView)rl_viewBag.findViewById(R.id.actionbar_notifcation_textview);
        notifCount.setText(cartCount);

        rl_viewBag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cartIntent = new Intent(getApplicationContext(), CartShowActivity.class);
                startActivity(cartIntent);
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }

        return super.onOptionsItemSelected(item);
    }


}
