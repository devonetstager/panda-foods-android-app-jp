package com.sha.netstager.pandafoods.activity;

import android.app.FragmentTransaction;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.sha.netstager.pandafoods.R;
import com.sha.netstager.pandafoods.contentProvider.LocalDbProvider;
import com.sha.netstager.pandafoods.datatables.OrderDb;
import com.sha.netstager.pandafoods.datatables.OrderProductsDb;
import com.sha.netstager.pandafoods.fragments.OrderAdressSetFragment;
import com.sha.netstager.pandafoods.fragments.ProductListFragment;
import com.sha.netstager.pandafoods.util.AppConstants;

public class OrderedProductShowActivity extends PandaActivity {
    private TextView totalamount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        actionBar.setDisplayHomeAsUpEnabled(true);
        setContentView(R.layout.activity_ordered_product_show);
        //initialize frame layout to display ProductListFragment and OrderAdressSetFragment
        FrameLayout productLayout = (FrameLayout) findViewById(R.id.product_list_fragment);
        FrameLayout adressLayout = (FrameLayout) findViewById(R.id.order_address_fragment);
        //initialize sharedpreferences and views in the activity
        SharedPreferences sharedPreferences = getSharedPreferences(AppConstants.PANDA_PREFERENCES, MODE_PRIVATE);
        TextView customerid = (TextView) findViewById(R.id.cart_cust_id);
        TextView grandtotal = (TextView) findViewById(R.id.cart_grand_total);
        totalamount = (TextView) findViewById(R.id.cart_total_amount);
        TextView discount = (TextView) findViewById(R.id.cart_discount);

        if (savedInstanceState == null) {
            Bundle bundle = getIntent().getExtras();
            String customer_id = "";
            //initialize ProductListFragment and pass orderid as parameter
            ProductListFragment productListFragment = ProductListFragment.newInstance(3, bundle.getString(OrderProductsDb.ORDER_ID));
            //load cursor data from order table to display total amount,discount etc
            Cursor cursor = getContentResolver().query(LocalDbProvider.ORDER_URI, new String[]{OrderDb.EXE_ID, OrderDb.USER_ID, OrderDb.ORDER_AMOUNT, OrderDb.PROMO_CODE,OrderDb.PROMO_DISCOUNT}, OrderDb._ID + "=?", new String[]{bundle.getString(OrderProductsDb.ORDER_ID)}, null);
            if (cursor.moveToFirst()) {
                customer_id = cursor.getString(cursor.getColumnIndexOrThrow(OrderDb.USER_ID));
                customerid.setText(customer_id);
                discount.setText(cursor.getString(cursor.getColumnIndexOrThrow(OrderDb.PROMO_DISCOUNT)) + "%");
                grandtotal.setText(cursor.getString(cursor.getColumnIndexOrThrow(OrderDb.ORDER_AMOUNT)));
            }
            // initialize OrderAdressSetFragment accroding to user logged
            OrderAdressSetFragment adressSetFragment;
            // if customer is logged in put parameter as self
            if (!sharedPreferences.getString(AppConstants.PREFERENCES_USER_ID, "0").equals("0")&& sharedPreferences.getString(AppConstants.PREFERENCES_EXE_ID, "0").equals("0")) {
                adressSetFragment = OrderAdressSetFragment.newInstance("Self");
            } else {
                // if executive is logged in put parameter as customerid
                adressSetFragment = OrderAdressSetFragment.newInstance(customer_id);
            }
            //add fragments to framelayouts and commit
            FragmentTransaction ft = getFragmentManager().beginTransaction();
            ft.add(productLayout.getId(), productListFragment);
            ft.add(adressLayout.getId(), adressSetFragment);
            ft.commit();
        }
    }




    //method for set total amount to textview
    public void totalFragmentDisplay(float totalcash) {
        totalamount.setText(totalcash+"");
    }

}
