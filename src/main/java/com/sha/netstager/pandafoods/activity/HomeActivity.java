package com.sha.netstager.pandafoods.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.sha.netstager.pandafoods.R;
import com.sha.netstager.pandafoods.adapters.HomeOptionAdapter;
import com.sha.netstager.pandafoods.model.HomeMainModel;
import com.sha.netstager.pandafoods.util.AppConstants;

//home activity for both user and customer
public class HomeActivity extends PandaActivity {

    private ListView menuListView;
    private SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        preferences=getSharedPreferences(AppConstants.PANDA_PREFERENCES,MODE_PRIVATE);

        //check wether user not logged in, or login information is cleared
        if (preferences.getString(AppConstants.PREFERENCES_USER_ID,"0").equals("0")&&preferences.getString(AppConstants.PREFERENCES_EXE_ID,"0").equals("0")){
            //if not logged in go to StartActivity
            Log.i("cust id& exe id",preferences.getString(AppConstants.PREFERENCES_USER_ID, "0")+preferences.getString(AppConstants.PREFERENCES_EXE_ID, "0")+"");
            Intent intent=new Intent(getApplicationContext(),StartActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        }
        actionBar.setTitle(R.string.menu);
        actionBar.setDisplayHomeAsUpEnabled(false);
        menuListView= (ListView) findViewById(R.id.home_menu_list);
        // create object for HomeOptionAdapter (custom list view adapter) and set to menu list view to display menu according to user type
        final HomeOptionAdapter homeOptionAdapter=addCustomerOptions();
        menuListView.setAdapter(homeOptionAdapter);
//       specify action if an item clicked in the menu
        menuListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //if clicked in first option that is Order, then check any customer is set when we logged as executive, if we didnt select any customer, then display a message
                if (position==0&&preferences.getString(AppConstants.PREFERENCES_USER_ID,"0").equals("0")&&preferences.getInt(AppConstants.PREFERENCES_USER_MODE,1)==2){
                    Toast.makeText(getApplicationContext(), "Please select a customer from home/customer", Toast.LENGTH_SHORT).show();
                }

                else {
                    try {
                        //else when we clicked on any item take its class name from adapter, and make intent and display it
                        Class cls = Class.forName(homeOptionAdapter.getItem(position));
                        Intent intent = new Intent(getApplicationContext(), cls);
                        startActivity(intent);
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

    }

    //This method return an HomeOptionAdapter instance
    private HomeOptionAdapter addCustomerOptions() {
        HomeOptionAdapter homeOptionAdapter=new HomeOptionAdapter(getApplicationContext());
        // add different options on home menu
        HomeMainModel productModel=new HomeMainModel(Grid_Home_Activity.class.getName(),R.drawable.orderhome,"Order");
        HomeMainModel orderModel= new HomeMainModel(RecentOrderActivity.class.getName(),R.drawable.recentorders,"Recent Orders");
        HomeMainModel offerModel= new HomeMainModel(OfferViewActivity.class.getName(),R.drawable.offerico,"Offers");
        HomeMainModel settingsModel= new HomeMainModel(SettingsActivity.class.getName(),R.drawable.settings,"Settings");
        homeOptionAdapter.addModels(productModel);
        homeOptionAdapter.addModels(orderModel);
        if (preferences.getInt(AppConstants.PREFERENCES_USER_MODE,1)==2){
            HomeMainModel customerModel= new HomeMainModel(AddCustomerActivity.class.getName(),R.drawable.addcustomer,"Add Customer");
            homeOptionAdapter.addModels(customerModel);
            HomeMainModel setCustomerModel= new HomeMainModel(SetCustomerActivity.class.getName(),R.drawable.setcustomer,"Customer");
            homeOptionAdapter.addModels(setCustomerModel);
        }
        homeOptionAdapter.addModels(offerModel);
        homeOptionAdapter.addModels(settingsModel );
        return homeOptionAdapter;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (preferences.getString(AppConstants.PREFERENCES_USER_ID,"0").equals("0")&&preferences.getString(AppConstants.PREFERENCES_EXE_ID,"0").equals("0")){
            Intent intent=new Intent(getApplicationContext(),StartActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        }
    }

    //specify what action occured when back button pressed
    @Override
    public void onBackPressed() {
        Intent startMain = new Intent(Intent.ACTION_MAIN);
        startMain.addCategory(Intent.CATEGORY_HOME);
        startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(startMain);
        finish();
    }

}
