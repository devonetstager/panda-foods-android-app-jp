package com.sha.netstager.pandafoods.activity;

import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.sha.netstager.pandafoods.R;
import com.sha.netstager.pandafoods.contentProvider.LocalDbProvider;
import com.sha.netstager.pandafoods.datatables.CartDb;
import com.sha.netstager.pandafoods.datatables.CatagoryDb;
import com.sha.netstager.pandafoods.datatables.ProductsDb;
import com.sha.netstager.pandafoods.datatables.WeightDb;
import com.sha.netstager.pandafoods.util.AppConstants;
import com.squareup.picasso.Picasso;

public class ProductDetailsActivity extends PandaActivity{

    private ImageView productThumb;
    private TextView prodName,prodCode,prodPrice,prodDescription;
    private ImageView proImg;
    private EditText quantityText;
    private Button addToCart,backToProducts;
    private String cat_id;
    private SharedPreferences sharedPreferences;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_details);
        final Bundle bundle=getIntent().getExtras();
        sharedPreferences= getSharedPreferences(AppConstants.PANDA_PREFERENCES,MODE_PRIVATE);
        String[] args=new String[]{bundle.getString(ProductsDb.PROD_ID)};
        Log.i("proid=",ProductsDb.PROD_ID);
        String[] projection=new String[]{ProductsDb._ID,ProductsDb.PROD_ID,ProductsDb.PROD_NAME,ProductsDb.PROD_WEIGHT_ID,ProductsDb.PROD_CATEGORY,sharedPreferences.getString(AppConstants.PREFERENCES_CUSTOMER_PRICE_TYPE,ProductsDb.PROD_CFSTOCKIST_PRICE),ProductsDb.PROD_IMAGE,ProductsDb.PROD_DESCRIPTION};
        Cursor cursor=getContentResolver().query(LocalDbProvider.CONTENT_URI,projection,ProductsDb.PROD_ID+"=?",args,null);
        if (cursor.moveToFirst()) {

            cat_id = cursor.getString(cursor.getColumnIndexOrThrow(ProductsDb.PROD_CATEGORY));
            updateFields(cursor);
        }
        cursor.close();
        quantityText= (EditText) findViewById(R.id.product_details_quantity);
        addToCart= (Button) findViewById(R.id.cart_addtocart_btn);
        backToProducts = (Button) findViewById(R.id.cart_backtoproducts_btn);
        addToCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String quantity=quantityText.getText().toString();
                if (TextUtils.isEmpty(quantity)){
                    Toast.makeText(getApplicationContext(),"Quantity is nil",Toast.LENGTH_SHORT).show();
                }
                else {
                    String prodid = bundle.getString(ProductsDb.PROD_ID);
                    ContentValues values = new ContentValues();
                    values.put(CartDb.PROD_ID, prodid);
                    Integer totalqty;

                    //Query Variables
                    String[] projection = new String[]{CartDb.PROD_ID, CartDb.PROD_QUANTITY};
                    String selection = CartDb.PROD_ID + "=?";
                    String[] args = new String[]{prodid};

                    Cursor updator = getContentResolver().query(LocalDbProvider.CART_URI, projection, selection, args, null);
                    updator.moveToFirst();

                    if (updator.isAfterLast()) {
                        values.put(CartDb.PROD_QUANTITY, quantity);
                        getContentResolver().insert(LocalDbProvider.CART_URI, values);
                    } else {

                        int nonLoggedId = 0;

                        SharedPreferences.Editor editor=sharedPreferences.edit();
                        editor.putString("nonLoggedId", String.valueOf(nonLoggedId));
                        editor.apply();

                        String existqty = updator.getString(updator.getColumnIndexOrThrow(CartDb.PROD_QUANTITY));
                        totalqty = Integer.parseInt(existqty) + Integer.parseInt(quantity);
                        values.put(CartDb.PROD_QUANTITY, totalqty);
                        values.put(CartDb.USER_ID, nonLoggedId);
                        getContentResolver().update(LocalDbProvider.CART_URI, values, selection, args);
                    }

                    Toast.makeText(getApplicationContext(), "Successfully Added to cart", Toast.LENGTH_SHORT).show();
                    quantityText.setText("");
                    finish();



                }
            }
        });

        backToProducts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }

    private void updateFields(Cursor cursor) {
        prodName= (TextView) findViewById(R.id.product_details_name);
        proImg = (ImageView) findViewById(R.id.imageView2);
        prodCode= (TextView) findViewById(R.id.product_details_code);
        prodDescription= (TextView) findViewById(R.id.product_details_description);
        prodPrice= (TextView) findViewById(R.id.product_details_price);

        //retieve weight
        String weightID= cursor.getString(cursor.getColumnIndexOrThrow(ProductsDb.PROD_WEIGHT_ID));
        String[] projection=new String[]{
                WeightDb._ID,
                WeightDb.WEIGHT_ID,
                WeightDb.PROD_WEIGHT,
                WeightDb.PROD_UNIT
        };
        Cursor weighCursor=getContentResolver().query(LocalDbProvider.WEIGHT_URI,projection, WeightDb.WEIGHT_ID+"=?",new String[]{weightID},null);
        String productWeight = null,weightUnit = null;
        if (weighCursor.moveToFirst()){
            productWeight=weighCursor.getString(weighCursor.getColumnIndexOrThrow(WeightDb.PROD_WEIGHT));
            weightUnit =weighCursor.getString(weighCursor.getColumnIndexOrThrow(WeightDb.PROD_UNIT));
        }

        String url=cursor.getString(cursor.getColumnIndexOrThrow(ProductsDb.PROD_IMAGE));

        prodName.setText(cursor.getString(cursor.getColumnIndexOrThrow(ProductsDb.PROD_NAME))+" "+productWeight+weightUnit);
        prodCode.setText(cursor.getString(cursor.getColumnIndexOrThrow(ProductsDb.PROD_ID)));
        prodPrice.setText(cursor.getString(cursor.getColumnIndexOrThrow(sharedPreferences.getString(AppConstants.PREFERENCES_CUSTOMER_PRICE_TYPE,ProductsDb.PROD_CFSTOCKIST_PRICE))));
        prodDescription.setText(cursor.getString(cursor.getColumnIndexOrThrow(ProductsDb.PROD_DESCRIPTION)));

        String Img_Url= "http://orders.pandafoods.co.in/"+url;
        Log.i("Image Url detail page",Img_Url);

        //Picasso.with(this).load(Img_Url).into(proImg);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == 16908332) {
            finish();
//            onBackPressed();
//            Intent intent=new Intent(getApplicationContext(),ProductMainActivity.class);
//            intent.putExtra(CatagoryDb.CAT_ID,cat_id);
//            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

        @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_panda, menu);

        RelativeLayout rl_viewBag = (RelativeLayout) menu.findItem(R.id.badge).getActionView();
        String cartCount=sharedPreferences.getString(AppConstants.cartcount,"0");
        Log.i("Cart Value second",cartCount);
        notifCount = (TextView)rl_viewBag.findViewById(R.id.actionbar_notifcation_textview);
        notifCount.setText(cartCount);

        rl_viewBag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cartIntent = new Intent(getApplicationContext(), CartShowActivity.class);
                startActivity(cartIntent);
            }
        });
        return true;
    }


}
