package com.sha.netstager.pandafoods.activity;

import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.sha.netstager.pandafoods.R;
import com.sha.netstager.pandafoods.fragments.ProductListFragment;
import com.sha.netstager.pandafoods.util.AppConstants;

public class CartShowActivity extends PandaActivity {

    private ProductListFragment productListFragment;
    private float totalcash=0;

    private SharedPreferences sharedPreferences;
    Button cart_continue_shop_btn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        ActionBar actionBar=getSupportActionBar();
        actionBar.setHomeButtonEnabled(false);

        setContentView(R.layout.activity_cart_show);
        sharedPreferences = getSharedPreferences(AppConstants.PANDA_PREFERENCES, Context.MODE_PRIVATE);
        cart_continue_shop_btn= (Button) findViewById(R.id.cart_continue_shop_btn);
        String shopcontnew = sharedPreferences.getString(AppConstants.xShop,"0");

        if(shopcontnew.equals("true")){
            cart_continue_shop_btn.setVisibility(View.GONE);
        }
        FrameLayout productLayout = (FrameLayout) findViewById(R.id.product_list_fragment);
        if (savedInstanceState == null) {
            //instantiate ProductListFragment  and add into framelayout
            productListFragment = ProductListFragment.newInstance(1,"");
            FragmentTransaction ft = getFragmentManager().beginTransaction();
            ft.add(productLayout.getId(), productListFragment);
            ft.commit();
        }
        actionBar.setTitle(R.string.pro_cart);
    }

    @Override
    public void onBackPressed() {
        String shopcontnew=sharedPreferences.getString(AppConstants.xShop,"0");
        if(shopcontnew.equals("true")){
            Toast.makeText(this,"Please make your order first !!",Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        return true;
    }



    //give actions to buttons
    public void cartActions(View view) {
        switch (view.getId()) {
            //action for order button, go to OrderProductsActivity if anything purchased
            case R.id.cart_order_btn:
                if (totalcash!=0) {

                    if((sharedPreferences.getString(AppConstants.PREFERENCES_USER_ID,"0").equals("0")&&sharedPreferences.getString(AppConstants.PREFERENCES_EXE_ID,"0").equals("0"))){
                        Intent intent = new Intent(getApplicationContext(), LoginAuthActivity.class);
                        intent.putExtra("login_type", AppConstants.TYPE_CUSTOMER_LOGIN);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        finish();
                    }
                    else
                    {
//                        String userVLogIn = sharedPreferences.getString("EmailTag", null);
//                        String exe_id = sharedPreferences.getString("exe_id", null);
//                        String cust_id = sharedPreferences.getString("exe_id", null);
//
//                        Log.i("User was logged check",userVLogIn);
//                        Log.i("User was logged check",exe_id);
//                        Log.i("User was logged check",cust_id);

                        //editor.putString("EmailTag",mEmailView.getText().toString());
                        Intent orderIntent = new Intent(getApplicationContext(), OrderProductsActivity.class);
                        startActivity(orderIntent);
                    }


                }
                else {
                    Toast.makeText(getApplicationContext(),"Cart is Empty!",Toast.LENGTH_SHORT).show();
                }
                break;
            //action for Continue Shopping, go to ProductCatagoryActivity
            case R.id.cart_continue_shop_btn:
                Intent productIntent = new Intent(getApplicationContext(), Grid_Home_Activity.class);
                startActivity(productIntent);
                break;
        }
    }

    //setting total cash
    public void totalFragmentDisplay(float totalcash) {
        this.totalcash=totalcash;
    }

    //refresh data set in ProductListFragment
    public void notifyDataSet() {
        productListFragment.refreshData();
    }
}
