package com.sha.netstager.pandafoods.activity;

import android.app.LoaderManager;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CursorAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.sha.netstager.pandafoods.R;
import com.sha.netstager.pandafoods.contentProvider.LocalDbProvider;
import com.sha.netstager.pandafoods.datatables.OrderDb;
import com.sha.netstager.pandafoods.datatables.OrderProductsDb;
import com.sha.netstager.pandafoods.util.AppConstants;
import com.sha.netstager.pandafoods.util.JSONParser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class RecentOrderActivity extends PandaActivity implements LoaderManager.LoaderCallbacks<Cursor> {

    private ListView recentOrderListView;
    private RecentOrderAdapter dataAdapter;
    private String ANDROID_UDID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        actionBar.setTitle(R.string.string_recent_order);
        setContentView(R.layout.activity_recent_order);
        recentOrderListView = (ListView) findViewById(R.id.recent_order_list_view);



        //load listview
        loadListView();
    }

    //method for set adapter to listview
    private void loadListView() {
        // the XML defined views which the data will be bound to
        dataAdapter = new RecentOrderAdapter(getApplicationContext(), null, 0);
        recentOrderListView.setAdapter(dataAdapter);
        //Ensures a loader is initialized and active.
        getLoaderManager().initLoader(0, null, this);

    }


    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        String[] projection = {
                OrderDb._ID,
                OrderDb.USER_ID,
                OrderDb.EXE_ID,
                OrderDb.ORDER_DATE,
                OrderDb.PROMO_CODE,
                OrderDb.ORDER_SEESION_ID,
                OrderDb.PROD_SYNC_STATUS,
                OrderDb.ORDER_AMOUNT
        };
        return new CursorLoader(getApplicationContext(),
                LocalDbProvider.ORDER_URI, projection, null, null, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        dataAdapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        dataAdapter.swapCursor(null);
    }


    // send asynchronously the orders to server with api
    private class SendAsynchronousOrders extends AsyncTask<String, Void, String> {
        private ProgressDialog dialog;
        private String contentid;

        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(RecentOrderActivity.this);
            dialog.setMessage(RecentOrderActivity.this
                    .getString(R.string.string_progress_loading));
            dialog.setCancelable(false);
            dialog.show();
        }


        public SendAsynchronousOrders(String contentid) {
            this.contentid = contentid;

        }

        //send order to the api
        @Override
        protected String doInBackground(String... params) {
//            Log.i("json_req",params[0]);
            return JSONParser.makeHttpRequest(AppConstants.URL_ORDER, "POST", params[0]);
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            dialog.dismiss();
        }


        @Override
        protected void onPostExecute(String result) {
            try {
                dialog.dismiss();
                JSONObject object = new JSONObject(result);
                // retrieve the response and update order status
                if (object.getString("status").equals("success")) {
                    ContentValues values = new ContentValues();
                    values.put(OrderDb.ORDER_SEESION_ID, object.getString(OrderDb.ORDER_SEESION_ID));
                    values.put(OrderDb.PROD_SYNC_STATUS, "1");
                    getContentResolver().update(LocalDbProvider.ORDER_URI, values, OrderDb._ID + "=?", new String[]{contentid});
                    Toast.makeText(getApplicationContext(), "Order Sent!", Toast.LENGTH_SHORT).show();
                    finish();
                    //after updating launch RecentOrderActivity
                    Intent intent = new Intent(getApplicationContext(), RecentOrderActivity.class);
                    startActivity(intent);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    //custom CursorAdapter class for display recent orders
    private class RecentOrderAdapter extends CursorAdapter {
        private LayoutInflater mInflater;
        private Context context;


        private RecentOrderAdapter(Context context, Cursor c, int flags) {
            super(context, c, flags);
            this.context = context;
            mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        //binding the views
        @Override
        public void bindView(View view, final Context context, final Cursor cursor) {
            TextView order_no = (TextView) view.findViewById(R.id.recent_order_no);
            TextView order_date = (TextView) view.findViewById(R.id.recent_order_date);
            TextView order_amount = (TextView) view.findViewById(R.id.recent_order_amount);
            order_no.setText(cursor.getString(cursor.getColumnIndexOrThrow(OrderDb._ID)));
            order_date.setText(cursor.getString(cursor.getColumnIndexOrThrow(OrderDb.ORDER_DATE)));
            order_amount.setText("\u20B9" + cursor.getString(cursor.getColumnIndexOrThrow(OrderDb.ORDER_AMOUNT)));
        }



        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            final View view = super.getView(position, convertView, parent);
            TextView order_details = (TextView) view.findViewById(R.id.recent_order_details);
            //set onclick listener for orderdetails view,when click it goes to OrderedProductShowActivity
            order_details.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Cursor c = (Cursor) getItem(position);
                    Intent intent = new Intent(context, OrderedProductShowActivity.class);
                    intent.putExtra(OrderProductsDb.ORDER_ID, c.getString(c.getColumnIndexOrThrow(OrderDb._ID)));
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                }
            });

            // get cursor data of that position, and set sync status icon according to status of order
            Cursor cursor = (Cursor) getItem(position);
            Button synStatus = (Button) view.findViewById(R.id.recent_order_sync);
            synStatus.setTag(position);
            String sync = cursor.getString(cursor.getColumnIndexOrThrow(OrderDb.PROD_SYNC_STATUS));


            if (sync.equals("0")) {
                synStatus.setBackgroundResource(R.drawable.sync);
                //set onclick listener for send the order
                synStatus.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Cursor btncursor = (Cursor) getItem((Integer) v.getTag());
                        ANDROID_UDID = Settings.Secure.getString(getContentResolver(),
                                Settings.Secure.ANDROID_ID);
                        Cursor productsCursor = retrieveProductCursor(btncursor.getString(btncursor.getColumnIndexOrThrow(OrderProductsDb._ID)));
                        try {
//                            prepare JSONobject according to api format
                            JSONArray productArray = new JSONArray();
                            if (productsCursor.moveToFirst())
                                do {
                                    JSONObject orderProductObject = new JSONObject();
                                    orderProductObject.put(OrderProductsDb.PROD_ID, productsCursor.getString(productsCursor.getColumnIndexOrThrow(OrderProductsDb.PROD_ID)));
                                    orderProductObject.put(OrderProductsDb.PROD_QUANTITY, productsCursor.getString(productsCursor.getColumnIndexOrThrow(OrderProductsDb.PROD_QUANTITY)));
                                    orderProductObject.put(OrderProductsDb.PROD_PRICE, productsCursor.getString(productsCursor.getColumnIndexOrThrow(OrderProductsDb.PROD_PRICE)));
                                    orderProductObject.put(OrderProductsDb.PROD_CATEGORY, productsCursor.getString(productsCursor.getColumnIndexOrThrow(OrderProductsDb.PROD_CATEGORY)));
                                    orderProductObject.put(OrderProductsDb.PROD_GROUP, productsCursor.getString(productsCursor.getColumnIndexOrThrow(OrderProductsDb.PROD_GROUP)));

                                    productArray.put(orderProductObject);
                                } while (productsCursor.moveToNext());
                            productsCursor.close();

                            JSONObject orderobject = new JSONObject();
                            orderobject.put("ud_id", ANDROID_UDID);
                            orderobject.put(OrderDb.USER_ID, btncursor.getString(btncursor.getColumnIndexOrThrow(OrderDb.USER_ID)));
                            orderobject.put(OrderDb.EXE_ID, btncursor.getString(btncursor.getColumnIndexOrThrow(OrderDb.EXE_ID)));
                            orderobject.put(OrderDb.PROMO_CODE, btncursor.getString(btncursor.getColumnIndexOrThrow(OrderDb.PROMO_CODE)));
                            orderobject.put(OrderDb.ORDER_DATE, btncursor.getString(btncursor.getColumnIndexOrThrow(OrderDb.ORDER_DATE)));
                            orderobject.put("order_details", productArray);
                            JSONObject mainProductObject = new JSONObject();
                            mainProductObject.put("order_details_main", orderobject.toString());
                            //send the order object to api
                            new SendAsynchronousOrders(btncursor.getString(btncursor.getColumnIndexOrThrow(OrderProductsDb._ID))).execute(mainProductObject.toString());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                });
            } else {
                synStatus.setBackgroundResource(R.drawable.done);
            }

            return view;
        }

        @Override
        public Cursor swapCursor(Cursor newCursor) {
            return super.swapCursor(newCursor);
        }


        @Override
        public View newView(Context context, Cursor cursor, ViewGroup parent) {
            return mInflater.inflate(R.layout.view_recent_order_list_item, parent, false);
        }
        //method for retrieve product cursor
        private Cursor retrieveProductCursor(String orderid) {
            String[] projection = new String[]{
                    OrderProductsDb._ID,
                    OrderProductsDb.ORDER_ID,
                    OrderProductsDb.PROD_CATEGORY,
                    OrderProductsDb.PROD_GROUP,
                    OrderProductsDb.PROD_ID,
                    OrderProductsDb.PROD_QUANTITY,
                    OrderProductsDb.PROD_PRICE
            };

            Cursor cursor = getContentResolver().query(LocalDbProvider.ORDER_PRODUCT_URI, projection, OrderProductsDb.ORDER_ID + "=?", new String[]{orderid}, null);

            return cursor;
        }
    }
}
