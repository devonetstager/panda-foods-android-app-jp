package com.sha.netstager.pandafoods.activity;

import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import com.sha.netstager.pandafoods.R;
import com.sha.netstager.pandafoods.adapters.ProductListCursorAdapter;
import com.sha.netstager.pandafoods.contentProvider.LocalDbProvider;
import com.sha.netstager.pandafoods.datatables.CatagoryDb;
import com.sha.netstager.pandafoods.util.AppConstants;

public class SubCatagoryActivity extends PandaActivity implements LoaderManager.LoaderCallbacks<Cursor> {

    private ListView subCatrgoryView;
    private String cat_id;
    private static ProductListCursorAdapter dataAdapter;
    private SimpleCursorAdapter simpleCursorAdapter;
    private TextView product_availability_view;
    private ImageView imgThumb;
    private SharedPreferences preferences;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub_catagory);
        preferences=getSharedPreferences(AppConstants.PANDA_PREFERENCES,MODE_PRIVATE);

        String userid=preferences.getString(AppConstants.logCheck,"0");



        Bundle bundle = getIntent().getExtras();
        cat_id = bundle.getString(CatagoryDb.CAT_ID);
        subCatrgoryView= (ListView) findViewById(R.id.sub_catagoryListView);
        product_availability_view = (TextView) findViewById(R.id.product_availability_display);
        //simpleCursorAdapter=new SimpleCursorAdapter(getApplicationContext(),R.layout.view_product_list_subcatagory,null,new String[]{CatagoryDb.CAT_NAME},new int[]{R.id.sub_catagoryName},0);
        dataAdapter = new ProductListCursorAdapter(this,null,0);
        subCatrgoryView.setAdapter(dataAdapter);
        //set adapter of subcatagoryview listview


        subCatrgoryView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//              if a subcatagory item clicked retrieve its cursor data and pass into ProductMainActivity

                Cursor cursor= (Cursor) subCatrgoryView.getItemAtPosition(position);
                String Img_Url = cursor.getString(cursor.getColumnIndexOrThrow(CatagoryDb.CAT_IMAGE));
                String Img_DirctUrl= "http://orders.pandafoods.co.in/"+Img_Url;

                SharedPreferences.Editor editor=preferences.edit();
                editor.putString("itmImgUrl",Img_DirctUrl);
                editor.apply();

                Log.i("Cat Img",Img_DirctUrl);

                Intent intent=new Intent(getApplicationContext(),ProductMainActivity.class);
                intent.putExtra(CatagoryDb.CAT_ID,cursor.getString(cursor.getColumnIndexOrThrow(CatagoryDb.CAT_ID)));
                startActivity(intent);
            }
        });
        // init loader
        getLoaderManager().initLoader(0,null,this);
    }


    // here init loader and load subcategory data
    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        String[] projection=new String[]{
                CatagoryDb._ID,
                CatagoryDb.CAT_ID,
                CatagoryDb.CAT_NAME,
                CatagoryDb.CAT_IMAGE,
                CatagoryDb.PARENT_CAT,
                CatagoryDb.CAT_DESCRIPTION,
        };
        return new CursorLoader(this, LocalDbProvider.CATAGORY_URI,projection,CatagoryDb.PARENT_CAT+"=?",new String[]{cat_id},null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        dataAdapter.swapCursor(data);
        if (!data.moveToFirst()) {
            product_availability_view.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        dataAdapter.swapCursor(null);
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_panda, menu);
//
//        RelativeLayout rl_viewBag = (RelativeLayout) menu.findItem(R.id.badge).getActionView();
//        String cartCount=preferences.getString(AppConstants.cartcount,"0");
//        Log.i("Cart Value in backed",cartCount);
//        notifCount = (TextView)rl_viewBag.findViewById(R.id.actionbar_notifcation_textview);
//        notifCount.setText(cartCount);
//
//        rl_viewBag.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent cartIntent = new Intent(getApplicationContext(), CartShowActivity.class);
//                startActivity(cartIntent);
//            }
//        });
//        return true;
//    }
}
