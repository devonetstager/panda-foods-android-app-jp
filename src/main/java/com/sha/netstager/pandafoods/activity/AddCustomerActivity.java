package com.sha.netstager.pandafoods.activity;

import android.app.DialogFragment;
import android.app.LoaderManager;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Loader;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SimpleCursorAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.sha.netstager.pandafoods.R;
import com.sha.netstager.pandafoods.contentProvider.LocalDbProvider;
import com.sha.netstager.pandafoods.datatables.CartDb;
import com.sha.netstager.pandafoods.datatables.CountryDb;
import com.sha.netstager.pandafoods.datatables.DistrictDb;
import com.sha.netstager.pandafoods.datatables.LocationDb;
import com.sha.netstager.pandafoods.datatables.StateDb;
import com.sha.netstager.pandafoods.util.AppConstants;
import com.sha.netstager.pandafoods.util.JSONParser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class AddCustomerActivity extends PandaActivity implements LoaderManager.LoaderCallbacks<Cursor> {
    private EditText editCustomerName, editEmail, editPassword, editConfirmPassword, editPhone, editAddress;
    private Spinner editCustomerType, editLocation, editCountry,editState,editDistrict;
    private Button btnSignUp;
    private SharedPreferences sharedPreferences;
    private static final String TAG_REQUIRE="field is required";
    private static final String TAG_PASSWORD_MATCH="password not match";
    private static final String TAG_EMAIL_FORMAT="not a valied email";
    private static final String TAG_PASSWORD_LENGTH="password doesnt have sufficient length";

    private SimpleCursorAdapter countryAdapter,stateAdapter,districtAdapter,locationAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_customer);
        sharedPreferences= getSharedPreferences(AppConstants.PANDA_PREFERENCES,MODE_PRIVATE);
        assignIdsForView();
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

        boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();
        if (isConnected){
            new LoadLocations().execute();
        }else {
            Toast.makeText(getApplicationContext(),"Network is not connected",Toast.LENGTH_SHORT).show();
        }

        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registerCustomer();
            }
        });
    }

    private void loadLocInfo() {
        getLoaderManager().initLoader(1, null, this);

//        set on item selected listener for country,state,city
        editCountry.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                reload state list according to country
                Cursor cursor = (Cursor) countryAdapter.getItem(position);
                Bundle bundle = new Bundle();
                bundle.putString(StateDb.COUNTRY_ID, cursor.getString(cursor.getColumnIndexOrThrow(CountryDb.COUNTRY_ID)));
                getLoaderManager().destroyLoader(2);
                getLoaderManager().destroyLoader(3);
                getLoaderManager().destroyLoader(4);
                getLoaderManager().initLoader(2, bundle, AddCustomerActivity.this);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        editState.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                reload district list according to state
                Cursor cursor = (Cursor) stateAdapter.getItem(position);
                Bundle bundle = new Bundle();
                bundle.putString(DistrictDb.STATE_ID, cursor.getString(cursor.getColumnIndexOrThrow(StateDb.STATE_ID)));
                getLoaderManager().destroyLoader(3);
                getLoaderManager().destroyLoader(4);

                getLoaderManager().initLoader(3, bundle, AddCustomerActivity.this);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        editDistrict.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                reload location list according to district
                Cursor cursor = (Cursor) districtAdapter.getItem(position);
                Bundle bundle = new Bundle();
                bundle.putString(LocationDb.DISTRICT_ID, cursor.getString(cursor.getColumnIndexOrThrow(DistrictDb.DISTRICT_ID)));
                getLoaderManager().destroyLoader(4);

                getLoaderManager().initLoader(4, bundle, AddCustomerActivity.this);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void registerCustomer() {
        String customer_name= editCustomerName.getText().toString();
        String email= editEmail.getText().toString();
        String password= editPassword.getText().toString();
        String confirm_password= editConfirmPassword.getText().toString();
        String phonenumber= editPhone.getText().toString();
        String address= editAddress.getText().toString();
        if(TextUtils.isEmpty(customer_name)){
            editCustomerName.setError(TAG_REQUIRE);
        }
        else if(TextUtils.isEmpty(email)){
            editEmail.setError(TAG_REQUIRE);

        }
        else if (!email.contains("@")) {
            editEmail.setError(TAG_EMAIL_FORMAT);
        }
        else if (TextUtils.isEmpty(address)){
            editAddress.setError(TAG_REQUIRE);
        }
        else if (TextUtils.isEmpty(phonenumber)){
            editPhone.setError(TAG_REQUIRE);
        }
        else if (!password.equals(confirm_password)) {
            editPassword.setError(TAG_PASSWORD_MATCH);
        }
        else if (password.length()<5){
            editPassword.setError(TAG_PASSWORD_LENGTH);
        }
        else if (editCustomerType.getSelectedItemPosition()==0){
            Toast.makeText(getApplicationContext(), "Select Customer Type", Toast.LENGTH_SHORT).show();
        }
//        else if (editLocation.getSelectedItemPosition()==0){
//            Toast.makeText(getApplicationContext(),"Select Location",Toast.LENGTH_SHORT).show();
//        }
        else {
            JSONObject requestObj=new JSONObject();
            try {
                requestObj.put("cust_name",customer_name);
                Cursor c= (Cursor) editLocation.getSelectedItem();
                requestObj.put("exe_id",sharedPreferences.getString(AppConstants.PREFERENCES_EXE_ID,"0"));
                requestObj.put(AppConstants.PREFERENCES_USER_LOCATION,c.getString(c.getColumnIndexOrThrow(LocationDb.LOC_ID)));
                requestObj.put("cust_phone",phonenumber);
                requestObj.put("cust_email",email);
                requestObj.put("cust_address",address);
                requestObj.put("cust_password",password);
               // requestObj.put("cust_type_id",editCustomerType.getSelectedItemPosition());
                requestObj.put("cust_type_id","0");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            new Attemptlogin().execute(requestObj.toString());

        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    private void assignIdsForView() {
        editCustomerName = (EditText) findViewById(R.id.signup_customer_name);
        editEmail = (EditText) findViewById(R.id.signup_email);
        editAddress = (EditText) findViewById(R.id.signup_address);
        editLocation = (Spinner) findViewById(R.id.signup_location);
        editPassword = (EditText) findViewById(R.id.signup_password);
        editConfirmPassword = (EditText) findViewById(R.id.signup_confirm_password);
        editPhone = (EditText) findViewById(R.id.signup_phonenumber);
        //editCustomerType = (Spinner) findViewById(R.id.signup_customer_type);
        editCountry = (Spinner) findViewById(R.id.countrySpinner);
        editState = (Spinner) findViewById(R.id.stateSpinner);
        editDistrict = (Spinner) findViewById(R.id.districtSpinner);
        btnSignUp = (Button) findViewById(R.id.signup_btn_register);

        countryAdapter=new SimpleCursorAdapter(getApplicationContext(),R.layout.view_location_spinner_item,null,new String[]{CountryDb.COUNTRY},new int[]{R.id.loc_view_item},0);
        stateAdapter=new SimpleCursorAdapter(getApplicationContext(),R.layout.view_location_spinner_item,null,new String[]{StateDb.STATE},new int[]{R.id.loc_view_item},0);
        districtAdapter=new SimpleCursorAdapter(getApplicationContext(),R.layout.view_location_spinner_item,null,new String[]{DistrictDb.DISTRICT},new int[]{R.id.loc_view_item},0);
        locationAdapter=new SimpleCursorAdapter(getApplicationContext(),R.layout.view_location_spinner_item,null,new String[]{LocationDb.LOCATION},new int[]{R.id.loc_view_item},0);

        editCountry.setAdapter(countryAdapter);
        editState.setAdapter(stateAdapter);
        editDistrict.setAdapter(districtAdapter);
        editLocation.setAdapter(locationAdapter);
    }

    private void insertToDB(JSONObject responseObject){
        try {
            JSONObject detailsObject = responseObject.getJSONObject("details");
            JSONArray countryArray = detailsObject.getJSONArray("country");
            JSONArray stateArray = detailsObject.getJSONArray("state");
            JSONArray districtArray = detailsObject.getJSONArray("district");
            JSONArray locArray = detailsObject.getJSONArray("location");

            //retrieve country,state, district and location
            ContentValues[] country_values = new ContentValues[countryArray.length()];
            for (int i = 0; i < countryArray.length(); i++) {
                JSONObject singleCountryObject = countryArray.getJSONObject(i);
                country_values[i] = retrieveCountryValues(singleCountryObject);
            }
            getContentResolver().delete(LocalDbProvider.COUNTRY_URI,null,null);
            getContentResolver().bulkInsert(LocalDbProvider.COUNTRY_URI, country_values);

            ContentValues[] state_values = new ContentValues[stateArray.length()];
            for (int i = 0; i < stateArray.length(); i++) {
                JSONObject singleStateObject = stateArray.getJSONObject(i);
                state_values[i] = retrieveStateValues(singleStateObject);
            }
            getContentResolver().delete(LocalDbProvider.STATE_URI,null,null);
            getContentResolver().bulkInsert(LocalDbProvider.STATE_URI, state_values);

            ContentValues[] district_values = new ContentValues[districtArray.length()];
            for (int i = 0; i < districtArray.length(); i++) {
                JSONObject singleDistrictObject = districtArray.getJSONObject(i);
                district_values[i] = retrieveDistriceValues(singleDistrictObject);
            }
            getContentResolver().delete(LocalDbProvider.DISTRICT_URI,null,null);
            getContentResolver().bulkInsert(LocalDbProvider.DISTRICT_URI, district_values);

            ContentValues[] loc_values = new ContentValues[locArray.length()];
            for (int i = 0; i < locArray.length(); i++) {
                JSONObject singleLocObject = locArray.getJSONObject(i);
                loc_values[i] = retrieveLocValues(singleLocObject);
            }
            getContentResolver().delete(LocalDbProvider.LOC_URI,null,null);
            getContentResolver().bulkInsert(LocalDbProvider.LOC_URI, loc_values);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    private ContentValues retrieveCountryValues(JSONObject singleLocObject) {
        ContentValues countryvalues = new ContentValues();
        try {
            countryvalues.put(CountryDb.COUNTRY_ID, singleLocObject.getString(CountryDb.COUNTRY_ID));
            countryvalues.put(CountryDb.COUNTRY, singleLocObject.getString(CountryDb.COUNTRY));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return countryvalues;
    }

    private ContentValues retrieveStateValues(JSONObject singleLocObject) {
        ContentValues statevalues = new ContentValues();
        try {
            statevalues.put(StateDb.STATE_ID, singleLocObject.getString(StateDb.STATE_ID));
            statevalues.put(StateDb.STATE, singleLocObject.getString(StateDb.STATE));
            statevalues.put(StateDb.COUNTRY_ID, singleLocObject.getString(StateDb.COUNTRY_ID));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return statevalues;
    }

    private ContentValues retrieveDistriceValues(JSONObject singleLocObject) {
        ContentValues districtvalues = new ContentValues();
        try {
            districtvalues.put(DistrictDb.DISTRICT_ID, singleLocObject.getString(DistrictDb.DISTRICT_ID));
            districtvalues.put(DistrictDb.DISTRICT, singleLocObject.getString(DistrictDb.DISTRICT));
            districtvalues.put(DistrictDb.STATE_ID, singleLocObject.getString(DistrictDb.STATE_ID));
            districtvalues.put(DistrictDb.COUNTRY_ID, singleLocObject.getString(DistrictDb.COUNTRY_ID));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return districtvalues;
    }

    private ContentValues retrieveLocValues(JSONObject singleLocObject) {
        ContentValues locvalues = new ContentValues();
        try {
            locvalues.put(LocationDb.LOC_ID, singleLocObject.getString(LocationDb.LOC_ID));
            locvalues.put(LocationDb.LOCATION, singleLocObject.getString(LocationDb.LOCATION));
            locvalues.put(LocationDb.DISTRICT_ID, singleLocObject.getString(LocationDb.DISTRICT_ID));
            locvalues.put(LocationDb.STATE_ID, singleLocObject.getString(LocationDb.STATE_ID));
            locvalues.put(LocationDb.COUNTRY_ID, singleLocObject.getString(LocationDb.COUNTRY_ID));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return locvalues;
    }



    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        switch (id){
            case 1:
                return new CursorLoader(getApplicationContext(),LocalDbProvider.COUNTRY_URI,new String[]{CountryDb._ID,CountryDb.COUNTRY_ID,CountryDb.COUNTRY},null,null,null);
            case 2:
                if (args!=null) {
                    return new CursorLoader(getApplicationContext(), LocalDbProvider.STATE_URI, new String[]{StateDb._ID, StateDb.COUNTRY_ID, StateDb.STATE_ID, StateDb.STATE}, CountryDb.COUNTRY_ID + "=?", new String[]{args.getString(CountryDb.COUNTRY_ID)}, null);
                }
            case 3:
                if (args!=null) {
                    return new CursorLoader(getApplicationContext(), LocalDbProvider.DISTRICT_URI, new String[]{DistrictDb._ID, DistrictDb.DISTRICT_ID, DistrictDb.DISTRICT, DistrictDb.STATE_ID}, DistrictDb.STATE_ID + "=?", new String[]{args.getString(StateDb.STATE_ID)}, null);
                }
            case 4:
                if (args!=null) {
                    return new CursorLoader(getApplicationContext(), LocalDbProvider.LOC_URI, new String[]{LocationDb._ID, LocationDb.DISTRICT_ID, LocationDb.LOC_ID, LocationDb.LOCATION}, LocationDb.DISTRICT_ID + "=?", new String[]{args.getString(LocationDb.DISTRICT_ID)}, null);
                }
            default:
                break;

        }
        return null;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        switch (loader.getId()){
            case 1:
                countryAdapter.swapCursor(data);

                break;
            case 2:
                stateAdapter.swapCursor(data);
                break;
            case 3:
                districtAdapter.swapCursor(data);
                break;
            case 4:
                locationAdapter.swapCursor(data);
                break;
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        switch (loader.getId()){
            case 1:
                countryAdapter.swapCursor(null);
                break;
            case 2:
                stateAdapter.swapCursor(null);
                break;
            case 3:
                districtAdapter.swapCursor(null);
                break;
            case 4:
                locationAdapter.swapCursor(null);
                break;
        }

    }


    private class Attemptlogin extends AsyncTask<String, Void, String> {
        private ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(AddCustomerActivity.this);
            dialog.setMessage(AddCustomerActivity.this
                    .getString(R.string.string_progress_loading));
            dialog.setCancelable(false);
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            return JSONParser.makeHttpRequest(AppConstants.URL_ADD_CUSTOMER,"POST",params[0]);
        }



        @Override
        protected void onPostExecute(String response) {
            try {
                Log.i("strts",response);
                JSONObject responseObject=new JSONObject(response);
                Toast.makeText(getApplicationContext(),responseObject.getString("description"),Toast.LENGTH_SHORT).show();
               // Intent intent=new Intent(getApplicationContext(),ConfirmSignUpActivity.class);

//                switch (responseObject.getInt("status"))
//                {
//                    case 1:
//                        intent.putExtra("confirm_type",1);
//                        break;
//                    case 2:
//                        intent.putExtra("confirm_type",2);
//                        break;
//                    case 3:
//                        intent.putExtra("confirm_type",3);
//                        break;
//                }
                if (responseObject.getInt("status")==2){
                    Toast.makeText(getApplicationContext(),"Customer Added",Toast.LENGTH_SHORT).show();
                }

                finish();
              //  startActivity(intent);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            dialog.dismiss();
        }

        @Override
        protected void onCancelled() {
            dialog.dismiss();
        }
    }

    private class LoadLocations extends AsyncTask<Void, Void, String> {
        private ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(AddCustomerActivity.this);
            dialog.setMessage(AddCustomerActivity.this
                    .getString(R.string.string_progress_loading));
            dialog.setCancelable(false);
            dialog.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            return JSONParser.makeHttpRequest(AppConstants.URL_PREDATA_FETCH, "POST", "");
        }

        @Override
        protected void onPostExecute(String response) {
            try {
                Log.i("str", response);
                JSONObject responseObject = new JSONObject(response);
                insertToDB(responseObject);
                loadLocInfo();
//                Toast.makeText(getApplicationContext(),responseObject.getString("description"),Toast.LENGTH_SHORT).show();

//                Intent intent=new Intent(getApplicationContext(),ConfirmSignUpActivity.class);
//
//                startActivity(intent);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            dialog.dismiss();
        }

        @Override
        protected void onCancelled() {
            dialog.dismiss();
        }
    }


    public static class AddToCartFragment extends DialogFragment {
        private String procode;
        private int existquanity=0;
        private SharedPreferences sharedPreferences;
        private EditText dialoguequantityfield;

        public AddToCartFragment(){

        }
       public static AddToCartFragment getInstance(String procode){
           AddToCartFragment fragment = new AddToCartFragment();
           Bundle args = new Bundle();
           args.putString("procode", procode);
           fragment.setArguments(args);
           return fragment;
       }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.view_fragment_edit_quantity, container,
                    false);
            if (getArguments() != null) {
                procode = getArguments().getString("procode");
                sharedPreferences= getActivity().getSharedPreferences(AppConstants.PANDA_PREFERENCES, Context.MODE_PRIVATE);
                String loggedCheck=sharedPreferences.getString(AppConstants.logCheck,"0");
                Log.i("loggin id in fragment", loggedCheck);
                if (loggedCheck.equals("0"))
                {
                    Cursor cursor= getActivity().getContentResolver().query(LocalDbProvider.CART_URI,new String[]{CartDb._ID,CartDb.PROD_ID,CartDb.PROD_QUANTITY},CartDb.USER_ID+"=? AND " + CartDb.PROD_ID+"= ?",new String[]{sharedPreferences.getString(AppConstants.logCheck,"0"),procode},null);
                    if (cursor.moveToFirst()) existquanity= Integer.parseInt(cursor.getString(cursor.getColumnIndexOrThrow(CartDb.PROD_QUANTITY)));
                    else existquanity=0;
                    cursor.close();
                }
                else
                {
                    Cursor cursor= getActivity().getContentResolver().query(LocalDbProvider.CART_URI,new String[]{CartDb._ID,CartDb.PROD_ID,CartDb.PROD_QUANTITY},CartDb.USER_ID+"=? AND " + CartDb.PROD_ID+"= ?",new String[]{sharedPreferences.getString(AppConstants.PREFERENCES_USER_ID,"0"),procode},null);
                    if (cursor.moveToFirst()) existquanity= Integer.parseInt(cursor.getString(cursor.getColumnIndexOrThrow(CartDb.PROD_QUANTITY)));
                    else existquanity=0;
                    cursor.close();
                }

            }

            dialoguequantityfield= (EditText) rootView.findViewById(R.id.quantity_field);
            Button cancel_btn = (Button) rootView.findViewById(R.id.close_btn);
            Button remove_btn = (Button) rootView.findViewById(R.id.remove_btn);
            Button edit_btn = (Button) rootView.findViewById(R.id.edit_btn);

            dialoguequantityfield.setText(String.valueOf(existquanity));
            dialoguequantityfield.requestFocus();

            edit_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String quantity = dialoguequantityfield.getText().toString();
                    if (TextUtils.isEmpty(quantity)) {
                        Toast.makeText(getActivity().getApplicationContext(), "Quantity is nil", Toast.LENGTH_SHORT).show();
                    } else if (Integer.parseInt(quantity) == 0) {
                        Toast.makeText(getActivity().getApplicationContext(), "Quantity is 0", Toast.LENGTH_SHORT).show();
                    } else {
                        ContentValues values = new ContentValues();
                        values.put(CartDb.PROD_ID, procode);
                        values.put(CartDb.USER_ID, sharedPreferences.getString(AppConstants.PREFERENCES_USER_ID, "0"));
                        values.put(CartDb.PROD_QUANTITY, quantity);
                        getActivity().getContentResolver().update(LocalDbProvider.CART_URI, values, CartDb.USER_ID + "=? AND " + CartDb.PROD_ID + "= ?", new String[]{sharedPreferences.getString(AppConstants.PREFERENCES_USER_ID, "0"), procode});
                        Toast.makeText(getActivity().getApplicationContext(), "Quantity is updated", Toast.LENGTH_SHORT).show();
                        ((CartShowActivity) getActivity()).notifyDataSet();
                        dismiss();
                    }

                }

            });
            cancel_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dismiss();
                }
            });

            remove_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getActivity().getContentResolver().delete(LocalDbProvider.CART_URI, CartDb.PROD_ID + "=?", new String[]{procode});
                    Toast.makeText(getActivity().getApplicationContext(), "Successfully Removed from cart", Toast.LENGTH_SHORT).show();
                    ((CartShowActivity) getActivity()).notifyDataSet();
                    dismiss();
                }
            });

            getDialog().setTitle("Edit Cart");
            return rootView;
        }
    }
}
