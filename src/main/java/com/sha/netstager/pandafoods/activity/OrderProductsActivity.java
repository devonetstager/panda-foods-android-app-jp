package com.sha.netstager.pandafoods.activity;

import android.annotation.TargetApi;
import android.app.FragmentTransaction;
import android.app.LoaderManager;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.sha.netstager.pandafoods.R;
import com.sha.netstager.pandafoods.adapters.OfferSpinnerAdapter;
import com.sha.netstager.pandafoods.contentProvider.LocalDbProvider;
import com.sha.netstager.pandafoods.datatables.CartDb;
import com.sha.netstager.pandafoods.datatables.OfferDb;
import com.sha.netstager.pandafoods.datatables.OrderDb;
import com.sha.netstager.pandafoods.datatables.OrderProductsDb;
import com.sha.netstager.pandafoods.datatables.ProductsDb;
import com.sha.netstager.pandafoods.fragments.OrderAdressSetFragment;
import com.sha.netstager.pandafoods.fragments.TotalBillFragment;
import com.sha.netstager.pandafoods.util.AppConstants;
import com.sha.netstager.pandafoods.util.JSONParser;
import com.sha.netstager.pandafoods.util.OfferDisplayView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class OrderProductsActivity extends PandaActivity implements LoaderManager.LoaderCallbacks<Cursor> {

    private TotalBillFragment totalBillFragment;
    private FragmentTransaction ft;
    private float totalcash;
    private SharedPreferences preferences;
    private Spinner promotext;
    private OfferSpinnerAdapter dataAdapter;
    private ArrayList<String> freeproducts = new ArrayList<>();
    private String freequantity = "1";
    private String order_discount = "0";
    private LinearLayout offer_selected_view;
    private LinearLayout offer_displaylayer;
    private TextView offer_title, offer_description;
    private int selected = 0;
    private Button addpromo;
    private String ANDROID_UDID;

    private LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_products);

        //initialize the views
        preferences = getSharedPreferences(AppConstants.PANDA_PREFERENCES, MODE_PRIVATE);
        FrameLayout billLayout = (FrameLayout) findViewById(R.id.bill_list_fragment);
        FrameLayout adressLayout = (FrameLayout) findViewById(R.id.order_address_fragment);
        offer_displaylayer = (LinearLayout) findViewById(R.id.offer_displaylayer);
        offer_selected_view = (LinearLayout) findViewById(R.id.offer_selected_view);
        offer_title = (TextView) findViewById(R.id.offer_title);
        offer_description = (TextView) findViewById(R.id.offer_description);


        if (savedInstanceState == null) {
            // instiantiate TotalBillFragment and OrderAdressSetFragment and add into framelayouts in the view

            String userid=preferences.getString(AppConstants.logCheck,"0");
            if(userid.equals("0"))
            {
                Log.i("in my logic",userid);
                totalBillFragment = TotalBillFragment.newInstance(2);

                OrderAdressSetFragment adressSetFragment = OrderAdressSetFragment.newInstance(preferences.getString(AppConstants.logCheck, "0"));
                ft = getFragmentManager().beginTransaction();
                ft.add(billLayout.getId(), totalBillFragment);
                ft.add(adressLayout.getId(), adressSetFragment);
                ft.commit();
            }
            else{
                Log.i("in shaks logic",userid);
                totalBillFragment = TotalBillFragment.newInstance(2);
                String idUsr = preferences.getString(AppConstants.PREFERENCES_USER_ID, "0");
                OrderAdressSetFragment adressSetFragment = OrderAdressSetFragment.newInstance(preferences.getString(AppConstants.PREFERENCES_USER_ID, "0"));
                ft = getFragmentManager().beginTransaction();
                ft.add(billLayout.getId(), totalBillFragment);
                ft.add(adressLayout.getId(), adressSetFragment);
                ft.commit();
            }



        }

        actionBar.setTitle(R.string.string_order);

        addpromo = (Button) findViewById(R.id.addpromo);
        promotext = (Spinner) findViewById(R.id.promoselecter);

        // set OfferSpinnerAdapter instance to promotext view
        dataAdapter = new OfferSpinnerAdapter(getApplicationContext(), null, 0);
        promotext.setAdapter(dataAdapter);

        getLoaderManager().initLoader(0, null, this);

        //set onitemselected listener, when selected display the details into the offerview layer
        promotext.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                offer_selected_view.setVisibility(View.VISIBLE);
                selected = position;
                Cursor cursor = (Cursor) promotext.getSelectedItem();
                offer_title.setText(cursor.getString(cursor.getColumnIndexOrThrow(OfferDb.OFFER_TITLE)));
                offer_description.setText(cursor.getString(cursor.getColumnIndexOrThrow(OfferDb.OFFER_DESCRIPTION)));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //apply promocode if any offer is displayed as applicable and is selected
        addpromo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dataAdapter.isEmpty()) {
                    Toast.makeText(getApplicationContext(), "No Offer Selected", Toast.LENGTH_SHORT).show();
                } else {
                    Cursor promocursor = dataAdapter.getCursor();
                    promocursor.moveToPosition(selected);
//                promocursor.moveToFirst();
                    String promocode = promocursor.getString(promocursor.getColumnIndexOrThrow(OfferDb.PROMO_CODE));
                    promocursor.close();
                    applyPromo(promocode, false);
                }

            }
        });

    }

    //apply promocode by retrievinng offer data
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private boolean applyPromo(String promocode, boolean applyStatus) {
        String[] projection = new String[]{
                OfferDb._ID,
                OfferDb.OFFER_JSON
        };
        Cursor cursor = getContentResolver().query(LocalDbProvider.OFFER_URI, projection, OfferDb.PROMO_CODE + "=?", new String[]{promocode}, null, null);
        if (cursor.moveToFirst()) {
            try {
                JSONObject object = new JSONObject(cursor.getString(cursor.getColumnIndexOrThrow(OfferDb.OFFER_JSON)));
                if (isApplicableUserType(object.getJSONArray("offer_applicable_to")) && isApplicableGroup(object.getJSONArray("offer_applicable_to_group"))) {
                    if (object.getString("offer_type").equals("value")) {
                        if (object.getString("offer_by").equals("price")) {
                            float discountadd = 0;
                            float discount = Float.parseFloat(object.getString("offer_discount"));
                            boolean isDiscount = false;
                            if (object.getString("offer_price_mode").equals("by_range")) {
                                float price_from = Float.parseFloat(object.getString("offer_price_from"));
                                float price_to = Float.parseFloat(object.getString("offer_price_to"));
                                if (price_from <= totalcash && totalcash <= price_to)
                                    isDiscount = true;

                            } else if (object.getString("offer_price_mode").equals("by_limit")) {
                                float offer_price_limit = Float.parseFloat(object.getString("offer_price_limit"));
                                if (totalcash >= offer_price_limit) isDiscount = true;
                            }
                            if (isDiscount) {
                                if (applyStatus) return true;
                                else {
                                    discountadd = discountCalculate(discount, totalcash);
                                    applyDiscountToView(discountadd, discount);
                                }

                            } else {
                                if (applyStatus) return false;
                                Toast.makeText(getApplicationContext(), "Promotion can't be applied to this price range", Toast.LENGTH_SHORT).show();
                            }

                        } else if (object.getString("offer_by").equals("product")) {
                            if (object.getString("offer_by_prod").equals("product")) {
                                JSONArray applicableProd = object.getJSONArray("prod_mod_product");
                                if (object.getString("offer_by_w_n").equals("weight")) {
                                    applyProductDiscounts(object, ProductsDb.PROD_ID, applicableProd, applyStatus);
                                } else if (object.getString("offer_by_w_n").equals("pack")) {
                                    applyPackDiscount(object, ProductsDb.PROD_ID, applicableProd, applyStatus);
                                }
                            } else if (object.getString("offer_by_prod").equals("category")) {
                                JSONArray prodModCatagories = object.getJSONArray("prod_mod_cat");
                                if (object.getString("offer_by_w_n").equals("weight")) {
                                    applyProductDiscounts(object, ProductsDb.PROD_CATEGORY, prodModCatagories, applyStatus);
                                } else if (object.getString("offer_by_w_n").equals("pack")) {
                                    applyPackDiscount(object, ProductsDb.PROD_CATEGORY, prodModCatagories, applyStatus);
                                }
                            } else if (object.getString("offer_by_prod").equals("group")) {
                                JSONArray prodModCatagories = object.getJSONArray("prod_mod_prodgroup");
                                if (object.getString("offer_by_w_n").equals("weight")) {
                                    applyProductDiscounts(object, ProductsDb.PROD_GROUP, prodModCatagories, applyStatus);
                                } else if (object.getString("offer_by_w_n").equals("pack")) {
                                    applyPackDiscount(object, ProductsDb.PROD_GROUP, prodModCatagories, applyStatus);
                                }
                            }
                        }

                    } else if (object.getString("offer_type").equals("product")) {
                        boolean offerApplystatus = false;
                        int suboffers = Integer.parseInt(object.getString("sub_offer_condition_count"));
                        for (int j = 1; j <= suboffers; j++) {
                            if (object.getString("offer_by_prod_" + j).equals("product")) {
                                JSONArray productstoapply = object.getJSONArray("prod_mod_product_" + j);
                                if (object.getString("offer_by_w_n_" + j).equals("weight")) {
                                    offerApplystatus = applyProductOfferByWeight(object, j, ProductsDb.PROD_ID, productstoapply);
                                } else if (object.getString("offer_by_w_n_" + j).equals("pack")) {
                                    offerApplystatus = applyProductOfferByPack(object, j, ProductsDb.PROD_ID, productstoapply);
                                }
                            } else if (object.getString("offer_by_prod_" + j).equals("category")) {
                                JSONArray catagorytoapply = object.getJSONArray("prod_mod_cat_" + j);
                                if (object.getString("offer_by_w_n_" + j).equals("weight")) {
                                    offerApplystatus = applyProductOfferByWeight(object, j, ProductsDb.PROD_CATEGORY, catagorytoapply);
                                } else if (object.getString("offer_by_w_n_" + j).equals("pack")) {
                                    offerApplystatus = applyProductOfferByPack(object, j, ProductsDb.PROD_CATEGORY, catagorytoapply);
                                }
                            } else if (object.getString("offer_by_prod_" + j).equals("pack")) {
                                JSONArray catagorytoapply = object.getJSONArray("prod_mod_prodgroup_" + j);
                                if (object.getString("offer_by_w_n_" + j).equals("weight")) {
                                    offerApplystatus = applyProductOfferByWeight(object, j, ProductsDb.PROD_CATEGORY, catagorytoapply);
                                } else if (object.getString("offer_by_w_n_" + j).equals("pack")) {
                                    offerApplystatus = applyProductOfferByPack(object, j, ProductsDb.PROD_CATEGORY, catagorytoapply);
                                }
                                if (!offerApplystatus) break;
                            }
                            if (offerApplystatus) {
                                if (applyStatus) return true;
                                else {
                                    applyFreeItems(object);
                                }
                            }
                        }

                        if (freeproducts.size() > 0) {
                            offer_displaylayer.setVisibility(View.VISIBLE);
                            promotext.setEnabled(false);
                            addpromo.setEnabled(false);
//                            TextView textView= (TextView) findViewById(R.id.free_productname);
                            for (String prodCode : freeproducts) {
                                Cursor c = getContentResolver().query(LocalDbProvider.CONTENT_URI, new String[]{ProductsDb.PROD_NAME, ProductsDb.PROD_WEIGHT, ProductsDb.PROD_UNIT}, ProductsDb.PROD_ID + "=?", new String[]{prodCode}, null);
                                c.moveToFirst();
//                            textView.setText(c.getString(c.getColumnIndexOrThrow(ProductsDb.PROD_NAME))+" "+c.getString(c.getColumnIndexOrThrow(ProductsDb.PROD_WEIGHT))+c.getString(c.getColumnIndexOrThrow(ProductsDb.PROD_UNIT)));
                                OfferDisplayView offerDisplayView = new OfferDisplayView(getApplicationContext());

                                offerDisplayView.setLayoutParams(params);
                                offerDisplayView.setProductText(c.getString(c.getColumnIndexOrThrow(ProductsDb.PROD_NAME)) + " " + c.getString(c.getColumnIndexOrThrow(ProductsDb.PROD_WEIGHT)) + c.getString(c.getColumnIndexOrThrow(ProductsDb.PROD_UNIT)));
                                offerDisplayView.setQuantityText(freequantity);
                                offer_displaylayer.addView(offerDisplayView);
                                c.close();
                            }
                        }
                    }

                } else {
                    if (applyStatus) return false;
                    Toast.makeText(getApplicationContext(), "Promotion can't be applied to this user", Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        cursor.close();

        return false;
    }


    private boolean applyProductOfferByPack(JSONObject object, int j, String prodId, JSONArray productstoapply) {
        try {
            int numberforoffer = Integer.parseInt(object.getString("offer_by_nos_" + j));
            MatrixCursor productCursor = totalBillFragment.getProductCursor();
            int totalqty = 0;
            if (productCursor.moveToFirst()) {
                do {
                    for (int i = 0; i < productstoapply.length(); i++) {
                        if (productCursor.getString(productCursor.getColumnIndex(prodId)).equals(productstoapply.getString(i))) {
                            int quantity = Integer.parseInt(productCursor.getString(productCursor.getColumnIndexOrThrow(CartDb.PROD_QUANTITY)));
                            totalqty += quantity;


                        }
                    }
                } while (productCursor.moveToNext());
            }
            productCursor.close();
            if (totalqty > numberforoffer) {
                return true;

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return false;
    }

    private boolean applyProductOfferByWeight(JSONObject object, int j, String indexName, JSONArray prodModArray) {
        try {

            float offerByWeight = Integer.parseInt(object.getString("offer_by_weight_" + j));
            String weightunit = object.getString("weight_unit_" + j);
            MatrixCursor productCursor = totalBillFragment.getProductCursor();

            if (productCursor.moveToFirst())
                do {
                    for (int i = 0; i < prodModArray.length(); i++) {
                        if (productCursor.getString(productCursor.getColumnIndexOrThrow(indexName)).equals(prodModArray.getString(i))) {
                            float weightof_product = Float.parseFloat(productCursor.getString(productCursor.getColumnIndexOrThrow(ProductsDb.PROD_WEIGHT)));
                            String weight_unit = productCursor.getString(productCursor.getColumnIndexOrThrow(ProductsDb.PROD_UNIT));
                            int quantity = Integer.parseInt(productCursor.getString(productCursor.getColumnIndexOrThrow(CartDb.PROD_QUANTITY)));
                            float unitedweight = weightEqualize(weightunit, weight_unit, weightof_product);
                            if ((unitedweight * quantity) > offerByWeight) {
                                return true;
                            }
                        }
                    }
                } while (productCursor.moveToNext());
            productCursor.close();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return false;

    }

    private void applyFreeItems(JSONObject object) {
        try {
            JSONArray offeredproducts = object.getJSONArray("prod_mod_offer_products");
            freequantity = object.getString("prod_mod_offer_products_nos");
            for (int j = 0; j < offeredproducts.length(); j++) {
                freeproducts.add(offeredproducts.getString(j));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private boolean applyPackDiscount(JSONObject object, String indexName, JSONArray prodModCatagories, boolean applyStatus) {
        try {
            boolean flag = false;
            int offerBynumber = Integer.parseInt(object.getString("offer_by_nos"));
            float discount = Float.parseFloat(object.getString("offer_discount"));
            MatrixCursor productCursor = totalBillFragment.getProductCursor();
            float totaldiscount = 0;
            while (productCursor.moveToNext()) {
                for (int i = 0; i < prodModCatagories.length(); i++) {
                    if (productCursor.getString(productCursor.getColumnIndex(indexName)).equals(prodModCatagories.getString(i))) {
                        int quantity = Integer.parseInt(productCursor.getString(productCursor.getColumnIndexOrThrow(CartDb.PROD_QUANTITY)));
                        if (quantity > offerBynumber) {
                            float priceofProduct = Float.parseFloat(productCursor.getString(productCursor.getColumnIndexOrThrow(preferences.getString(AppConstants.PREFERENCES_CUSTOMER_PRICE_TYPE, ProductsDb.PROD_CFSTOCKIST_PRICE))));
                            float discountadd = discountCalculate(discount, priceofProduct);
                            totaldiscount += discountadd;
                            flag = true;
                        }
                    }
                }
            }
            if (flag) {
                if (applyStatus) return true;
                else {
                    applyDiscountToView(totaldiscount, discount);
                }

            } else {
                if (applyStatus) return false;
                Toast.makeText(getApplicationContext(), "Can't be applied", Toast.LENGTH_SHORT).show();
            }
            productCursor.close();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return false;
    }

    private boolean applyProductDiscounts(JSONObject object, String indexName, JSONArray applicableProd, boolean applyStatus) {
        try {

            int weight = Integer.parseInt(object.getString("offer_by_weight"));
            String unit = object.getString("weight_unit");
            float discount = Float.parseFloat(object.getString("offer_discount"));
            MatrixCursor productCursor = totalBillFragment.getProductCursor();
            float totaldiscount = 0;
            boolean flag = false;
            if (productCursor.moveToFirst())
                do {
                    for (int i = 0; i < applicableProd.length(); i++) {
                        boolean isapplied;
                        if (indexName.equals(ProductsDb.PROD_CATEGORY))
                            isapplied = productCursor.getString(productCursor.getColumnIndexOrThrow(indexName)).equals(applicableProd.getString(i)) || productCursor.getString(productCursor.getColumnIndexOrThrow(ProductsDb.PROD_SUB_CATEGORY)).equals(applicableProd.getString(i));
                        else
                            isapplied = productCursor.getString(productCursor.getColumnIndexOrThrow(indexName)).equals(applicableProd.getString(i));
                        if (isapplied) {
                            float weightof_product = Integer.parseInt(productCursor.getString(productCursor.getColumnIndexOrThrow(ProductsDb.PROD_WEIGHT)));
                            String weight_unit = productCursor.getString(productCursor.getColumnIndexOrThrow(ProductsDb.PROD_UNIT));
                            int quantity = Integer.parseInt(productCursor.getString(productCursor.getColumnIndexOrThrow(CartDb.PROD_QUANTITY)));
                            float unitedweight = weightEqualize(unit, weight_unit, weightof_product);
                            Log.i("united", unitedweight + "");
                            if (unitedweight * quantity > weight) {
                                float priceofProduct = Float.parseFloat(productCursor.getString(productCursor.getColumnIndexOrThrow(preferences.getString(AppConstants.PREFERENCES_CUSTOMER_PRICE_TYPE, ProductsDb.PROD_CFSTOCKIST_PRICE))));
                                float discountadd = discountCalculate(discount, (priceofProduct * quantity));
                                totaldiscount += discountadd;
                                flag = true;
                            } else
                                Toast.makeText(getApplicationContext(), "Promotion can't be applied to this price range", Toast.LENGTH_SHORT).show();
                        }
                    }
                } while (productCursor.moveToNext());
            if (flag) {
                if (applyStatus) return true;
                else {
                    applyDiscountToView(totaldiscount, discount);
                }
            } else {
                if (applyStatus) return false;
                Toast.makeText(getApplicationContext(), "Can't be applied", Toast.LENGTH_SHORT).show();
            }
            productCursor.close();

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return false;
    }


    boolean isApplicableUserType(JSONArray array) {
        try {
            for (int i = 0; i < array.length(); i++) {
                if (preferences.getString(AppConstants.PREFERENCES_USER_TYPE, "1").equals(array.getString(i)))
                    return true;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return false;
    }

    boolean isApplicableGroup(JSONArray array) {
        try {
            for (int i = 0; i < array.length(); i++) {
                if (preferences.getString(AppConstants.PREFERENCES_USER_GROUP_ID, "1").equals(array.getString(i)))
                    return true;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return false;
    }

    private float discountCalculate(float discount, float totalcash) {
        return totalcash * ( discount / 100);
    }

    private void applyDiscountToView(float totaldicount, float discountpercent) {
        promotext.setEnabled(false);
        addpromo.setEnabled(false);
        order_discount = discountpercent + "";
        Toast.makeText(getApplicationContext(), "Promotion applied!", Toast.LENGTH_SHORT).show();
        totalBillFragment.setFinalcash(totalcash - totaldicount, discountpercent);
    }

    float weightEqualize(String unit, String product_unit, float weightof_product) {
        if (unit.equals(product_unit))
            return weightof_product;
        else if (unit.equals("gm") && product_unit.equals("kg"))
            return (weightof_product * 1000);
        else if (unit.equals("kg") && product_unit.equals("gm"))
            return (weightof_product / 1000);
        return 0;
    }


    //action for order
    public void orderActions(View view) {
        switch (view.getId()) {
            case R.id.order_backtocart_btn:
                NavUtils.navigateUpFromSameTask(this);
                break;
            case R.id.order_confirm_btn:
                ContentValues orderValues = new ContentValues();
                DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                Date date = new Date();
                String datecurrent = dateFormat.format(date);
                orderValues.put(OrderDb.ORDER_DATE, datecurrent);
                orderValues.put(OrderDb.ORDER_SEESION_ID, "");
                orderValues.put(OrderDb.USER_ID, preferences.getString(AppConstants.PREFERENCES_USER_ID, "0"));
                orderValues.put(OrderDb.EXE_ID, preferences.getString(AppConstants.PREFERENCES_EXE_ID, "0"));
                String promocode = "";
                if (!addpromo.isEnabled()) {
                    Cursor promocursor = (Cursor) promotext.getSelectedItem();
                    promocursor.moveToFirst();
                    promocode = promocursor.getString(promocursor.getColumnIndexOrThrow(OfferDb.PROMO_CODE));
                    promocursor.close();

                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putString(promocode + "_" + preferences.getString(AppConstants.PREFERENCES_USER_ID, "0"), "1");
                    editor.apply();
                }
                orderValues.put(OrderDb.PROMO_CODE, promocode);
                orderValues.put(OrderDb.PROMO_DISCOUNT, order_discount);
                orderValues.put(OrderDb.PROD_SYNC_STATUS, "0");
                orderValues.put(OrderDb.ORDER_AMOUNT, totalBillFragment.getfinalCash() + "");
                Uri uri = getContentResolver().insert(LocalDbProvider.ORDER_URI, orderValues);
                String contentid = uri.getPathSegments().get(1);
                MatrixCursor productCursor = totalBillFragment.getProductCursor();
                if (productCursor.moveToFirst()) {
                    do {
                        ContentValues orderProductValues = new ContentValues();
                        orderProductValues.put(OrderProductsDb.ORDER_ID, contentid);
                        orderProductValues.put(OrderProductsDb.PROD_ID, productCursor.getString(productCursor.getColumnIndexOrThrow(ProductsDb.PROD_ID)));
                        orderProductValues.put(OrderProductsDb.PROD_QUANTITY, productCursor.getString(productCursor.getColumnIndexOrThrow(CartDb.PROD_QUANTITY)));
                        orderProductValues.put(OrderProductsDb.PROD_PRICE, productCursor.getString(productCursor.getColumnIndexOrThrow(preferences.getString(AppConstants.PREFERENCES_CUSTOMER_PRICE_TYPE, ProductsDb.PROD_CFSTOCKIST_PRICE))));
                        orderProductValues.put(OrderProductsDb.PROD_CATEGORY, productCursor.getString(productCursor.getColumnIndexOrThrow(ProductsDb.PROD_CATEGORY)));
                        orderProductValues.put(OrderProductsDb.PROD_GROUP, productCursor.getString(productCursor.getColumnIndexOrThrow(ProductsDb.PROD_GROUP)));
                        getContentResolver().insert(LocalDbProvider.ORDER_PRODUCT_URI, orderProductValues);
                    } while (productCursor.moveToNext());
                }
                finish();
                Toast.makeText(getApplicationContext(), "Order Successfully Placed!!", Toast.LENGTH_SHORT).show();

//                Cursor btncursor = (Cursor) getItem((Integer) v.getTag());
                ANDROID_UDID = Settings.Secure.getString(getContentResolver(),
                        Settings.Secure.ANDROID_ID);
                Cursor productsCursor = retrieveProductCursor(contentid);
                try {
//                            prepare JSONobject according to api format
                    JSONArray productArray = new JSONArray();
                    if (productsCursor.moveToFirst())
                        do {
                            JSONObject orderProductObject = new JSONObject();
                            orderProductObject.put(OrderProductsDb.PROD_ID, productsCursor.getString(productsCursor.getColumnIndexOrThrow(OrderProductsDb.PROD_ID)));
                            orderProductObject.put(OrderProductsDb.PROD_QUANTITY, productsCursor.getString(productsCursor.getColumnIndexOrThrow(OrderProductsDb.PROD_QUANTITY)));
                            orderProductObject.put(OrderProductsDb.PROD_PRICE, productsCursor.getString(productsCursor.getColumnIndexOrThrow(OrderProductsDb.PROD_PRICE)));
                            orderProductObject.put(OrderProductsDb.PROD_CATEGORY, productsCursor.getString(productsCursor.getColumnIndexOrThrow(OrderProductsDb.PROD_CATEGORY)));
                            orderProductObject.put(OrderProductsDb.PROD_GROUP, productsCursor.getString(productsCursor.getColumnIndexOrThrow(OrderProductsDb.PROD_GROUP)));

                            productArray.put(orderProductObject);
                        } while (productsCursor.moveToNext());
                    productsCursor.close();

                    JSONObject orderobject = new JSONObject();
                    orderobject.put("ud_id", ANDROID_UDID);
                    orderobject.put(OrderDb.USER_ID, preferences.getString(AppConstants.PREFERENCES_USER_ID, "0"));
                    orderobject.put(OrderDb.EXE_ID, preferences.getString(AppConstants.PREFERENCES_EXE_ID, "0"));
                    orderobject.put(OrderDb.PROMO_CODE, promocode);
                    orderobject.put(OrderDb.ORDER_DATE, datecurrent);
                    orderobject.put("order_details", productArray);
                    JSONObject mainProductObject = new JSONObject();
                    mainProductObject.put("order_details_main", orderobject.toString());
                    //send the order object to api
                    new SendAsynchronousOrders(contentid).execute(mainProductObject.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }


                SharedPreferences.Editor editor=preferences.edit();
                editor.putString("nonLoggedId","sessionover");
                editor.putString("cartCount","0");
                editor.putString("xshop", "false");
                editor.apply();

                //startActivity(new Intent(getApplicationContext(), RecentOrderActivity.class));
                getContentResolver().delete(LocalDbProvider.CART_URI, null, null);
                break;
        }
    }

    // send asynchronously the orders to server with api
    private class SendAsynchronousOrders extends AsyncTask<String, Void, String> {
        private ProgressDialog dialog;
        private String contentid;

        @Override
        protected void onPreExecute() {
        }


        public SendAsynchronousOrders(String contentid) {
            this.contentid = contentid;

        }

        //send order to the api
        @Override
        protected String doInBackground(String... params) {
//            Log.i("json_req",params[0]);
            return JSONParser.makeHttpRequest(AppConstants.URL_ORDER, "POST", params[0]);
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
        }


        @Override
        protected void onPostExecute(String result) {
            try {
                JSONObject object = new JSONObject(result);
                // retrieve the response and update order status
                if (object.getString("status").equals("success")) {
                    ContentValues values = new ContentValues();
                    values.put(OrderDb.ORDER_SEESION_ID, object.getString(OrderDb.ORDER_SEESION_ID));
                    values.put(OrderDb.PROD_SYNC_STATUS, "1");
                    getContentResolver().update(LocalDbProvider.ORDER_URI, values, OrderDb._ID + "=?", new String[]{contentid});
                    //Toast.makeText(getApplicationContext(), "Order Sent!", Toast.LENGTH_SHORT).show();
                    finish();
                    //after updating launch RecentOrderActivity
                    Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                    startActivity(intent);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }


    public void setTotalcash(float totalcash) {
        this.totalcash = totalcash;
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        String[] projection = {
                OfferDb._ID,
                OfferDb.OFFER_ID,
                OfferDb.OFFER_DESCRIPTION,
                OfferDb.PROMO_CODE,
                OfferDb.OFFER_TITLE,
                OfferDb.OFFER_JSON
        };
        return new CursorLoader(getApplicationContext(), LocalDbProvider.OFFER_URI, projection, null, null, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        MatrixCursor cursor = new MatrixCursor(new String[]{OfferDb._ID, OfferDb.OFFER_ID, OfferDb.OFFER_DESCRIPTION, OfferDb.PROMO_CODE, OfferDb.OFFER_TITLE, OfferDb.OFFER_JSON});
        while (data.moveToNext()) {
            String offerpromo = data.getString(data.getColumnIndexOrThrow(OfferDb.PROMO_CODE));

            String userid = preferences.getString(AppConstants.PREFERENCES_USER_ID, "0");

            boolean appliedalready = preferences.getString(offerpromo + "_" + userid, "0").equals("1");

            if (applyPromo(offerpromo, true) && !appliedalready) {
                String _id = data.getString(data.getColumnIndexOrThrow(OfferDb._ID));
                String offerid = data.getString(data.getColumnIndexOrThrow(OfferDb.OFFER_ID));
                String offerdesc = data.getString(data.getColumnIndexOrThrow(OfferDb.OFFER_DESCRIPTION));
                String promo_code = data.getString(data.getColumnIndexOrThrow(OfferDb.PROMO_CODE));
                String offertitle = data.getString(data.getColumnIndexOrThrow(OfferDb.OFFER_TITLE));
                String offer_jsondata = data.getString(data.getColumnIndexOrThrow(OfferDb.OFFER_JSON));
                cursor.addRow(new String[]{_id, offerid, offerdesc, promo_code, offertitle, offer_jsondata});
                Toast.makeText(this,"in jishads login logic",Toast.LENGTH_LONG).show();
            }
        }
        dataAdapter.swapCursor(cursor);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        dataAdapter.swapCursor(null);
    }

    public Cursor retrieveProductCursor(String orderid) {
        String[] projection = new String[]{
                OrderProductsDb._ID,
                OrderProductsDb.ORDER_ID,
                OrderProductsDb.PROD_CATEGORY,
                OrderProductsDb.PROD_GROUP,
                OrderProductsDb.PROD_ID,
                OrderProductsDb.PROD_QUANTITY,
                OrderProductsDb.PROD_PRICE
        };

        Cursor cursor = getContentResolver().query(LocalDbProvider.ORDER_PRODUCT_URI, projection, OrderProductsDb.ORDER_ID + "=?", new String[]{orderid}, null);

        return cursor;
    }
}
