package com.sha.netstager.pandafoods.activity;

import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.sha.netstager.pandafoods.R;
import com.sha.netstager.pandafoods.adapters.CatagoryCursorAdapter;
import com.sha.netstager.pandafoods.contentProvider.LocalDbProvider;
import com.sha.netstager.pandafoods.datatables.CatagoryDb;
import com.sha.netstager.pandafoods.util.AppConstants;

public class ProductCatagoryActivity extends PandaActivity implements LoaderManager.LoaderCallbacks<Cursor>{

    private ListView catListView;
    private CatagoryCursorAdapter dataAdapter;
    private TextView product_availability_view;
    private SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        actionBar.setTitle(R.string.string_products);
        setContentView(R.layout.activity_product_catagory);
        catListView = (ListView) findViewById(R.id.catagory_list_view);
        product_availability_view= (TextView) findViewById(R.id.product_availability_display);
        loadListView();
    }

    @Override
    public void onBackPressed()
    {
        Log.i("cust id& exe id", preferences.getString(AppConstants.PREFERENCES_USER_ID, "0") + preferences.getString(AppConstants.PREFERENCES_EXE_ID, "0") + "");
        Log.i("user_id",preferences.getString(AppConstants.logCheck, "0"));
        Intent homeIntent=new Intent(getApplicationContext(),HomeActivity.class);
        startActivity(homeIntent);
    }
    //LOAD listview from custom CatagoryCursorAdapter
    private void loadListView(){
        dataAdapter= new CatagoryCursorAdapter(getApplicationContext(),null,0);
        catListView.setAdapter(dataAdapter);

        //specify what action occured when click on catagory
        catListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //when clicking on an item, retrieve cursor data at that position,then obtain category id of that particular category, and pass into SubCatagoryActivity and launch it
                Cursor cursor = (Cursor) catListView.getItemAtPosition(position);
                Intent intent=new Intent(getApplicationContext(),SubCatagoryActivity.class);
                String cat_id=cursor.getString(cursor.getColumnIndexOrThrow(CatagoryDb.CAT_ID));
                //intent.putExtra(CatagoryDb.CAT_ID,cat_id);
                intent.putExtra(CatagoryDb.CAT_ID,cat_id);
                startActivity(intent);
            }
        });
        //Ensures a loader is initialized and active.
        getLoaderManager().initLoader(0, null, this);
    }


    //this loader class contact with database with the help of content provider class
    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        String[] projection = {
                CatagoryDb._ID,
                CatagoryDb.CAT_ID,
                CatagoryDb.CAT_NAME,
                CatagoryDb.CAT_IMAGE,
                CatagoryDb.CAT_DESCRIPTION
        };
        return new CursorLoader(this,
                LocalDbProvider.CATAGORY_URI, projection, CatagoryDb.PARENT_CAT+"=?", new String[]{"0"}, null);
    }

    //on laod finished swap cursor data of CatagoryCursorAdapter instance
    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        dataAdapter.swapCursor(data);
        if (!data.moveToFirst()) {
            product_availability_view.setVisibility(View.VISIBLE);
//            product_list_view.setVisibility(View.GONE);
        }
    }
    // if loader reset, swap adapter cursor into null
    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        dataAdapter.swapCursor(null);
    }



}
