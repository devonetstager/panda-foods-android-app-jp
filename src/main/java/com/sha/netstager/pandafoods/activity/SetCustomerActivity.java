package com.sha.netstager.pandafoods.activity;

import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import com.sha.netstager.pandafoods.R;
import com.sha.netstager.pandafoods.adapters.CustomerFilterAdapter;
import com.sha.netstager.pandafoods.contentProvider.LocalDbProvider;
import com.sha.netstager.pandafoods.datatables.CustomerDb;
import com.sha.netstager.pandafoods.datatables.LocationDb;
import com.sha.netstager.pandafoods.model.CustomerModel;
import com.sha.netstager.pandafoods.util.AppConstants;

import java.util.ArrayList;
import java.util.List;

public class SetCustomerActivity extends PandaActivity {

    private TextView custName,custId;
    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayShowCustomEnabled(true);
        setContentView(R.layout.activity_set_customer);
        sharedPreferences=getSharedPreferences(AppConstants.PANDA_PREFERENCES,MODE_PRIVATE);

        //projection data from customer table
        String[] projection=new String[]{
          CustomerDb._ID,
          CustomerDb.CUST_ID,
          CustomerDb.CUST_NAME,
          CustomerDb.CUST_EMAIL,
          CustomerDb.CUST_PHONE,
          CustomerDb.CUST_ADDRESS,
          CustomerDb.LOC_ID,
          CustomerDb.CUST_TYPE_ID
        };
        //retrieve all customers list from customer table to cursor1
        Cursor cursor=getContentResolver().query(LocalDbProvider.CUSOTMER_URI,projection,null,null,null);
        List<CustomerModel> customerModels=new ArrayList<>();
        while (cursor.moveToNext()){
            //add each customer data to customer arraylist
            String cust_id=cursor.getString(cursor.getColumnIndexOrThrow(CustomerDb.CUST_ID));
            String cust_name =cursor.getString(cursor.getColumnIndexOrThrow(CustomerDb.CUST_NAME));
            String address=cursor.getString(cursor.getColumnIndexOrThrow(CustomerDb.CUST_ADDRESS));
            String email=cursor.getString(cursor.getColumnIndexOrThrow(CustomerDb.CUST_EMAIL));
            String location=cursor.getString(cursor.getColumnIndexOrThrow(CustomerDb.LOC_ID));
            String phoneno=cursor.getString(cursor.getColumnIndexOrThrow(CustomerDb.CUST_PHONE));
            String customer_type=cursor.getString(cursor.getColumnIndexOrThrow(CustomerDb.CUST_TYPE_ID));
            CustomerModel customerModel=new CustomerModel(cust_id,cust_name,address,email,location,phoneno,customer_type);
            customerModels.add(customerModel);
        }
        final CustomerFilterAdapter dataAdapter= new CustomerFilterAdapter(this,customerModels);
        //instantiate views
        ListView listView= (ListView) findViewById(R.id.customer_list_view);
        TextView inputSearch= (TextView) findViewById(R.id.filterTextbox);
        custName = (TextView) findViewById(R.id.selectedCustName);
        custId = (TextView) findViewById(R.id.selectedCustId);
        //refresh customers list
        refreshCustomer();

        listView.setAdapter(dataAdapter);

        //search customers from the list, by input search to inputSearch field
        inputSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                // When user changed the Text
                dataAdapter.getFilter().filter(cs);
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
            }
        });

        //set onclick listener for the listview
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        //set selected customer into preferences
                        CustomerModel m=dataAdapter.getItem(position);
                        SharedPreferences.Editor editor=sharedPreferences.edit();
                        editor.putString(AppConstants.PREFERENCES_USER_ID,m.getUser_id());
                        editor.putString(AppConstants.PREFERENCES_USER_NAME,m.getUser_name());
                        editor.putString(AppConstants.PREFERENCES_USER_PHONE,m.getPhoneno());
                        editor.putString(AppConstants.PREFERENCES_USER_LOCATION,m.getLocation());
                        editor.putString(AppConstants.PREFERENCES_USER_ADDRESS,m.getAddress());
                        editor.apply();
                        dataAdapter.notifyDataSetChanged();
                       // getContentResolver().delete(LocalDbProvider.CART_URI,null,null);
                        refreshCustomer();
            }
        });
    }

    //method refresh customers list
    public void refreshCustomer(){
        String custname=sharedPreferences.getString(AppConstants.PREFERENCES_USER_NAME, "");
        String location=sharedPreferences.getString(AppConstants.PREFERENCES_USER_LOCATION,"");
        Cursor cursor= getContentResolver().query(LocalDbProvider.LOC_URI,new String[]{LocationDb._ID,LocationDb.LOC_ID,LocationDb.LOCATION} ,LocationDb.LOC_ID+"=?",new String[]{location},null);
        String locationname="";
        if (cursor.moveToFirst())
            locationname=cursor.getString(cursor.getColumnIndexOrThrow(LocationDb.LOCATION));
        custName.setText(custname+"  "+locationname);
        cursor.close();
        custId.setText(sharedPreferences.getString(AppConstants.PREFERENCES_USER_ID,""));
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

}
