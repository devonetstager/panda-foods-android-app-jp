package com.sha.netstager.pandafoods.activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.sha.netstager.pandafoods.R;

public class  ConfirmSignUpActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm_sign_up);
        TextView title = (TextView) findViewById(R.id.confirm_signup_title);
        TextView content = (TextView) findViewById(R.id.confirm_signup_content);
        Bundle bundle=getIntent().getExtras();
        int status= bundle.getInt("confirm_type");
        switch (status){
            case 0:
                title.setText("Already Registered");
                content.setText("This user already exist");
                break;
            case 1:
                title.setText("Already Registered");
                content.setText("User with this email already registered and waiting for admin's approval");
                break;
            case 2:
                title.setText("Thank you for registering with us");
                content.setText("administrator will review your application and you will get a confirmation message shortly");
                break;
            case 3:
                title.setText("User already Exist");
                content.setText("This user already exist");
                break;
            default:
                break;
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    public void backtoStart(View view) {
        finish();
    }
}
