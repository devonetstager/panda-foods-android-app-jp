package com.sha.netstager.pandafoods.activity;

import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.sha.netstager.pandafoods.R;
import com.sha.netstager.pandafoods.adapters.SettingsAdapter;
import com.sha.netstager.pandafoods.contentProvider.LocalDbProvider;
import com.sha.netstager.pandafoods.model.SettingsModel;
import com.sha.netstager.pandafoods.services.OrderService;
import com.sha.netstager.pandafoods.services.SyncCustomerService;
import com.sha.netstager.pandafoods.services.SyncService;
import com.sha.netstager.pandafoods.util.AppConstants;

public class SettingsActivity extends PandaActivity {
    private SharedPreferences preferences;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        actionBar.setTitle(R.string.string_settings);
        preferences= getSharedPreferences(AppConstants.PANDA_PREFERENCES,MODE_PRIVATE);
        ListView settingsListView = (ListView) findViewById(R.id.settings_menu_list);
        //instantiate settings adapter from
        final SettingsAdapter settingsAdapter = addCustomerOptions();

        //set onclick listener for settings menu
        settingsListView.setAdapter(settingsAdapter);
        settingsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //check network is connected or not
                ConnectivityManager cm =
                        (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

                NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

                boolean isConnected = activeNetwork != null &&
                        activeNetwork.isConnectedOrConnecting();
                switch ((int) settingsAdapter.getItemId(position)) {
                    case 1:
                        //sync product service
                        if (isConnected) {
                            if (isMyServiceRunning(OrderService.class) || isMyServiceRunning(SyncCustomerService.class) || isMyServiceRunning(SyncService.class)) {
                                Toast.makeText(getApplicationContext(), "Sync already in progress. Please wait..", Toast.LENGTH_SHORT).show();
                            } else {
                                Intent syncIntent = new Intent(SettingsActivity.this, SyncService.class);
                                startService(syncIntent);
                            }
                        }
                        break;
                    case 2:
                        //sync order service
                        if (isConnected) {
//                            if (isMyServiceRunning(OrderService.class) || isMyServiceRunning(SyncCustomerService.class) || isMyServiceRunning(SyncService.class)) {
//                                Toast.makeText(getApplicationContext(), "Sync already in progress. Please wait..", Toast.LENGTH_SHORT).show();
//                            } else {
                                Intent syncIntent = new Intent(SettingsActivity.this, OrderService.class);
                                syncIntent.putExtra("OrderID", "All");
                                startService(syncIntent);
//                            }
                        }
                        else {
                            Toast.makeText(getApplicationContext(),"Connection Error!",Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case 3:
                        //sync customer service
                        if (isConnected) {
//                            if (isMyServiceRunning(OrderService.class) || isMyServiceRunning(SyncCustomerService.class) || isMyServiceRunning(SyncService.class)) {
//                                Toast.makeText(getApplicationContext(), "Sync already in progress. Please wait..", Toast.LENGTH_SHORT).show();
//                            } else {
                                Intent syncIntent = new Intent(SettingsActivity.this, SyncCustomerService.class);
                                startService(syncIntent);
//                            }
                        }
                        else {
                            Toast.makeText(getApplicationContext(),"Connection Error!",Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case 4:
                        //sync all services
                        if (isConnected) {
//                            if (isMyServiceRunning(OrderService.class) || isMyServiceRunning(SyncCustomerService.class) || isMyServiceRunning(SyncService.class)) {
//                                Toast.makeText(getApplicationContext(), "Sync already in progress. Please wait..", Toast.LENGTH_SHORT).show();
//                            } else {
                                Log.i("syncing", "yes");
                                if (!preferences.getString(AppConstants.PREFERENCES_EXE_ID, "0").equals("0")) {
                                    Intent syncIntent = new Intent(SettingsActivity.this, SyncCustomerService.class);
                                    startService(syncIntent);
                                }
                                Intent syncOrderIntent = new Intent(SettingsActivity.this, OrderService.class);
                                    syncOrderIntent.putExtra("OrderID", "All");
                                startService(syncOrderIntent);
                                Intent syncserviceIntent = new Intent(SettingsActivity.this, SyncService.class);
                                startService(syncserviceIntent);
//                            }
                        }
                        else {
                            Toast.makeText(getApplicationContext(),"Connection Error!",Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case 5:
                        //show dialogue for conforming log out
                        showDialog();
                        break;
                }
            }
        });
    }

    //method for display logout which confirming log out
    private void showDialog() {
        final AlertDialog.Builder alert = new AlertDialog.Builder(SettingsActivity.this);
        alert.setTitle("Confirm Logout"); //Set Alert dialog title here
        alert.setMessage("Do you really want to log out? you will lose all your data, including Product List and Orders"); //Message here


        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
//                if user confirm to logout clear all databases and stored id of user/customer
                SharedPreferences.Editor editor= preferences.edit();
                editor.putString(AppConstants.PREFERENCES_EXE_ID,"0");
                editor.putString(AppConstants.PREFERENCES_USER_ID,"0");
                editor.apply();
                getContentResolver().delete(LocalDbProvider.CART_URI, null, null);
//                getContentResolver().delete(LocalDbProvider.CONTENT_URI, null, null);
//                getContentResolver().delete(LocalDbProvider.CATAGORY_URI,null,null);
                getContentResolver().delete(LocalDbProvider.ORDER_URI,null,null);
                getContentResolver().delete(LocalDbProvider.ORDER_PRODUCT_URI,null,null);
//                getContentResolver().delete(LocalDbProvider.WEIGHT_URI,null,null);
//                getContentResolver().delete(LocalDbProvider.COUNTRY_URI,null,null);
//                getContentResolver().delete(LocalDbProvider.STATE_URI,null,null);
//                getContentResolver().delete(LocalDbProvider.DISTRICT_URI,null,null);
//                getContentResolver().delete(LocalDbProvider.LOC_URI,null,null);
//                getContentResolver().delete(LocalDbProvider.CUSOTMER_URI,null,null);
//                getContentResolver().delete(LocalDbProvider.OFFER_URI,null,null);
                Intent intent= new Intent(getApplicationContext(),StartActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();

                dialog.dismiss();
            } // End of onClick(DialogInterface dialog, int whichButton)
        }); //End of alert.setPositiveButton
        alert.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.dismiss();
            }
        }); //End of alert.setNegativeButton
        AlertDialog alertDialog = alert.create();
        alertDialog.show();
    }

    //method for instantiate SettingsAdapter and specify list of options
    private SettingsAdapter addCustomerOptions() {
        SettingsAdapter settingsAdapter = new SettingsAdapter(getApplicationContext());
        SettingsModel syncProducts = new SettingsModel( R.drawable.sync_products, "Sync Products",1);
        SettingsModel syncOrders = new SettingsModel(R.drawable.sync_orders, "Sync Orders",2);
        SettingsModel syncAll = new SettingsModel(R.drawable.sync_all, "Sync All",4);
        SettingsModel logout = new SettingsModel(R.drawable.logout, "Logout",5);
        settingsAdapter.addModels(syncProducts);
        settingsAdapter.addModels(syncOrders);
//        if executive is logged in, then only sync customer will show
        if(preferences.getInt(AppConstants.PREFERENCES_USER_MODE,1)==2){
            SettingsModel syncCustomers = new SettingsModel( R.drawable.sync_customers, "Sync Customers",3);
            settingsAdapter.addModels(syncCustomers);
        }
        settingsAdapter.addModels(syncAll);
        settingsAdapter.addModels(logout);
        return settingsAdapter;
    }
//    method for checking a particular service is running
    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getApplicationContext().getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }
}

