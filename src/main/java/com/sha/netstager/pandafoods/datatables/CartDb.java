package com.sha.netstager.pandafoods.datatables;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

/**
 * Created by netstager on 31/12/14.
 */
public class CartDb {
    public static final String _ID="_id";
    public static final String USER_ID="user_id";
    public static final String PROD_ID = "prod_id";
    public static final String PROD_QUANTITY = "prod_quantity";
    private static final String LOG_TAG = "CartDb";
    public static final String SQLITE_TABLE = "Cart";

    private static final String DATABASE_CREATE =
            "CREATE TABLE if not exists " + SQLITE_TABLE + " (" +
                    _ID+" integer PRIMARY KEY autoincrement,"+
                    USER_ID +","+
                    PROD_ID +","+
                    PROD_QUANTITY + ");";

    public static void onCreate(SQLiteDatabase db) {
        Log.w(LOG_TAG, DATABASE_CREATE);
        db.execSQL(DATABASE_CREATE);
    }

    public static void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(LOG_TAG, "Upgrading database from version " + oldVersion + " to "
                + newVersion + ", which will destroy all old data");
        db.execSQL("DROP TABLE IF EXISTS " + SQLITE_TABLE);
        onCreate(db);
    }
}
