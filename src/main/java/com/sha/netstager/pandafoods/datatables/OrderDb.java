package com.sha.netstager.pandafoods.datatables;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

/**
 * Created by netstager on 20/12/14.
 */
public class OrderDb {
    public static final String _ID = "_id";
    public static final String ORDER_SEESION_ID = "order_seesion_id";
    public static final String USER_ID = "user_id";
    public static final String EXE_ID = "exe_id";
    public static final String ORDER_DATE = "order_date";
    public static final String PROD_SYNC_STATUS = "sync_status";
    public static final String ORDER_AMOUNT="order_amount";
    public static final String PROMO_CODE="promo_code";
    public static final String PROMO_DISCOUNT="promo_discount";


    private static final String LOG_TAG = "OrderDb";
    public static final String SQLITE_TABLE = "Orders";

    private static final String DATABASE_CREATE =
            "CREATE TABLE if not exists " + SQLITE_TABLE + " (" +
                    _ID + " integer PRIMARY KEY autoincrement," +
                    ORDER_SEESION_ID +","+
                    USER_ID +","+
                    EXE_ID + "," +
                    PROD_SYNC_STATUS +"," +
                    ORDER_AMOUNT +"," +
                    PROMO_CODE +"," +
                    PROMO_DISCOUNT +"," +
                    ORDER_DATE +
                    ");";

    public static void onCreate(SQLiteDatabase db) {
        Log.w(LOG_TAG, DATABASE_CREATE);
        db.execSQL(DATABASE_CREATE);
    }

    public static void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(LOG_TAG, "Upgrading database from version " + oldVersion + " to "
                + newVersion + ", which will destroy all old data");
        db.execSQL("DROP TABLE IF EXISTS " + SQLITE_TABLE);
        onCreate(db);
    }
}
