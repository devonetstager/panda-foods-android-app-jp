package com.sha.netstager.pandafoods.datatables;

/**
 * Created by netstager on 24/12/14.
 */

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class CatagoryDb {

    public static final String _ID="_id";
    public static final String CAT_ID = "cat_id";
    public static final String CAT_NAME = "cat_name";
    public static final String CAT_DESCRIPTION = "cat_description";
    public static final String PARENT_CAT = "parent_cat";
    public static final String CAT_IMAGE = "cat_image";
    private static final String LOG_TAG = "CatagoryDb";
    public static final String SQLITE_TABLE = "Catagory";

    private static final String DATABASE_CREATE =
            "CREATE TABLE if not exists " + SQLITE_TABLE + " (" +
                    _ID+" integer PRIMARY KEY autoincrement,"+
                    CAT_ID +","+
                    CAT_NAME + "," +
                    CAT_DESCRIPTION + "," +
                    PARENT_CAT + "," +
                    CAT_IMAGE + "," +
                    " UNIQUE (" + CAT_ID +"));";

    public static void onCreate(SQLiteDatabase db) {
        Log.w(LOG_TAG, DATABASE_CREATE);
        db.execSQL(DATABASE_CREATE);
    }
    public static void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(LOG_TAG, "Upgrading database from version " + oldVersion + " to "
                + newVersion + ", which will destroy all old data");
        db.execSQL("DROP TABLE IF EXISTS " + SQLITE_TABLE);
        onCreate(db);
    }

}
