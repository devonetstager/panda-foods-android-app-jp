package com.sha.netstager.pandafoods.datatables;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

/**
 * Created by netstager on 17/12/14.
 */
public class CustomerDb {
        public static final String _ID="_id";
        public static final String CUST_ID = "cust_id";
        public static final String CUST_NAME = "cust_name";
        public static final String LOC_ID = "loc_id";
        public static final String CUST_PHONE = "cust_phone";
        public static final String CUST_EMAIL = "cust_email";
        public static final String CUST_ADDRESS = "cust_address";
        public static final String USER_MODE ="user_mode";
        public static final String CUST_TYPE_ID ="cust_type_id";
        public static final String CUST_GROUP_ID ="cust_group_id";
        private static final String LOG_TAG = "CustomerDb";
        public static final String SQLITE_TABLE = "Customers";

        private static final String DATABASE_CREATE =
                "CREATE TABLE if not exists " + SQLITE_TABLE + " (" +
                        _ID+" integer PRIMARY KEY autoincrement,"+
                        CUST_ID +","+
                        CUST_NAME + "," +
                        LOC_ID + "," +
                        CUST_PHONE + "," +
                        CUST_EMAIL + "," +
                        CUST_ADDRESS + "," +
                        USER_MODE + "," +
                        CUST_TYPE_ID + "," +
                        CUST_GROUP_ID+","+
                        " UNIQUE (" + CUST_ID +"));";

        public static void onCreate(SQLiteDatabase db) {
            Log.w(LOG_TAG, DATABASE_CREATE);
            db.execSQL(DATABASE_CREATE);
        }

        public static void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            Log.w(LOG_TAG, "Upgrading database from version " + oldVersion + " to "
                    + newVersion + ", which will destroy all old data");
            db.execSQL("DROP TABLE IF EXISTS " + SQLITE_TABLE);
            onCreate(db);
        }

}
