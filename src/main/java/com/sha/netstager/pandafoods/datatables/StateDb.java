package com.sha.netstager.pandafoods.datatables;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

/**
 * Created by netstager on 31/12/14.
 */
public class StateDb {
    public static final String _ID="_id";
    public static final String STATE_ID = "state_id";
    public static final String STATE = "state";
    public static final String COUNTRY_ID = "country_id";

    private static final String LOG_TAG = "StateDb";
    public static final String SQLITE_TABLE = "State";

    private static final String DATABASE_CREATE =
            "CREATE TABLE if not exists " + SQLITE_TABLE + " (" +
                    _ID+" integer PRIMARY KEY autoincrement,"+
                    STATE_ID +","+
                    STATE + "," +
                    COUNTRY_ID + "," +
                    " UNIQUE (" + STATE_ID +"));";

    public static void onCreate(SQLiteDatabase db) {
        Log.w(LOG_TAG, DATABASE_CREATE);
        db.execSQL(DATABASE_CREATE);
    }

    public static void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(LOG_TAG, "Upgrading database from version " + oldVersion + " to "
                + newVersion + ", which will destroy all old data");
        db.execSQL("DROP TABLE IF EXISTS " + SQLITE_TABLE);
        onCreate(db);
    }
}
