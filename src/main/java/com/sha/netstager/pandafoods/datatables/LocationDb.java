package com.sha.netstager.pandafoods.datatables;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

/**
 * Created by netstager on 31/12/14.
 */
public class LocationDb {
    public static final String _ID="_id";
    public static final String LOC_ID = "loc_id";
    public static final String LOCATION = "location";
    public static final String DISTRICT_ID = "district_id";
    public static final String STATE_ID = "state_id";
    public static final String COUNTRY_ID = "country_id";

    private static final String LOG_TAG = "locationDb";
    public static final String SQLITE_TABLE = "Location";

    private static final String DATABASE_CREATE =
            "CREATE TABLE if not exists " + SQLITE_TABLE + " (" +
                    _ID+" integer PRIMARY KEY autoincrement,"+
                    LOC_ID +","+
                    LOCATION + "," +
                    DISTRICT_ID + "," +
                    STATE_ID + "," +
                    COUNTRY_ID + "," +
                    " UNIQUE (" + LOC_ID +"));";

    public static void onCreate(SQLiteDatabase db) {
        Log.w(LOG_TAG, DATABASE_CREATE);
        db.execSQL(DATABASE_CREATE);
    }

    public static void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(LOG_TAG, "Upgrading database from version " + oldVersion + " to "
                + newVersion + ", which will destroy all old data");
        db.execSQL("DROP TABLE IF EXISTS " + SQLITE_TABLE);
        onCreate(db);
    }
}
