package com.sha.netstager.pandafoods.datatables;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

/**
 * Created by netstager on 31/12/14.
 */
public class WeightDb {
    public static final String _ID="_id";
    public static final String WEIGHT_ID = "weight_id";
    public static final String PROD_WEIGHT = "weight";
    public static final String PROD_UNIT = "unit";

    private static final String LOG_TAG = "WeightDB";
    public static final String SQLITE_TABLE = "Weight";

    private static final String DATABASE_CREATE =
            "CREATE TABLE if not exists " + SQLITE_TABLE + " (" +
                    _ID+" integer PRIMARY KEY autoincrement,"+
                    WEIGHT_ID +","+
                    PROD_WEIGHT +","+
                    PROD_UNIT + "," +
                    " UNIQUE (" + WEIGHT_ID +"));";

    public static void onCreate(SQLiteDatabase db) {
        Log.w(LOG_TAG, DATABASE_CREATE);
        db.execSQL(DATABASE_CREATE);
    }

    public static void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(LOG_TAG, "Upgrading database from version " + oldVersion + " to "
                + newVersion + ", which will destroy all old data");
        db.execSQL("DROP TABLE IF EXISTS " + SQLITE_TABLE);
        onCreate(db);
    }
}
