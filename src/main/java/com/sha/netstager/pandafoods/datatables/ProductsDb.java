package com.sha.netstager.pandafoods.datatables;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

/**
 * Created by netstager on 17/12/14.
 */
public class ProductsDb {
        public static final String _ID="_id";
        public static final String PROD_ID = "prod_id";
        public static final String PROD_NAME = "prod_name";
        public static final String PROD_DESCRIPTION = "prod_description";
        public static final String PROD_CATEGORY = "prod_category";
        public static final String PROD_SUB_CATEGORY = "prod_sub_category";
        public static final String PROD_GROUP = "prod_group";
        public static final String PROD_WEIGHT_ID = "prod_weight";
        public static final String PROD_WEIGHT = "weight";
        public static final String PROD_UNIT = "unit";
        public static final String PACKAGE = "package";
        public static final String PROD_CFSTOCKIST_PRICE ="prod_cfstockist_price";
        public static final String PROD_DEALER_PRICE ="prod_dealer_price";
        public static final String PROD_DISTRIBUTOR_PRICE ="prod_distributor_price";
        public static final String PROD_CONSUMERS_PRICE ="prod_consumers_price";
        public static final String PROD_IMAGE ="prod_image";
        public static final String PROD_IMAGE_THUMB ="prod_image_thumb";
        public static final String PROD_STATUS ="prod_status";
        private static final String LOG_TAG = "ProductDb";
        public static final String SQLITE_TABLE = "Products";

        private static final String DATABASE_CREATE =
                "CREATE TABLE if not exists " + SQLITE_TABLE + " (" +
                        _ID+" integer PRIMARY KEY autoincrement,"+
                        PROD_ID +","+
                        PROD_NAME + "," +
                        PROD_DESCRIPTION + "," +
                        PROD_CATEGORY + "," +
                        PROD_SUB_CATEGORY + "," +
                        PROD_GROUP + "," +
                        PROD_WEIGHT_ID + "," +
                        PROD_WEIGHT + "," +
                        PROD_UNIT + "," +
                        PACKAGE + "," +
                        PROD_CFSTOCKIST_PRICE + "," +
                        PROD_DEALER_PRICE + "," +
                        PROD_DISTRIBUTOR_PRICE + "," +
                        PROD_CONSUMERS_PRICE+","+
                        PROD_IMAGE +","+
                        PROD_IMAGE_THUMB +","+
                        PROD_STATUS +","+
                        " UNIQUE (" + PROD_ID +"));";

        public static void onCreate(SQLiteDatabase db) {
            Log.w(LOG_TAG, DATABASE_CREATE);
            db.execSQL(DATABASE_CREATE);
        }

        public static void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            Log.w(LOG_TAG, "Upgrading database from version " + oldVersion + " to "
                    + newVersion + ", which will destroy all old data");
            db.execSQL("DROP TABLE IF EXISTS " + SQLITE_TABLE);
            onCreate(db);
        }

}
