package com.sha.netstager.pandafoods.datatables;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

/**
 * Created by netstager on 20/12/14.
 */
public class OfferDb {
    public static final String _ID = "_id";
    public static final String OFFER_ID = "offer_id";
    public static final String OFFER_TITLE = "offer_title";
    public static final String OFFER_DESCRIPTION = "offer_description";
    public static final String OFFER_JSON = "offer_json";
    public static final String OFFER_TYPE = "offer_type";
    public static final String OFFER_FROM_DATE = "offer_from_date";
    public static final String OFFER_TO_DATE ="offer_to_date";
    public static final String PROMO_CODE ="promo_code";
    public static final String OFFER_STATUS ="offer_status";

    private static final String LOG_TAG = "OfferDb";
    public static final String SQLITE_TABLE = "Offers";

    private static final String DATABASE_CREATE =
            "CREATE TABLE if not exists " + SQLITE_TABLE + " (" +
                    _ID + " integer PRIMARY KEY autoincrement," +
                    OFFER_ID +","+
                    OFFER_TITLE +","+
                    OFFER_DESCRIPTION + "," +
                    OFFER_FROM_DATE +"," +
                    OFFER_TO_DATE +"," +
                    OFFER_JSON + ","+
                    OFFER_TYPE + ","+
                    PROMO_CODE + ","+
                    OFFER_STATUS + ","+
                    " UNIQUE (" + OFFER_ID +"));";

    public static void onCreate(SQLiteDatabase db) {
        Log.w(LOG_TAG, DATABASE_CREATE);
        db.execSQL(DATABASE_CREATE);
    }

    public static void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(LOG_TAG, "Upgrading database from version " + oldVersion + " to "
                + newVersion + ", which will destroy all old data");
        db.execSQL("DROP TABLE IF EXISTS " + SQLITE_TABLE);
        onCreate(db);
    }
}
