package com.sha.netstager.pandafoods.adapters;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import com.sha.netstager.pandafoods.R;
import com.sha.netstager.pandafoods.datatables.OfferDb;
import com.sha.netstager.pandafoods.util.AppConstants;

/**
 * Created by netstager on 13/01/15.
 */
public class OfferCursorAdapter extends CursorAdapter {

    private LayoutInflater mInflater;
    public OfferCursorAdapter(Context context, Cursor c, int flags) {
        super(context, c, flags);
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public void bindView(View view, Context context, final Cursor cursor) {

      //  TextView prod_id = (TextView) view.findViewById(R.id.product_id);
        TextView offerDesc = (TextView) view.findViewById(R.id.offer_description);
        TextView offerTitle = (TextView) view.findViewById(R.id.offer_title);
        TextView promoCode= (TextView) view.findViewById(R.id.promo_code);
        offerDesc.setText(cursor.getString(cursor.getColumnIndexOrThrow(OfferDb.OFFER_DESCRIPTION)));
        promoCode.setText(cursor.getString(cursor.getColumnIndexOrThrow(OfferDb.PROMO_CODE)));
        offerTitle.setText(cursor.getString(cursor.getColumnIndexOrThrow(OfferDb.OFFER_TITLE)));

    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        return super.getView(position, convertView, parent);
//
    }

    @Override
    public Cursor swapCursor(Cursor newCursor) {
        return super.swapCursor(newCursor);
    }


    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return mInflater.inflate(R.layout.view_offer_list_item, parent, false);
    }



}
