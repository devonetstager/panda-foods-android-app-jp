package com.sha.netstager.pandafoods.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.sha.netstager.pandafoods.R;
import com.sha.netstager.pandafoods.model.HomeMainModel;
import com.sha.netstager.pandafoods.model.SettingsModel;

import java.util.ArrayList;

/**
 * Created by netstager on 12/01/15.
 */
public class SettingsAdapter extends BaseAdapter {
    private ArrayList<SettingsModel> optLists=new ArrayList<>();
    private Context context;
    public SettingsAdapter(Context context){
        this.context=context;
    }
    @Override
    public int getCount() {
        return optLists.size();
    }

    @Override
    public String getItem(int position) {
        return optLists.get(position).getLabel();
    }

    @Override
    public long getItemId(int position) {
        return optLists.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(
                    R.layout.view_settings_list_item, null);
        }
        ImageView labelImage= (ImageView) convertView.findViewById(R.id.settings_item_icon);
        TextView labelText= (TextView) convertView.findViewById(R.id.settings_item_label);
        SettingsModel mainModel=optLists.get(position);
        labelImage.setImageResource(mainModel.getResourceId());
        labelText.setText(mainModel.getLabel());
        return convertView;
    }
    public void addModels(SettingsModel mainModel){
        optLists.add(mainModel);
    }
}
