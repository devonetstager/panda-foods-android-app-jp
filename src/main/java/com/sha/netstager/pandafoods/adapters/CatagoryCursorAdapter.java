package com.sha.netstager.pandafoods.adapters;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.sha.netstager.pandafoods.R;
import com.sha.netstager.pandafoods.datatables.CatagoryDb;
import com.squareup.picasso.Picasso;

/**
 * Created by netstager on 13/01/15.
 */
//adapter class for displaying catagory details
public class CatagoryCursorAdapter extends CursorAdapter {

        private LayoutInflater mInflater;

        //public constructer
        public CatagoryCursorAdapter(Context context, Cursor c, int flags) {
            super(context, c, flags);
            mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }


    //bind the view parameters
    @Override
        public void bindView(View view, Context context, Cursor cursor) {
            //initializing variables in the view

            TextView label = (TextView) view.findViewById(R.id.catagory_list_item_label);
//            ImageView image= (ImageView) view.findViewById(R.id.catagory_list_item_image);
            ImageView thumImage= (ImageView) view.findViewById(R.id.thumbCat);
            label.setText(cursor.getString(cursor.getColumnIndex(CatagoryDb.CAT_NAME)));

            String url=cursor.getString(cursor.getColumnIndexOrThrow(CatagoryDb.CAT_IMAGE));
//            String nameDta=cursor.getString(cursor.getColumnIndexOrThrow(CatagoryDb.CAT_NAME));
            String Img_Url= "http://orders.pandafoods.co.in/"+url;

             Picasso.with(context).load(Img_Url).into(thumImage);
        }

        @Override
        public View newView(Context context, Cursor cursor, ViewGroup parent) {
            //return mInflater.inflate(R.layout.view_product_list_item, parent, false);
            return mInflater.inflate(R.layout.view_grid_product_list_item, parent, false);
        }

//    private class ImageDownloader extends AsyncTask<String, Void, Bitmap> {
//        ImageView bmImage;
//
//        public ImageDownloader(ImageView bmImage) {
//            this.bmImage = bmImage;
//        }
//
//        protected Bitmap doInBackground(String... urls) {
//            String url = urls[0];
//            Bitmap mIcon = null;
//            try {
//                InputStream in = new java.net.URL(url).openStream();
//                mIcon = BitmapFactory.decodeStream(in);
//            } catch (Exception e) {
//                Log.e("Error", e.getMessage());
//            }
//            return mIcon;
//        }
//
//        protected void onPostExecute(Bitmap result) {
//            bmImage.setImageBitmap(result);
//        }
//    }

}
