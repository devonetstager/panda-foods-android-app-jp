package com.sha.netstager.pandafoods.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.sha.netstager.pandafoods.R;
import com.sha.netstager.pandafoods.model.CustomerModel;
import com.sha.netstager.pandafoods.util.AppConstants;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by netstager on 22/01/15.
 */
public class CustomerFilterAdapter extends ArrayAdapter<CustomerModel> implements Filterable {
    private List<CustomerModel> allModelItemsArray;
    private List<CustomerModel> filteredModelItemsArray;
    private Activity context;
    private ModelFilter filter;
    private LayoutInflater inflator;
    private SharedPreferences sharedPreferences;

    public CustomerFilterAdapter(Activity context, List<CustomerModel> list) {
        super(context, R.layout.view_customer_list_item, list);
        this.context = context;
        this.sharedPreferences=context.getSharedPreferences(AppConstants.PANDA_PREFERENCES, Context.MODE_PRIVATE);
        this.allModelItemsArray = new ArrayList<CustomerModel>();
        allModelItemsArray.addAll(list);
        this.filteredModelItemsArray = new ArrayList<>();
        filteredModelItemsArray.addAll(allModelItemsArray);
        inflator = context.getLayoutInflater();
        getFilter();
    }
    @Override
    public Filter getFilter() {
        if (filter == null){
            filter  = new ModelFilter();
        }
        return filter;
    }

    static class ViewHolder {
        protected TextView custName;
        protected TextView custId;
        protected ImageView checkSet;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = null;

        final CustomerModel m = filteredModelItemsArray.get(position);
        ViewHolder viewHolder = null;
        if (convertView == null) {

            view = inflator.inflate(R.layout.view_customer_list_item, null);
            viewHolder = new ViewHolder();
            viewHolder.custName = (TextView) view.findViewById(R.id.customer_name);
            viewHolder.custId = (TextView) view.findViewById(R.id.customer_id);

            view.setTag(viewHolder);
        } else {
            view = convertView;
            viewHolder = ((ViewHolder) view.getTag());
        }
        viewHolder.custName.setText(m.getUser_name());
        viewHolder.custId.setText(m.getUser_id());

        final ViewHolder finalViewHolder = viewHolder;
        finalViewHolder.checkSet = (ImageView) view.findViewById(R.id.setasCustomer);
        if (m.getUser_id().equals(sharedPreferences.getString(AppConstants.PREFERENCES_USER_ID,"0"))){
            finalViewHolder.checkSet.setVisibility(View.VISIBLE);
        }
        else {
            finalViewHolder.checkSet.setVisibility(View.INVISIBLE);
        }
        return view;
    }

    private class ModelFilter extends Filter
    {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            constraint = constraint.toString().toLowerCase();
            FilterResults result = new FilterResults();
            if(constraint != null && constraint.toString().length() > 0)
            {
                ArrayList<CustomerModel> filteredItems = new ArrayList<CustomerModel>();

                for(int i = 0, l = allModelItemsArray.size(); i < l; i++)
                {
                    CustomerModel m = allModelItemsArray.get(i);
                    if(m.getUser_name().toLowerCase().contains(constraint))
                        filteredItems.add(m);
                }
                result.count = filteredItems.size();
                result.values = filteredItems;
            }
            else
            {
                synchronized(this)
                {
                    result.values = allModelItemsArray;
                    result.count = allModelItemsArray.size();
                }
            }
            return result;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {

            filteredModelItemsArray = (ArrayList<CustomerModel>)results.values;
            notifyDataSetChanged();
            clear();
            for(int i = 0, l = filteredModelItemsArray.size(); i < l; i++)
                add(filteredModelItemsArray.get(i));
            notifyDataSetInvalidated();
        }
    }
}
