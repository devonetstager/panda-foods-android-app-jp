package com.sha.netstager.pandafoods.adapters;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sha.netstager.pandafoods.R;
import com.sha.netstager.pandafoods.adapters.ProductCursorAdapter;
import com.sha.netstager.pandafoods.datatables.CatagoryDb;
import com.sha.netstager.pandafoods.datatables.ProductsDb;
import com.sha.netstager.pandafoods.model.CartModel;
import com.sha.netstager.pandafoods.util.AppConstants;
import com.squareup.picasso.Picasso;

/**
 * Created by prajeeshkk on 01/07/15.
 */
public class ProductListCursorAdapter extends CursorAdapter {
    private LayoutInflater mInflater;
    private Context context;
    private SharedPreferences sharedPreferences;
    private static CartModel[] quantityArray;
    private RelativeLayout itmImgLayout;

    public ProductListCursorAdapter(Context context, Cursor c, int flags) {
        super(context, c, flags);
        this.context = context;
        this.sharedPreferences = context.getSharedPreferences(AppConstants.PANDA_PREFERENCES, Context.MODE_PRIVATE);
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        quantityArray = new CartModel[0];
    }

    static class ViewHolder {
        ImageView imgIcon;
        TextView productname;
        TextView price;
        TextView packagename;
        EditText quantityText;
        Integer poss;
    }

    @Override
    public Cursor swapCursor(Cursor newCursor) {
        if (newCursor != null) {

            quantityArray = new CartModel[newCursor.getCount()];
            for (int i = 0; i < quantityArray.length; i++) {
                CartModel cartModel = new CartModel();
                cartModel.setQuantity("");
                quantityArray[i] = cartModel;
            }
        }
        return super.swapCursor(newCursor);
    }

    @Override
    public void bindView(View view, Context context, final Cursor cursor) {
        //Log.i("stass", "bindview");
        if (view != null) {

            final ViewHolder viewHolder = (ViewHolder) view.getTag();
            String pro_name = cursor.getString(cursor.getColumnIndexOrThrow(CatagoryDb.CAT_NAME));
            viewHolder.productname.setText(pro_name);
           //Log.i("Name Product", product_name);

            String Img_Url = cursor.getString(cursor.getColumnIndexOrThrow(CatagoryDb.CAT_IMAGE));

            String Img_DirctUrl= "http://orders.pandafoods.co.in/"+Img_Url;

            //List image setting here. in subcatagory page

            Picasso.with(context).load(Img_DirctUrl).resize(140,100).into(viewHolder.imgIcon);
            //Picasso.with(context).load(Img_DirctUrl).resize(50,80).into(viewHolder.imgIcon);
        }

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = super.getView(position, convertView, parent);
//        final ViewHolder viewHolder = (ViewHolder) view.getTag();
//        viewHolder.poss = position;
//        viewHolder.quantityText.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//                if (!TextUtils.isEmpty(s)) {
//                    quantityArray[viewHolder.poss].setQuantity(s.toString());
//                    Cursor cursor = (Cursor) getItem(viewHolder.poss);
//                    quantityArray[viewHolder.poss].setProductid(cursor.getString(cursor.getColumnIndexOrThrow(ProductsDb.PROD_ID)));
//                    ((ProductMainActivity) context).setTotal();
//                }
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//
//            }
//        });
//        view.setTag(viewHolder);
//        viewHolder.quantityText.setText(quantityArray[viewHolder.poss].getQuantity());
        return view;
    }


    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        View view = mInflater.inflate(R.layout.view_product_list_subcatagory, parent, false);
        ViewHolder viewHolder = new ViewHolder();
        viewHolder.productname = (TextView) view.findViewById(R.id.sub_catagoryName);
        viewHolder.imgIcon = (ImageView) view.findViewById(R.id.ItmImg);
        view.setTag(viewHolder);
        return view;
    }

    public CartModel[] getQuantityArray() {
        return quantityArray;
    }
}
