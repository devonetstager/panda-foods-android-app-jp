package com.sha.netstager.pandafoods.adapters;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.sha.netstager.pandafoods.R;
import com.sha.netstager.pandafoods.activity.ProductMainActivity;
import com.sha.netstager.pandafoods.datatables.ProductsDb;
import com.sha.netstager.pandafoods.model.CartModel;
import com.sha.netstager.pandafoods.util.AppConstants;
import com.squareup.picasso.Picasso;

/**
 * Created by netstager on 13/01/15.
 */
public class ProductCursorAdapter extends CursorAdapter {

    private LayoutInflater mInflater;
    private final Context context;
    private SharedPreferences sharedPreferences;
    private static CartModel[] quantityArray;

    private RelativeLayout itmImgLayout;
    public ProductCursorAdapter(Context context, Cursor c, int flags) {
        super(context, c, flags);
        this.context = context;
        this.sharedPreferences = context.getSharedPreferences(AppConstants.PANDA_PREFERENCES, Context.MODE_PRIVATE);
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        quantityArray = new CartModel[0];
    }

    static class ViewHolder {
        ImageView imgIcon;
        TextView productname;
        TextView price;
        TextView packagename;
        EditText quantityText;
        Integer poss;
    }

    @Override
    public Cursor swapCursor(Cursor newCursor) {
        if (newCursor != null) {

            quantityArray = new CartModel[newCursor.getCount()];
            for (int i = 0; i < quantityArray.length; i++) {
                CartModel cartModel = new CartModel();
                cartModel.setQuantity("");
                quantityArray[i] = cartModel;
            }
        }
        return super.swapCursor(newCursor);
    }

    @Override
    public void bindView(View view, Context context, final Cursor cursor) {
        //Log.i("stass", "bindview");
        if (view != null) {
            final ViewHolder viewHolder = (ViewHolder) view.getTag();

            int counter= cursor.getCount();
            Log.i("itmslistnum", String.valueOf(counter));

            SharedPreferences.Editor editor=sharedPreferences.edit();
            editor.putString("childItems", String.valueOf(counter));
            editor.apply();

//            String Img_Url = cursor.getString(cursor.getColumnIndexOrThrow(ProductsDb.PROD_IMAGE));
//
//            String Img_DirctUrl= "http://orders.pandafoods.co.in/"+Img_Url;
//
//            //Log.i("Img-Path",Img_DirctUrl);





//            LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//            View vi = inflater.inflate(R.layout.activity_product_main, null);
//            ImageView tv = (ImageView) vi.findViewById(R.id.imageView2);

            // Picasso.with(context).load(imgPath).into(tv);
            // Picasso.with(context).load("http://orders.pandafoods.co.in/uploads/category/noimage1.jpg").into(tv);



            //log.xml is your file.

            String product_name = cursor.getString(cursor.getColumnIndexOrThrow(ProductsDb.PROD_NAME));

            String product_weight = cursor.getString(cursor.getColumnIndexOrThrow(ProductsDb.PROD_WEIGHT));
            String weight_unit = cursor.getString(cursor.getColumnIndexOrThrow(ProductsDb.PROD_UNIT));
            String packagename = cursor.getString(cursor.getColumnIndexOrThrow(ProductsDb.PACKAGE));

            Log.i("Name Product",product_name+product_weight+weight_unit+packagename);
           // Log.i("Img-Path",Img_Url);

            viewHolder.productname.setText(product_name + " " + product_weight + weight_unit);


            viewHolder.price.setText("\u20B9" + cursor.getString(cursor.getColumnIndexOrThrow(sharedPreferences.getString(AppConstants.PREFERENCES_CUSTOMER_PRICE_TYPE, ProductsDb.PROD_CFSTOCKIST_PRICE))));
            viewHolder.packagename.setText("(" + packagename + ")");
            viewHolder.quantityText.setTag(cursor.getString(cursor.getColumnIndexOrThrow(ProductsDb.PROD_ID)));

            //Picasso.with(context).load(Img_DirctUrl).into(viewHolder.imgIcon);
        }

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = super.getView(position, convertView, parent);
        final ViewHolder viewHolder = (ViewHolder) view.getTag();
        viewHolder.poss = position;
        viewHolder.quantityText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!TextUtils.isEmpty(s)) {
                    quantityArray[viewHolder.poss].setQuantity(s.toString());
                    Cursor cursor = (Cursor) getItem(viewHolder.poss);
                    quantityArray[viewHolder.poss].setProductid(cursor.getString(cursor.getColumnIndexOrThrow(ProductsDb.PROD_ID)));
                    ((ProductMainActivity) context).setTotal();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        view.setTag(viewHolder);
        viewHolder.quantityText.setText(quantityArray[viewHolder.poss].getQuantity());
        return view;
    }


    @Override
    public View newView(final Context context, Cursor cursor, ViewGroup parent) {
        View view = mInflater.inflate(R.layout.view_product_list_child, parent, false);
        ViewHolder viewHolder = new ViewHolder();
        viewHolder.productname = (TextView) view.findViewById(R.id.productname);
        viewHolder.price = (TextView) view.findViewById(R.id.product_details_price);
        viewHolder.packagename = (TextView) view.findViewById(R.id.product_package);
        viewHolder.imgIcon = (ImageView) view.findViewById(R.id.imageView2);
        viewHolder.quantityText = (EditText) view.findViewById(R.id.product_quantity_field);
        viewHolder.quantityText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                //Toast.makeText(context,"Click event",Toast.LENGTH_LONG).show();
                if(hasFocus){
                    ((ProductMainActivity) context).visibleFasle();
                }else {
                    ((ProductMainActivity) context).visibleTrue();

                }

            }
        });
        view.setTag(viewHolder);
        return view;
    }



//    private void visibleTrue(ViewGroup parent) {
//        View view = mInflater.inflate(R.layout.view_product_list_child, parent, false);
//        ViewHolder viewHolder = new ViewHolder();
//        viewHolder.imgIcon.setVisibility(View.GONE);
//    }

    public CartModel[] getQuantityArray() {
        return quantityArray;
    }
}
