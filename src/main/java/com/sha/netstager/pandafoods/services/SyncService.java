package com.sha.netstager.pandafoods.services;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.app.Service;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.IBinder;
import android.provider.Settings;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;
import android.util.Log;

import com.sha.netstager.pandafoods.R;
import com.sha.netstager.pandafoods.contentProvider.LocalDbProvider;
import com.sha.netstager.pandafoods.datatables.CatagoryDb;
import com.sha.netstager.pandafoods.datatables.OfferDb;
import com.sha.netstager.pandafoods.datatables.ProductsDb;
import com.sha.netstager.pandafoods.datatables.WeightDb;
import com.sha.netstager.pandafoods.util.AppConstants;
import com.sha.netstager.pandafoods.util.JSONParser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;
import java.util.List;

public class SyncService extends Service {

    private String ANDROID_UDID;
    private SharedPreferences sharedPreferences;

    public SyncService() {

    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        ANDROID_UDID = Settings.Secure.getString(getContentResolver(),
                Settings.Secure.ANDROID_ID);
        new syncDB().execute();
        return START_STICKY;
    }

    private ContentValues retrieveProductValues(JSONObject singleProductObject) {

        ContentValues productvalue = new ContentValues();
        try {
            productvalue.put(ProductsDb.PROD_ID, singleProductObject.getString(ProductsDb.PROD_ID));
            productvalue.put(ProductsDb.PROD_CATEGORY, singleProductObject.getString(ProductsDb.PROD_CATEGORY));
            productvalue.put(ProductsDb.PROD_SUB_CATEGORY, singleProductObject.getString(ProductsDb.PROD_SUB_CATEGORY));
            productvalue.put(ProductsDb.PROD_NAME, singleProductObject.getString(ProductsDb.PROD_NAME));
            productvalue.put(ProductsDb.PROD_GROUP, singleProductObject.getString(ProductsDb.PROD_GROUP));
            productvalue.put(ProductsDb.PROD_WEIGHT_ID, singleProductObject.getString(ProductsDb.PROD_WEIGHT_ID));
            productvalue.put(ProductsDb.PROD_WEIGHT, singleProductObject.getString(ProductsDb.PROD_WEIGHT));
            productvalue.put(ProductsDb.PROD_UNIT, singleProductObject.getString(ProductsDb.PROD_UNIT));
            productvalue.put(ProductsDb.PACKAGE, singleProductObject.getString(ProductsDb.PACKAGE));
            productvalue.put(ProductsDb.PROD_DESCRIPTION, singleProductObject.getString(ProductsDb.PROD_DESCRIPTION));
            productvalue.put(ProductsDb.PROD_CFSTOCKIST_PRICE, singleProductObject.getString(ProductsDb.PROD_CFSTOCKIST_PRICE));
            productvalue.put(ProductsDb.PROD_CONSUMERS_PRICE, singleProductObject.getString(ProductsDb.PROD_CONSUMERS_PRICE));
            productvalue.put(ProductsDb.PROD_DEALER_PRICE, singleProductObject.getString(ProductsDb.PROD_DEALER_PRICE));
            productvalue.put(ProductsDb.PROD_DISTRIBUTOR_PRICE, singleProductObject.getString(ProductsDb.PROD_DISTRIBUTOR_PRICE));
            productvalue.put(ProductsDb.PROD_IMAGE, singleProductObject.getString(ProductsDb.PROD_IMAGE));
            productvalue.put(ProductsDb.PROD_IMAGE_THUMB, singleProductObject.getString(ProductsDb.PROD_IMAGE_THUMB));
            productvalue.put(ProductsDb.PROD_STATUS, singleProductObject.getString(ProductsDb.PROD_STATUS));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return productvalue;
    }

    private ContentValues retrieveOfferValues(JSONObject singleOfferObject) {
        ContentValues offervalue = new ContentValues();
        try {
            offervalue.put(OfferDb.OFFER_ID, singleOfferObject.getString(OfferDb.OFFER_ID));
            offervalue.put(OfferDb.OFFER_DESCRIPTION, singleOfferObject.getString(OfferDb.OFFER_DESCRIPTION));
            offervalue.put(OfferDb.OFFER_TITLE, singleOfferObject.getString(OfferDb.OFFER_TITLE));
            offervalue.put(OfferDb.PROMO_CODE, singleOfferObject.getString(OfferDb.PROMO_CODE));
            offervalue.put(OfferDb.OFFER_JSON, singleOfferObject.getString(OfferDb.OFFER_JSON));
            JSONObject offerJSONobject = new JSONObject(singleOfferObject.getString(OfferDb.OFFER_JSON));
            offervalue.put(OfferDb.OFFER_TYPE, offerJSONobject.getString("offer_type"));
            offervalue.put(OfferDb.OFFER_FROM_DATE, singleOfferObject.getString(OfferDb.OFFER_FROM_DATE));
            offervalue.put(OfferDb.OFFER_TO_DATE, singleOfferObject.getString(OfferDb.OFFER_TO_DATE));
            offervalue.put(OfferDb.OFFER_STATUS, singleOfferObject.getString(OfferDb.OFFER_STATUS));

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return offervalue;
    }


    private ContentValues retrieveCatagoryValues(JSONObject singleCatagoryObject) {
        ContentValues catagoryvalue = new ContentValues();
        try {
            catagoryvalue.put(CatagoryDb.CAT_ID, singleCatagoryObject.getString(CatagoryDb.CAT_ID));
            catagoryvalue.put(CatagoryDb.CAT_NAME, singleCatagoryObject.getString(CatagoryDb.CAT_NAME));
            catagoryvalue.put(CatagoryDb.PARENT_CAT, singleCatagoryObject.getString(CatagoryDb.PARENT_CAT));
            catagoryvalue.put(CatagoryDb.CAT_IMAGE, singleCatagoryObject.getString(CatagoryDb.CAT_IMAGE));
            catagoryvalue.put(CatagoryDb.CAT_DESCRIPTION, singleCatagoryObject.getString(CatagoryDb.CAT_DESCRIPTION));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return catagoryvalue;
    }


    private ContentValues retrieveWeightValues(JSONObject singleweightObject) {
        ContentValues weightvalue = new ContentValues();
        try {
            weightvalue.put(WeightDb.WEIGHT_ID, singleweightObject.getString(WeightDb.WEIGHT_ID));
            weightvalue.put(WeightDb.PROD_WEIGHT, singleweightObject.getString(WeightDb.PROD_WEIGHT));
            weightvalue.put(WeightDb.PROD_UNIT, singleweightObject.getString(WeightDb.PROD_UNIT));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return weightvalue;
    }

    private void insertToDB(JSONObject responseObject) {
        try {
            JSONObject detailsObject = responseObject.getJSONObject("details");
            JSONArray productArray = detailsObject.getJSONArray("product");
            JSONArray catagoryArray = detailsObject.getJSONArray("category");
            JSONArray weightArray = detailsObject.getJSONArray("weight");
            JSONArray offerArray = detailsObject.getJSONArray("offer");

            if (responseObject.getInt("initial") == 1) {

                //insert product data into sqlite table
                ContentValues[] pro_values = new ContentValues[productArray.length()];
                for (int i = 0; i < productArray.length(); i++) {
                    JSONObject singleProductObject = productArray.getJSONObject(i);
                    pro_values[i] = retrieveProductValues(singleProductObject);
                }
                getContentResolver().delete(LocalDbProvider.CONTENT_URI, null,null);
                getContentResolver().bulkInsert(LocalDbProvider.CONTENT_URI, pro_values);

                //insert catagory data into sqlite table
                ContentValues[] cat_values = new ContentValues[catagoryArray.length()];
                for (int i = 0; i < catagoryArray.length(); i++) {
                    JSONObject singleCatagoryObject = catagoryArray.getJSONObject(i);
                    cat_values[i] = retrieveCatagoryValues(singleCatagoryObject);
                }
                getContentResolver().delete(LocalDbProvider.CATAGORY_URI, null,null);
                int ids = getContentResolver().bulkInsert(LocalDbProvider.CATAGORY_URI, cat_values);
                Log.i("insert st:", ids + "");

                //insert weight
                ContentValues[] weight_values = new ContentValues[weightArray.length()];
                for (int i = 0; i < weightArray.length(); i++) {
                    JSONObject singleWeightObject = weightArray.getJSONObject(i);
                    weight_values[i] = retrieveWeightValues(singleWeightObject);
                }
                getContentResolver().delete(LocalDbProvider.WEIGHT_URI, null,null);
                getContentResolver().bulkInsert(LocalDbProvider.WEIGHT_URI, weight_values);

                //retrieve offers
                ContentValues[] offer_values = new ContentValues[offerArray.length()];
                for (int i = 0; i < offerArray.length(); i++) {
                    JSONObject singleofferObject = offerArray.getJSONObject(i);
                    offer_values[i] = retrieveOfferValues(singleofferObject);
                }

                getContentResolver().delete(LocalDbProvider.OFFER_URI, null, null);
                getContentResolver().bulkInsert(LocalDbProvider.OFFER_URI, offer_values);

                //edit shared preference :change sync_status
                SharedPreferences.Editor editor = sharedPreferences.edit();
                Date date = new Date();
                editor.putString("sync_status", date.toString());
                editor.apply();
            } else {
                int count = 0;
                for (int i = 0; i < productArray.length(); i++) {
                    JSONObject singleProductObject = productArray.getJSONObject(i);

                    switch (singleProductObject.getString("prod_app_status")) {
                        case "update":
                            count = getContentResolver().update(LocalDbProvider.CONTENT_URI, retrieveProductValues(singleProductObject), ProductsDb.PROD_ID + "=?", new String[]{singleProductObject.getString(ProductsDb.PROD_ID)});
                            break;
                        case "insert":
                            getContentResolver().insert(LocalDbProvider.CONTENT_URI, retrieveProductValues(singleProductObject));
                            break;
                        case "delete":
                            String productID = singleProductObject.getString(ProductsDb.PROD_ID);
                            count = getContentResolver().delete(LocalDbProvider.CONTENT_URI, ProductsDb.PROD_ID + "=?", new String[]{productID});
                            break;
                    }
                }
                for (int i = 0; i < catagoryArray.length(); i++) {
                    JSONObject singleCatagoryObject = catagoryArray.getJSONObject(i);
                    switch (singleCatagoryObject.getString("cat_app_status")) {
                        case "update":
                            count = getContentResolver().update(LocalDbProvider.CATAGORY_URI, retrieveCatagoryValues(singleCatagoryObject), CatagoryDb.CAT_ID + "=?", new String[]{singleCatagoryObject.getString(CatagoryDb.CAT_ID)});
                            break;
                        case "insert":
                            getContentResolver().insert(LocalDbProvider.CATAGORY_URI, retrieveCatagoryValues(singleCatagoryObject));
                            break;
                        case "delete":
                            String cat_id = singleCatagoryObject.getString(CatagoryDb.CAT_ID);
                            count = getContentResolver().delete(LocalDbProvider.CATAGORY_URI, CatagoryDb.CAT_ID + "=?", new String[]{cat_id});
                            break;
                    }
                }
                for (int i = 0; i < weightArray.length(); i++) {
                    JSONObject singleWeightObject = weightArray.getJSONObject(i);
                    switch (singleWeightObject.getString("weight_app_status")) {
                        case "update":
                            count = getContentResolver().update(LocalDbProvider.WEIGHT_URI, retrieveWeightValues(singleWeightObject), WeightDb.WEIGHT_ID + "=?", new String[]{singleWeightObject.getString(WeightDb.WEIGHT_ID)});
                            break;
                        case "insert":
                            getContentResolver().insert(LocalDbProvider.WEIGHT_URI, retrieveWeightValues(singleWeightObject));
                            break;
                        case "delete":
                            String weight_id = singleWeightObject.getString(WeightDb.WEIGHT_ID);
                            count = getContentResolver().delete(LocalDbProvider.WEIGHT_URI, WeightDb.WEIGHT_ID + "=?", new String[]{weight_id});
                            break;
                    }
                }
//                for (int i = 0; i < locArray.length(); i++) {
//                    JSONObject singleLocObject = weightArray.getJSONObject(i);
//                    switch (singleLocObject.getString("loc_app_status")) {
//                        case "update":
//                            count = getContentResolver().update(LocalDbProvider.LOC_URI, retrieveLocValues(singleLocObject), LocationDb.LOC_ID + "=?", new String[]{singleLocObject.getString(LocationDb.LOC_ID)});
//                            break;
//                        case "insert":
//                            getContentResolver().insert(LocalDbProvider.LOC_URI, retrieveLocValues(singleLocObject));
//                            break;
//                        case "delete":
//                            String loc_id = singleLocObject.getString(LocationDb.LOC_ID);
//                            count = getContentResolver().delete(LocalDbProvider.LOC_URI, LocationDb.LOC_ID + "=?", new String[]{loc_id});
//                            break;
//                    }
//                }
//                for (int i = 0; i < offerArray.length(); i++) {
//                    JSONObject singleOfferObject = offerArray.getJSONObject(i);
//                    switch (singleOfferObject.getString("offer_app_status")) {
//                        case "update":
//                            count = getContentResolver().update(LocalDbProvider.OFFER_URI, retrieveOfferValues(singleOfferObject), OfferDb.OFFER_ID + "=?", new String[]{singleOfferObject.getString(OfferDb.OFFER_ID)});
//                            break;
//                        case "insert":
//                            getContentResolver().insert(LocalDbProvider.OFFER_URI, retrieveOfferValues(singleOfferObject));
//                            break;
//                        case "delete":
//                            String offer_id = singleOfferObject.getString(OfferDb.OFFER_ID);
//                            count = getContentResolver().delete(LocalDbProvider.OFFER_URI, OfferDb.OFFER_ID + "=?", new String[]{offer_id});
//                            break;
//                    }
//                }
//                for (int i = 0; i < countryArray.length(); i++) {
//                    JSONObject singleCountryObject = countryArray.getJSONObject(i);
//                    switch (singleCountryObject.getString("country_app_status")) {
//                        case "update":
//                            count = getContentResolver().update(LocalDbProvider.COUNTRY_URI, retrieveCountryValues(singleCountryObject), CountryDb.COUNTRY_ID + "=?", new String[]{singleCountryObject.getString(CountryDb.COUNTRY_ID)});
//                            break;
//                        case "insert":
//                            getContentResolver().insert(LocalDbProvider.COUNTRY_URI, retrieveCountryValues(singleCountryObject));
//                            break;
//                        case "delete":
//                            String country_id = singleCountryObject.getString(CountryDb.COUNTRY_ID);
//                            count = getContentResolver().delete(LocalDbProvider.COUNTRY_URI, CountryDb.COUNTRY_ID + "=?", new String[]{country_id});
//                            break;
//                    }
//                }
//                for (int i = 0; i < stateArray.length(); i++) {
//                    JSONObject singleStateObject = stateArray.getJSONObject(i);
//                    switch (singleStateObject.getString("state_app_status")) {
//                        case "update":
//                            count = getContentResolver().update(LocalDbProvider.STATE_URI, retrieveStateValues(singleStateObject), StateDb.STATE_ID + "=?", new String[]{singleStateObject.getString(StateDb.STATE_ID)});
//                            break;
//                        case "insert":
//                            getContentResolver().insert(LocalDbProvider.STATE_URI, retrieveStateValues(singleStateObject));
//                            break;
//                        case "delete":
//                            String offer_id = singleStateObject.getString(StateDb.STATE_ID);
//                            count = getContentResolver().delete(LocalDbProvider.STATE_URI, StateDb.STATE_ID + "=?", new String[]{offer_id});
//                            break;
//                    }
//                }
//                for (int i = 0; i < districtArray.length(); i++) {
//                    JSONObject singleDistrictObject = districtArray.getJSONObject(i);
//                    switch (singleDistrictObject.getString("district_app_status")) {
//                        case "update":
//                            count = getContentResolver().update(LocalDbProvider.DISTRICT_URI, retrieveDistriceValues(singleDistrictObject), DistrictDb.DISTRICT_ID + "=?", new String[]{singleDistrictObject.getString(DistrictDb.DISTRICT_ID)});
//                            break;
//                        case "insert":
//                            getContentResolver().insert(LocalDbProvider.DISTRICT_URI, retrieveDistriceValues(singleDistrictObject));
//                            break;
//                        case "delete":
//                            String district_id = singleDistrictObject.getString(DistrictDb.DISTRICT_ID);
//                            count = getContentResolver().delete(LocalDbProvider.DISTRICT_URI, DistrictDb.DISTRICT_ID + "=?", new String[]{district_id});
//                            break;
//                    }
//                }

                //edit shared preference :change sync_status
                SharedPreferences.Editor editor = sharedPreferences.edit();
                Date date = new Date();
                editor.putString("sync_status", date.toString());
                editor.apply();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private boolean checkActiveTask() {
        ActivityManager am = (ActivityManager) getApplicationContext()
                .getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> alltasks = am
                .getRunningTasks(1);
        for (ActivityManager.RunningTaskInfo aTask : alltasks) {
            // Used to check for CURRENT example main screen
            String packageName = "com.sha.netstager.pandafoods";

            if (aTask.topActivity.getPackageName().equals(
                    packageName)) {
                return true;
            }

        }
        return false;
    }


    private class syncDB extends AsyncTask<Void, Integer, String> {
        private JSONObject syncObject = new JSONObject();
        private NotificationCompat.Builder mBuilder;
        private ProgressDialog dialog;
        private NotificationManager mNotifyManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        private final int id = 1;
        long lenghtOfFile;

        @Override
        protected void onPreExecute() {

            //Toast.makeText(getApplicationContext(), "Syncing in Progress..", Toast.LENGTH_LONG).show();
            notifyMe();
            sharedPreferences = getSharedPreferences(AppConstants.PANDA_PREFERENCES, MODE_PRIVATE);
            String syncStatus = sharedPreferences.getString("sync_status", "INITIAL");
            try {
                syncObject.put("ud_id", ANDROID_UDID);
                if (syncStatus.equals("INITIAL")) {
                    syncObject.put("initial", "1");
                } else {
                    syncObject.put("initial", "0");
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        private void notifyMe() {
// Builds the notification and issues it.
            mBuilder = new NotificationCompat.Builder(getApplicationContext());
            mBuilder.setContentTitle("Panda Foods")
                    .setContentText("Syncing Products in progress")
                    .setSmallIcon(R.drawable.ic_noti_logo);
// Start a lengthy operation in a background thread
            new Thread(
                    new Runnable() {
                        @Override
                        public void run() {
                            mBuilder.setProgress(0, 0, true);
                            Notification notification = mBuilder.build();
                            notification.flags = Notification.FLAG_NO_CLEAR;
                            mNotifyManager.notify(id, notification);
                        }
                    }
// Starts the thread by calling the run() method in its Runnable
            ).start();
        }

        @Override
        protected String doInBackground(Void... params) {
            String response = JSONParser.makeHttpRequest(AppConstants.URL_SINK, "POST", syncObject.toString());
            return response;
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            SyncService.this.stopSelf();
            mNotifyManager.cancel(id);
        }

        @Override
        protected void onPostExecute(String response) {

            Log.i("sync api response",response);
            try {
                if (TextUtils.isEmpty(response)) {
                    mNotifyManager.cancel(id);
                    SharedPreferences.Editor editor=sharedPreferences.edit();
                    editor.putString("sync_status","INITIAL");
                    editor.apply();
                } else {
                    JSONObject responseObject = new JSONObject(response);
                    insertToDB(responseObject);
                }


                mBuilder.setContentText("Sync complete")
                        // Removes the progress bar
                        .setProgress(0, 0, false);
                mNotifyManager.notify(id, mBuilder.build());
                SyncService.this.stopSelf();
//                stopService(new Intent(getApplicationContext(), SyncService.class));
            } catch (JSONException e) {

                e.printStackTrace();
            }
        }

    }
}
