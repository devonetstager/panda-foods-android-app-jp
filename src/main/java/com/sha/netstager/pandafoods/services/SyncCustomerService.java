package com.sha.netstager.pandafoods.services;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.Service;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.IBinder;
import android.provider.Settings;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.sha.netstager.pandafoods.R;
import com.sha.netstager.pandafoods.contentProvider.LocalDbProvider;
import com.sha.netstager.pandafoods.datatables.CustomerDb;
import com.sha.netstager.pandafoods.util.AppConstants;
import com.sha.netstager.pandafoods.util.JSONParser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

public class SyncCustomerService extends Service {
    private String ANDROID_UDID;
    private SharedPreferences sharedPreferences;
    
    public SyncCustomerService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        ANDROID_UDID = Settings.Secure.getString(getContentResolver(),
                Settings.Secure.ANDROID_ID);
        new syncDB().execute();
        return START_STICKY;
    }

    private ContentValues retrieveCustomerValues(JSONObject singleProductObject) {
        ContentValues productvalue = new ContentValues();
        try {
            productvalue.put(CustomerDb.CUST_ID, singleProductObject.getString(CustomerDb.CUST_ID));
            productvalue.put(CustomerDb.CUST_NAME, singleProductObject.getString(CustomerDb.CUST_NAME));
            productvalue.put(CustomerDb.LOC_ID, singleProductObject.getString(CustomerDb.LOC_ID));
            productvalue.put(CustomerDb.CUST_PHONE, singleProductObject.getString(CustomerDb.CUST_PHONE));
            productvalue.put(CustomerDb.CUST_EMAIL, singleProductObject.getString(CustomerDb.CUST_EMAIL));
            productvalue.put(CustomerDb.CUST_ADDRESS, singleProductObject.getString(CustomerDb.CUST_ADDRESS));
            productvalue.put(CustomerDb.USER_MODE, singleProductObject.getString(CustomerDb.USER_MODE));
            productvalue.put(CustomerDb.CUST_TYPE_ID, singleProductObject.getString(CustomerDb.CUST_TYPE_ID));
            productvalue.put(CustomerDb.CUST_GROUP_ID, singleProductObject.getString(CustomerDb.CUST_GROUP_ID));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return productvalue;
    }

    private void insertToDB(JSONObject responseObject) {
        try {
            JSONArray customerArray = responseObject.getJSONArray("details");

            if (responseObject.getInt("initial") == 1) {

                ContentValues[] cust_values=new ContentValues[customerArray.length()];
                for (int i=0;i<customerArray.length();i++){
                    JSONObject singleLocObject = customerArray.getJSONObject(i);
                    cust_values[i]=retrieveCustomerValues(singleLocObject);
                }
                getContentResolver().bulkInsert(LocalDbProvider.CUSOTMER_URI, cust_values);


                //edit shared preference :change sync_status
                SharedPreferences.Editor editor = sharedPreferences.edit();
                Date date = new Date();
                editor.putString("cust_sync_status", date.toString());
                editor.apply();
            } else {
                int count = 0;
                for (int i = 0; i < customerArray.length(); i++) {
                    JSONObject singleProductObject = customerArray.getJSONObject(i);

                    switch (singleProductObject.getString("cust_app_status")) {
                        case "update":
                            count = getContentResolver().update(LocalDbProvider.CUSOTMER_URI, retrieveCustomerValues(singleProductObject), CustomerDb.CUST_ID + "=?", new String[]{singleProductObject.getString(CustomerDb.CUST_ID)});
                            break;
                        case "insert":
                            getContentResolver().insert(LocalDbProvider.CUSOTMER_URI, retrieveCustomerValues(singleProductObject));
                            break;
                        case "delete":
                            String custID = singleProductObject.getString(CustomerDb.CUST_ID);
                            count = getContentResolver().delete(LocalDbProvider.CUSOTMER_URI, CustomerDb.CUST_ID + "=?", new String[]{custID});
                            break;
                    }
                }

                //edit shared preference :change sync_status
                SharedPreferences.Editor editor = sharedPreferences.edit();
                Date date = new Date();
                editor.putString("cust_sync_status", date.toString());
                editor.apply();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private class syncDB extends AsyncTask<Void, Void, String> {
        private JSONObject syncObject = new JSONObject();
        private NotificationCompat.Builder mBuilder;
        private NotificationManager mNotifyManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        private final int id = 2;

        @Override
        protected void onPreExecute() {
            Toast.makeText(getApplicationContext(),"Syncing Customers...",Toast.LENGTH_LONG).show();
            notifyMe();
            sharedPreferences = getSharedPreferences(AppConstants.PANDA_PREFERENCES, MODE_PRIVATE);
            String syncStatus = sharedPreferences.getString("cust_sync_status", "INITIAL");
            try {
                syncObject.put("ud_id", ANDROID_UDID);
                if (syncStatus.equals("INITIAL")) {
                    syncObject.put("initial", "1");
                } else {
                    syncObject.put("initial", "0");
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        private void notifyMe() {

// Builds the notification and issues it.



            mBuilder = new NotificationCompat.Builder(getApplicationContext());
            mBuilder.setContentTitle("Panda Foods")
                    .setContentText("Syncing Customers in progress")
                    .setSmallIcon(R.drawable.ic_noti_logo);
// Start a lengthy operation in a background thread
            new Thread(
                    new Runnable() {
                        @Override
                        public void run() {
                            mBuilder.setProgress(0, 0, true);
                            Notification notification=mBuilder.build();
                            notification.flags=Notification.FLAG_NO_CLEAR;
                            mNotifyManager.notify(id, notification);

                        }
                    }
// Starts the thread by calling the run() method in its Runnable
            ).start();
        }

        @Override
        protected String doInBackground(Void... params) {
            return JSONParser.makeHttpRequest(AppConstants.URL_CUSTOMER_SINK, "POST", syncObject.toString());
        }
        @Override
        protected void onCancelled() {
            super.onCancelled();
            mNotifyManager.cancel(id);
            SyncCustomerService.this.stopSelf();
        }

        @Override
        protected void onPostExecute(String response) {
            try {
                if (TextUtils.isEmpty(response)) {
                    mNotifyManager.cancel(id);
                    SharedPreferences.Editor editor=sharedPreferences.edit();
                    editor.putString("cust_sync_status","INITIAL");
                    editor.apply();
                } else {
                    JSONObject responseObject = new JSONObject(response);
                    insertToDB(responseObject);
                }
                mBuilder.setContentText("Sync complete")
                        // Removes the progress bar
                        .setProgress(0, 0, false);
                mNotifyManager.notify(id, mBuilder.build());
                SyncCustomerService.this.stopSelf();
            } catch (JSONException e) {

                e.printStackTrace();
            }
        }
    }
    
}
