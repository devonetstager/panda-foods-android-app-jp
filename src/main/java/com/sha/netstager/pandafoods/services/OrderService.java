package com.sha.netstager.pandafoods.services;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.Service;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.IBinder;
import android.provider.Settings;
import android.support.v4.app.NotificationCompat;
import android.widget.Toast;

import com.sha.netstager.pandafoods.R;
import com.sha.netstager.pandafoods.contentProvider.LocalDbProvider;
import com.sha.netstager.pandafoods.datatables.OrderDb;
import com.sha.netstager.pandafoods.datatables.OrderProductsDb;
import com.sha.netstager.pandafoods.util.AppConstants;
import com.sha.netstager.pandafoods.util.JSONParser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class OrderService extends Service {
    private String ANDROID_UDID;
    private SharedPreferences sharedPreferences;
    private String orderid;

    public OrderService(){}

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.

        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        ANDROID_UDID = Settings.Secure.getString(getContentResolver(),
                Settings.Secure.ANDROID_ID);

        sharedPreferences= getSharedPreferences(AppConstants.PANDA_PREFERENCES,MODE_PRIVATE);
        Toast.makeText(getApplicationContext(),"Order Sending",Toast.LENGTH_SHORT).show();
        String[] projection = new String[]{
                OrderDb._ID,
                OrderDb.EXE_ID,
                OrderDb.ORDER_DATE,
                OrderDb.PROMO_CODE,
                OrderDb.ORDER_AMOUNT,
                OrderDb.USER_ID,
        };
        try {
            Cursor orderCursor;
            if (!intent.getExtras().getString("OrderID").equals("All")){
              String orderid=  intent.getExtras().getString("OrderID");
              orderCursor=getContentResolver().query(LocalDbProvider.ORDER_URI, projection, OrderDb._ID + "=?", new String[]{orderid}, null);
            }
            else {
                orderCursor= getContentResolver().query(LocalDbProvider.ORDER_URI, projection, OrderDb.PROD_SYNC_STATUS + "=?", new String[]{"0"}, null);
            }

            if (orderCursor.moveToFirst()) {
                do {
                    Cursor productsCursor = retrieveProductCursor(orderCursor.getString(orderCursor.getColumnIndexOrThrow(OrderProductsDb._ID)));

                    JSONArray productArray = new JSONArray();
                    if (productsCursor.moveToFirst())
                    do{
                        JSONObject orderProductObject = new JSONObject();
                        orderProductObject.put(OrderProductsDb.PROD_ID, productsCursor.getString(productsCursor.getColumnIndexOrThrow(OrderProductsDb.PROD_ID)));
                        orderProductObject.put(OrderProductsDb.PROD_QUANTITY, productsCursor.getString(productsCursor.getColumnIndexOrThrow(OrderProductsDb.PROD_QUANTITY)));
                        orderProductObject.put(OrderProductsDb.PROD_PRICE, productsCursor.getString(productsCursor.getColumnIndexOrThrow(OrderProductsDb.PROD_PRICE)));
                        orderProductObject.put(OrderProductsDb.PROD_CATEGORY, productsCursor.getString(productsCursor.getColumnIndexOrThrow(OrderProductsDb.PROD_CATEGORY)));
                        orderProductObject.put(OrderProductsDb.PROD_GROUP, productsCursor.getString(productsCursor.getColumnIndexOrThrow(OrderProductsDb.PROD_GROUP)));
                        productArray.put(orderProductObject);
                    }while (productsCursor.moveToNext());
                    productsCursor.close();
                    JSONObject orderobject = new JSONObject();
                    orderobject.put("ud_id", ANDROID_UDID);
                    orderobject.put(OrderDb.USER_ID, sharedPreferences.getString(AppConstants.PREFERENCES_USER_ID, "0"));
                    orderobject.put(OrderDb.EXE_ID, orderCursor.getString(orderCursor.getColumnIndexOrThrow(OrderDb.EXE_ID)));
                    orderobject.put(OrderDb.PROMO_CODE, orderCursor.getString(orderCursor.getColumnIndexOrThrow(OrderDb.PROMO_CODE)));
                    orderobject.put(OrderDb.ORDER_DATE, orderCursor.getString(orderCursor.getColumnIndexOrThrow(OrderDb.ORDER_DATE)));
                    orderobject.put("order_details", productArray);
                    JSONObject mainProductObject = new JSONObject();
                    mainProductObject.put("order_details_main", orderobject.toString());
                    new sendAsynchronousOrders(orderCursor.getString(orderCursor.getColumnIndexOrThrow(OrderProductsDb._ID))).execute(mainProductObject.toString());
                }while (orderCursor.moveToNext());
            }
            else {
                orderCursor.close();
                OrderService.this.stopSelf();
            }
            orderCursor.close();

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return super.onStartCommand(intent, flags, startId);
    }

    private Cursor retrieveProductCursor(String orderid) {
        String[] projection = new String[]{
                OrderProductsDb._ID,
                OrderProductsDb.ORDER_ID,
                OrderProductsDb.PROD_CATEGORY,
                OrderProductsDb.PROD_GROUP,
                OrderProductsDb.PROD_ID,
                OrderProductsDb.PROD_QUANTITY,
                OrderProductsDb.PROD_PRICE
        };
        Cursor cursor = getContentResolver().query(LocalDbProvider.ORDER_PRODUCT_URI, projection, OrderProductsDb.ORDER_ID + "=?", new String[]{orderid}, null);

        return cursor;
    }

    private class sendAsynchronousOrders extends AsyncTask<String, Void, String> {

        private String contentid;
        private NotificationCompat.Builder mBuilder;
        private NotificationManager mNotifyManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        private final int id = 3;

        @Override
        protected void onPreExecute() {
            notifyMe();
        }

        private void notifyMe() {
            mBuilder = new NotificationCompat.Builder(getApplicationContext());
            mBuilder.setContentTitle("Panda Foods")
                    .setContentText("Syncing in progress")
                    .setSmallIcon(R.drawable.ic_noti_logo);
            new Thread(
                    new Runnable() {
                        @Override
                        public void run() {
                            mBuilder.setProgress(0, 0, true);
                            Notification notification=mBuilder.build();
                            notification.flags=Notification.FLAG_NO_CLEAR;
                            mNotifyManager.notify(id, notification);
                        }
                    }
            ).start();
        }


        public sendAsynchronousOrders(String contentid) {
            this.contentid= contentid;
        }

        @Override
        protected String doInBackground(String... params) {
//            Log.i("json_req",params[0]);
            return JSONParser.makeHttpRequest(AppConstants.URL_ORDER,"POST",params[0]);
        }
        @Override
        protected void onCancelled() {
            super.onCancelled();
            mNotifyManager.cancel(id);
            OrderService.this.stopSelf();
//            stopService(new Intent(getApplicationContext(), OrderService.class));
        }


        @Override
        protected void onPostExecute(String result) {
            try {
                JSONObject object=new JSONObject(result);
                if (object.getString("status").equals("success")){
                    ContentValues values=new ContentValues();
                    values.put(OrderDb.ORDER_SEESION_ID,object.getString(OrderDb.ORDER_SEESION_ID));
                    values.put(OrderDb.PROD_SYNC_STATUS,"1");
                    getContentResolver().update(LocalDbProvider.ORDER_URI,values,OrderDb._ID+"=?",new String[]{contentid});
                    Toast.makeText(getApplicationContext(),"Order Sent!",Toast.LENGTH_SHORT).show();
                }
                mBuilder.setContentText("Sync complete")
                        // Removes the progress bar
                        .setProgress(0, 0, false);
                mNotifyManager.notify(id, mBuilder.build());

//                stopService(new Intent(getApplicationContext(), OrderService.class));
                OrderService.this.stopSelf();

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

}
