package com.sha.netstager.pandafoods.model;

/**
 * Created by netstager on 22/01/15.
 */
public class CustomerModel {
    public String user_id;
    public String user_name;
    public String address;
    public String email;
    public String location;
    public String phoneno;
    public String customer_type;

    public CustomerModel(String user_id,String user_name,String address,String email,String location,String phoneno,String customer_type){
        this.user_id= user_id;
        this.user_name=user_name;
        this.address= address;
        this.email=email;
        this.location=location;
        this.phoneno=phoneno;
        this.customer_type=customer_type;
    }

    public String getUser_id() {
        return user_id;
    }

    public String getUser_name() {
        return user_name;
    }

    public String getAddress() {
        return address;
    }

    public String getEmail() {
        return email;
    }

    public String getLocation() {
        return location;
    }

    public String getPhoneno() {
        return phoneno;
    }

    public String getCustomer_type() {
        return customer_type;
    }

}
