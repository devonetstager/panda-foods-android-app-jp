package com.sha.netstager.pandafoods.model;

/**
 * Created by netstager on 12/01/15.
 */
public class HomeMainModel {

    private String aClass;
    private int resourceId;
    private String label;

    public HomeMainModel(String aClass,int resourceId,String label){
        this.aClass=aClass;
        this.resourceId=resourceId;
        this.label=label;
    }

    public String getaClass() {
        return aClass;
    }

    public int getResourceId() {
        return resourceId;
    }

    public String getLabel() {
        return label;
    }
}
