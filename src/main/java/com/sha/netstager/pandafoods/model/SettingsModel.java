package com.sha.netstager.pandafoods.model;

/**
 * Created by netstager on 15/01/15.
 */
public class SettingsModel {
    private int resourceId;
    private String label;
    private int id;
    public SettingsModel( int resourceId, String label, int id){
        this.resourceId=resourceId;
        this.label=label;
        this.id=id;
    }

    public int getResourceId() {
        return resourceId;
    }

    public String getLabel() {
        return label;
    }

    public int getId() {
        return id;
    }
}
