package com.sha.netstager.pandafoods.model;

/**
 * Created by netstager on 07/02/15.
 */
public class CartModel {
    private String productid;
    private String quantity;

    public void setProductid(String productid) {
        this.productid = productid;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }
    public String getProductid() {
        return productid;
    }
    public String getQuantity() {
        return quantity;
    }

}
