package com.sha.netstager.pandafoods.model;

/**
 * Created by netstager on 22/01/15.
 */
public class ProductsModel {
    public String prod_id;
    public String prod_name;
    public String prod_group;
    public String prod_weight;
    public String prod_price;
    public String prod_catagory;


    public ProductsModel(String prod_id, String prod_name, String prod_group, String prod_weight, String prod_price, String prod_catagory){
        this.prod_id= prod_id;
        this.prod_name=prod_name;
        this.prod_group= prod_group;
        this.prod_weight=prod_weight;
        this.prod_price=prod_price;
        this.prod_catagory=prod_catagory;
    }

    public String getProd_id() {
        return prod_id;
    }

    public String getProd_name() {
        return prod_name;
    }

    public String getProd_catagory() {
        return prod_catagory;
    }

    public String getProd_group() {
        return prod_group;
    }

    public String getProd_price() {
        return prod_price;
    }

    public String getProd_weight() {
        return prod_weight;
    }

}
